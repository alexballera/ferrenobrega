<?php include('header.php');?>
<?php include('head.php');?>
<?php if(have_posts()) : while(have_posts()) : the_post();?>
	<section class="content-wrap">
		<div class="container page">

			<h2><?php the_title();?></h2>
			<?php the_breadcrumb();?>
			<?php the_content();?>

		</div>
	</section>
<?php endwhile;?>
<!-- Else -->
<?php else:?>
<?php endif;?>
<?php include('footer.php');?>