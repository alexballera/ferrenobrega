<?php include('header.php');?>
<?php include('head.php');?>

<?php if ( is_single() && get_post_type() == 'product' ) { ?>
<?php get_template_part('woocommerce/single', 'product');?>
<?php } elseif ( is_tax( 'product_cat' ) || is_tax( 'product_tag' ) ) {?>
<?php get_template_part('woocommerce/archive', 'product');?>
<?php } elseif ( is_post_type_archive( 'product' ) || is_page( woocommerce_get_page_id( 'shop' ) ) ) { ?>
<?php get_template_part('woocommerce/archive', 'product');?>




<?php }	?>
