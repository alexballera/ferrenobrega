<?php
/*
Template Name: pagina inicio
*/
?>
<?php include('header.php');?>
<?php include('head.php');?>

<section class="rotate">
	<div class="flexslider principal">
		<ul class="slides">
				<li><a href="<?php bloginfo('home');?>/tienda"><img src="<?php bloginfo('template_url');?>/images/banner1_compugate.jpg"></a></li>

			<li><img src="<?php bloginfo('template_url');?>/images/banner2.jpg"></a></li>
		</ul>
	</div>
</section>

<div class="clearfloat"><br></div>



<section class="productos hidden-xs">
	<div class="container">
		<h3 class="front-title"><span>Lo M�s Recientes</span></h3>
	</div>
	<div class="container woocommerce">
		<ul class="products">
			<?php	$args = array(
				'post_type' => 'product',
				'posts_per_page' => 8,
				);?>
				<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php include('loop-slider.php');?>
				
			<?php endwhile;	}	else { echo __( 'No products found' ); }	wp_reset_query();	?>
		</ul>
	</div>
</section>

<ul class="tabs-list nav nav-tabs">
		    				<li class="active">
					<a href="#tab27" data-toggle="tab" data-title="Clothing, Shoes &amp; Jewelry">
                        <i class="fa fa-suitcase"></i>
                                                <span class="wpo-cat_title">Clothing, Shoes &amp; Jewelry</span>
					</a>
				</li>
		    				<li>
					<a href="#tab26" data-toggle="tab" data-title="Electronics &amp; Computers">
                        <i class="fa fa-apple"></i>
                                                <span class="wpo-cat_title">Electronics &amp; Computers</span>
					</a>
				</li>
		    				<li>
					<a href="#tab28" data-toggle="tab" data-title="Home &amp; Kitchen">
                        <i class="fa fa-home"></i>
                                                <span class="wpo-cat_title">Home &amp; Kitchen</span>
					</a>
				</li>
		    				<li>
					<a href="#tab34" data-toggle="tab" data-title="Kids &amp; Toys">
                        <i class="fa fa-gift"></i>
                                                <span class="wpo-cat_title">Kids &amp; Toys</span>
					</a>
				</li>
		    				<li>
					<a href="#tab282" data-toggle="tab" data-title="Movies, Music &amp; Games">
                        <i class="fa fa fa-gamepad"></i>
                                                <span class="wpo-cat_title">Movies, Music &amp; Games</span>
					</a>
				</li>
		    			</ul>









<div class="clearfloat"><br></div>

<section class="productos hidden-xs">
	<div class="container">
		<h3 class="front-title"><span>Ofertas del Mes</span></h3>
	</div>
	<div class="container woocommerce">
		<div class="productos-slider carrusel flexslider">
			<ul class="slides products">
				<?php
				$args = array(
					'post_type'      => 'product',
					'posts_per_page' => 8,
					'meta_query'     => array(
						'relation' => 'OR',
						array(
							'key'           => '_sale_price',
							'value'         => 0,
							'compare'       => '>',
							'type'          => 'numeric'
							),
						array(
							'key'           => '_min_variation_sale_price',
							'value'         => 0,
							'compare'       => '>',
							'type'          => 'numeric'
							)
						)
					);
					?>
					<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php include('loop-slider.php');?>
				<?php endwhile; } else {	echo __( '<h3>No hay productos en oferta</h3>' );} wp_reset_postdata();?>
			</ul>
		</div>
	</div>
</section>

<section class="productos hidden-sm hidden-md hidden-lg">
	<div class="container">
		<h3 class="front-title"><span>Ofertas del Mes</span></h3>
	</div>
	<div class="container woocommerce">
		<ul class="products">
			<?php
			$args = array(
				'post_type'      => 'product',
				'posts_per_page' => 8,
				'meta_query'     => array(
					'relation' => 'OR',
					array(
						'key'           => '_sale_price',
						'value'         => 0,
						'compare'       => '>',
						'type'          => 'numeric'
						),
					array(
						'key'           => '_min_variation_sale_price',
						'value'         => 0,
						'compare'       => '>',
						'type'          => 'numeric'
						)
					)
				);
				?>
				<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php wc_get_template_part( 'content', 'product' );?>
			<?php endwhile; } else {	echo __( '<h3>No hay productos en oferta</h3>' );} wp_reset_postdata();?>
		</ul>
	</div>
</section>

<div class="clearfloat"><br><br></div>

<section class="bottom">
	<div class="container"><h3 class="front-title"><span>Y Aun Hay M�s</span></h3></div>
	<div class="clearfloat"><br></div>
	<div class="container marketing">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="<?php bloginfo('home');?>/categoria-producto/routers"><img src="<?php bloginfo('template_url');?>/images/banner-peque1.jpg" class="img-responsive center-block"></a>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg"><br></div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<a href="<?php bloginfo('home');?>/categoria-producto/tablets"><img src="<?php bloginfo('template_url');?>/images/banner-peque2.jpg" class="img-responsive center-block"></a>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg"><br></div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<a href="<?php bloginfo('home');?>/categoria-producto/cableado"><img src="<?php bloginfo('template_url');?>/images/banner-peque3.jpg" class="img-responsive center-block"></a>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg"><br></div>
			</div>
		</div>
	</div>
</section>

<section class="content-wrap">
		<div class="container page">
			<?php the_content();?>

		</div>
	</section>

<section class="productos">
	<div class="container">
		<h3 class="text-center">TRABAJAMOS CON LAS MEJORES MARCAS</h3>
	</div>
	<div class="clearfix"><br></div>
	<div class="rotate-products">
		<div class="container">
			<ul id="slides">
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/apple-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/epson-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/hp-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/logo-microsoft.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/logo-samsung.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/sandisk-logo.png" alt=""></li>
			</ul>
		</div>
	</div>
</section>

<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.flexisel.js"></script>
<script type="text/javascript">
jQuery(window).load(function() {
	jQuery("#slides").flexisel({
		visibleItems: 6,
		animationSpeed: 200,
		autoPlay: false,
		autoPlaySpeed: 3000,
		pauseOnHover: true,
		clone:false,
		enableResponsiveBreakpoints: true,
		responsiveBreakpoints: {
			portrait: {
				changePoint:480,
				visibleItems: 1
			},
			landscape: {
				changePoint:640,
				visibleItems: 2
			},
			tablet: {
				changePoint:768,
				visibleItems: 3
			}
		}
	});
});
</script>

<div class="nopad hidden-xs hidden-sm col-md-12">
<iframe src="https://www.google.com/maps/d/embed?mid=z9XHXCgDQs-Y.kEiXvR8boiAY" width="100%" height="480"frameborder="0" style="border:0; display: table;" allowfullscreen></iframe> 
</div>


<?php include('footer.php');?>