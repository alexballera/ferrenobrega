<li class="margin-ch product">
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<a href="<?php the_permalink(); ?>">
		<?php echo woocommerce_show_product_loop_sale_flash($post, $product); ?>
		<?php
		if ( has_post_thumbnail() ) {

			$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
			$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
			$image_link  	= get_the_permalink();
			$home					= get_template_directory_uri();
			$imagen_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
			$ruta_imagen = $imagen_full[0];

			$attachment_count = count( $product->get_gallery_attachment_ids() );

			if ( $attachment_count > 0 ) {
				$gallery = '[product-gallery]';
			} else {
				$gallery = '';
			}

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="'.$image_link.'" itemprop="image" class="woocommerce-main-image zoom" title="'.$title.'"><img src="'.$imagen_full[0].'"></a>', $image_link, $image_caption, $image_full ), $post->ID );

		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

		}
		?>
     
	</a>
	<?php
	do_action( 'woocommerce_shop_loop_item_title' );
	do_action( 'woocommerce_after_shop_loop_item_title' );
	?>
	<a href="<?php the_permalink();?>" rel="nofollow" class="read-more">Ver Detalles</a>
</li>
