<?php



/*-----------------------------------------------------------------------------------*/

/* SUPER ADMIN */

/*-----------------------------------------------------------------------------------*/



$user_id = get_current_user_id();

if ( is_user_logged_in() ) {

	if ( 1 == $user_id ) {



	}

	else {

		/* BLOQUEO DE OTRO USUARIOS */

		function example_remove_dashboard_widgets() {

			global $wp_meta_boxes;

			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);

			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);

			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);

    //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);

			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);

			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);

			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);

			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

		}

		// add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets' );



		function wpse_136058_remove_menu_pages() {

			remove_menu_page( 'iphorm_forms' );

			remove_menu_page( 'theme_my_login' );

			remove_menu_page( 'edit.php?post_type=acf-field-group' );

		}

		add_action( 'admin_init', 'wpse_136058_remove_menu_pages' );



		function quitar_menus () {

			global $menu;

			$restricted = array(

				__('Appearance'),

				__('Links'),

				__('Comments'),

				__('Plugins'),

				__('Settings'),

				__('Tools')

  		//__('Dashboard'),

  		//__('Users'),

  		//__('Pages'),

  		//__('Posts'),

  		//__('Media')

				);

			end ($menu);

			while (prev($menu)){

				$value = explode(' ',$menu[key($menu)][0]);

				if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}

			}

		}

		add_action('admin_menu', 'quitar_menus');



		function remove_submenus() {

			global $submenu;

  unset($submenu['index.php'][5]); // Removes 'Updates'.

  unset($submenu['index.php'][10]); // Removes 'Updates'.



}

add_action('admin_menu', 'remove_submenus');

/* FIN DEL BLOQUEO DE OTROS USUARIOS*/

}

}



/*-----------------------------------------------------------------------------------*/

/* TRADUCCION DE ALGUNOS ELEMENTOS DE WOOCOMMERCE */

/*-----------------------------------------------------------------------------------*/



function wpse_77783_woo_bacs_ibn($translation, $text, $domain) {

	if ($domain == 'woocommerce') {

		switch ($text) {

			case 'IBAN':

			$translation = 'RIF';

			break;

			case 'BIC / Swift':

			$translation = 'Email';

			break;

			case 'BIC':

			$translation = 'Email';

			break;

			case 'View Cart':

			$translation = 'Ver Carrito';

			break;

		}

	}

	return $translation;

}

add_filter('gettext', 'wpse_77783_woo_bacs_ibn', 10, 3);





add_filter( 'woocommerce_sale_flash', 'wc_custom_replace_sale_text' );

function wc_custom_replace_sale_text( $html ) {

    return str_replace( __( 'Sale!', 'woocommerce' ), __( '¡Oferta!', 'woocommerce' ), $html );

}





remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );

add_action( 'woocommerce_proceed_to_checkout', 'cambia_texto_boton_ir_a_pagina_pago' );

function cambia_texto_boton_ir_a_pagina_pago() {



	$checkout_url = WC()->cart->get_checkout_url();



	?>

	<a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Realizar Pedido', 'woocommerce' ); ?></a>

	<?php

}



/*-----------------------------------------------------------------------------------*/

/* REISTRO DE MENUS */

/*-----------------------------------------------------------------------------------*/



if (function_exists( 'register_nav_menus' )) {

	register_nav_menus(array('primary-menu' => __( 'Primary Menu' )));

	register_nav_menus(array('secondary-menu' => __( 'Secondary Menu' )));

	register_nav_menus(array('third_menu' => __( 'Third Menu' )));

	register_nav_menus(array('footer-menu-uno' => __( 'Footer Menu uno' )));

	register_nav_menus(array('footer-menu-dos' => __( 'Footer Menu dos' )));

}





/*-----------------------------------------------------------------------------------*/

/* BREADCRUMB / MIGAS DE PAN */

/*-----------------------------------------------------------------------------------*/



function the_breadcrumb() {



	echo '<nav class="woocommerce-breadcrumb pages">';



	if (!is_home()) {

		echo '<a href="';

		echo get_option('home');

		echo '">';

		echo 'INICIO';

		echo "</a> / ";



		if (is_category() || is_single()) {

			the_category('');



			if (is_single()) {

				the_title();

			}



		} elseif (is_page()) {

			echo the_title();

		}

	}



	elseif (is_tag()) {single_tag_title();}

	elseif (is_day()) {echo"<li>Archivo para "; the_time('F jS, Y'); echo'</li>';}

	elseif (is_month()) {echo"<li>Archivo para "; the_time('F, Y'); echo'</li>';}

	elseif (is_year()) {echo"<li>Archivo para "; the_time('Y'); echo'</li>';}

	elseif (is_author()) {echo"<li>Archivo de Autor"; echo'</li>';}

	elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Archivos"; echo'</li>';}

	elseif (is_search()) {echo"<li>Resultados de Búsqueda"; echo'</li>';}



	echo '</nav>';

}





/*-----------------------------------------------------------------------------------*/

/* SOPORTE A WOOCOMMERCE */

/*-----------------------------------------------------------------------------------*/



add_action( 'after_setup_theme', 'woocommerce_support' );

function woocommerce_support() {

	add_theme_support( 'woocommerce' );

}





/*-----------------------------------------------------------------------------------*/

/* ADAPTAR WOOCOMMERCE AL THEME */

/*-----------------------------------------------------------------------------------*/



remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);

remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content',create_function('', ' echo "

	<section class=\"content-wrap\">

	<div class=\"container\">

	<div class=\"row\">

	<div class=\"col-xs-12 woocommerce\">

	";'), 10);

add_action('woocommerce_after_main_content', create_function('', 'echo "

	</div>

	</div>

	</div>

	</section>

	";'), 10);





/*-----------------------------------------------------------------------------------*/

/* QUITAR BARRA DE ADMINISTRACION EN EL SITIO */

/*-----------------------------------------------------------------------------------*/



function quitar_barra_administracion(){

	return false;

}

add_filter( 'show_admin_bar' , 'quitar_barra_administracion');





/*-----------------------------------------------------------------------------------*/

/* CSS CUSTOM DEL WP-ADMIN */

/*-----------------------------------------------------------------------------------*/



function Custom_Css_Admin() {

	echo '

	<style type="text/css">

	.updated.fade {

		display: none!important;

	}

	</style>

	';

}

add_action('admin_head', 'Custom_Css_Admin');





/*-----------------------------------------------------------------------------------*/

/* EVITAR ACTUALIZACION DE WP Y PLUGINS */

/*-----------------------------------------------------------------------------------*/



remove_action( 'load-update-core.php', 'wp_update_plugins' );

add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );



add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );

    // add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );

add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );







/*-----------------------------------------------------------------------------------*/

/* SUPRIMIR CAMPOS INNECESARIOS EN CHECKOUT WOOCOMMERCE */

/*-----------------------------------------------------------------------------------*/



add_filter( 'woocommerce_enable_order_notes_field', '__return false' );

add_filter( 'woocommerce_checkout_fields' , 'simplifica_pagina_de_pago' );

function simplifica_pagina_de_pago( $fields ) {



	unset( $fields['billing']['billing_company'] );

	unset( $fields['billing']['billing_country'] );

	unset( $fields['billing']['billing_state'] );

	unset( $fields['billing']['billing_city'] );

  // unset( $fields['billing']['billing_phone'] );

	unset( $fields['billing']['billing_postcode'] );

	unset( $fields['billing']['billing_address_2'] );

	unset( $fields['order']['order_comments'] );



	$fields['billing']['billing_email' ][ 'clear' ] = false;

	// $fields['billing']['billing_city' ][ 'class' ][0] = 'form-row-wide';

  // $fields['billing']['billing_postcode' ][ 'class' ][0] = 'form-row-last';



	return $fields;

}

/*-----------------------------------------------------------------------------------*/

/* DECLARACION DE NUEVOS CAMPOS FORMULARIO DE REGISTRO */

/*-----------------------------------------------------------------------------------*/


function wooc_extra_register_fields() {

	 global $wpdb;

	 /*$product = wc_get_product(  406  );
	 
	 $attachment_ids = $product->get_gallery_attachment_ids();

	 foreach( $attachment_ids as $attachment_id ) 
		{
		  echo $image_link = wp_get_attachment_url( $attachment_id );
		  
		}
	// Atributos de la imagen destacada (ruta, altura, anchura, boolean)
	/*globalglobal $product;
	 $product;
	$thumbID = get_post_thumbnail_id( 406 );
	$imgDestacada = wp_get_attachment_image_src( $thumbID, 'full' ); // thumbnail, medium, large, full
	echo 'Ruta: ' . $imgDestacada[0] . "\n";
	echo 'Altura: ' . $imgDestacada[1] . 'px.' . "\n";
	echo 'Anchura: ' . $imgDestacada[2] . 'px.' . "\n";
	echo 'Boolean: ' . $imgDestacada[3];
<img src="<?php echo $imgDestacada[0]*/

	 $query = "SELECT *
					FROM wp_users INNER JOIN wp_usermeta 
					ON wp_users.ID = wp_usermeta.user_id 
					WHERE wp_usermeta.meta_key = 'wp_capabilities' 
					AND wp_usermeta.meta_value LIKE '%vendor%' 
					ORDER BY RAND() LIMIT 1;";
	 $resultado = $wpdb->get_results($query);
     	
	  
    ?>
    
	
    <p class="form-row form-row-wide">
    	<input type="hidden" class="input-text" name="id_vendedor" id="reg_id_vendedor" value="<?php echo $resultado[0]->ID; ?>" />
    </p>

    <p class="form-row form-row-wide">
    	<label for="username"><?php _e( 'Name', 'woocommerce' ); ?> <span class="required">*</span></label>
    	<input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name"  required/>
    </p>

    <p class="form-row form-row-wide">
    	<label for="username"><?php _e( 'Last Name', 'woocommerce' ); ?> <span class="required">*</span></label>
    	<input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" required />
    </p>

    <?php
}

add_action( 'woocommerce_register_form_start', 'wooc_extra_register_fields' );

### GUARDAR NUEVOS CAMPOS FORMULARIO DE REGISTRO

function wooc_save_extra_register_fields( $customer_id ) {
   
    if ( isset( $_POST['id_vendedor'] ) ) {
        // WooCommerce billing vendedor
        update_user_meta( $customer_id, 'id_vendedor', sanitize_text_field( $_POST['id_vendedor'] ) );
    }

    if ( isset( $_POST['billing_first_name'] ) ) {
        // WooCommerce billing vendedor
        update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
    }

    if ( isset( $_POST['billing_last_name'] ) ) {
        // WooCommerce billing vendedor
        update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
    }

}

add_action( 'woocommerce_created_customer', 'wooc_save_extra_register_fields' );



add_filter( 'woocommerce_email_recipient_customer_invoice', 'woocommerce_email_customer_invoice_add_recipients' );
function woocommerce_email_customer_invoice_add_recipients( $recipient, $order ) {

 global $wpdb;

 $user = wp_get_current_user();
 
    
    $id_vendedor = get_user_meta($user->ID, 'id_vendedor', true); 

    $email_vendedor = get_user_meta($id_vendedor, 'billing_email', true);

    if(empty($email_vendedor)){
     $query = "SELECT * FROM wp_users WHERE ID = ".$id_vendedor;
  $resultado = $wpdb->get_results($query);
  $email_vendedor = $resultado[0]->user_email; 
    }

    $recipient .= ', '.$email_vendedor;
 
 return $recipient;
}



















 /*-----------------------------------------------------------------------------------*/

 /*- AGREGAR EMAIL -*/

/*-----------------------------------------------------------------------------------*/
add_filter( 'woocommerce_email_recipient_customer_completed_order', 'your_email_recipient_filter_function', 10, 2);

function your_email_recipient_filter_function($recipient, $object) {

 global $wpdb;

 $user = wp_get_current_user();
 
    
    $id_vendedor = get_user_meta($user->ID, 'id_vendedor', true); 

    $email_vendedor = get_user_meta($id_vendedor, 'billing_email', true);

    if(empty($email_vendedor)){
     $query = "SELECT * FROM wp_users WHERE ID = ".$id_vendedor;
  $resultado = $wpdb->get_results($query);
  $email_vendedor = $resultado[0]->user_email; 
    }

    $recipient = $recipient . ','.$email_vendedor;
    return $recipient;
}
 /*- AGREGAR EMAIL -*/






/*-----------------------------------------------------------------------------------*/

/* REDIRECIONAR A LOS USUARIOS */

/*-----------------------------------------------------------------------------------*/

function wc_custom_user_redirect( $redirect, $user ) {
	// Get the first of all the roles assigned to the user
	global $wpdb;

	$role = $user->roles[0];

	$dashboard = admin_url();

	$primera =  get_user_meta( $user->ID, 'primera_vez', true ); 
	
	if($primera==true||$primera==1||!empty($primera)){
		$myaccount = home_url().'/mi-cuenta/view-order/';
	}else{
		$myaccount = get_permalink(  547  );
		$wpdb->insert($wpdb->usermeta, array("user_id" => $user->ID, "meta_key" => "primera_vez", "meta_value" => 1), array("%d", "%s", "%d"));
	}



	if( $role == 'administrator' ) {
		//Redirect administrators to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'shop-manager' ) {
		//Redirect shop managers to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'editor' ) {
		//Redirect editors to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'author' ) {
		//Redirect authors to the dashboard
		$redirect = $dashboard;
	} elseif ( $role == 'customer' || $role == 'subscriber' ) {
		//Redirect customers and subscribers to the "My Account" page
		$redirect = $myaccount;
	} else {
		//Redirect any other role to the previous visited page or, if not available, to the home
		$redirect = wp_get_referer() ? wp_get_referer() : home_url();
	}
	return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'wc_custom_user_redirect', 10, 2 );

function plugin_registration_redirect() {
    $url = get_permalink(  547  );
    return $url;
}

add_filter( 'woocommerce_registration_redirect', 'plugin_registration_redirect' );





/*-----------------------------------------------------------------------------------*/

/* PETICION AJAX

/*-----------------------------------------------------------------------------------*/

add_action( 'wp_ajax_get_post_information', 'get_post_information' );

function get_post_information() 
{
	if(!empty($_POST['id_user'])){

		global $wpdb;
		$data = array();
	    $query = "SELECT * FROM wp_users WHERE wp_users.ID = ".$_POST['id_user'];
		
		$users = $wpdb->get_results($query);
		
		$id_vendedor = "";
		foreach ($users as $user) {
		 	 $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$user->ID;
			 $user_data = $wpdb->get_results($query);	 

			 foreach ($user_data as $r) {
			 	if($r->meta_key=="first_name"){
			 		$name=$r->meta_value;
			 	}
			 	if($r->meta_key=="last_name"){
			 		$last_name=$r->meta_value;
			 	}
			 	if($r->meta_key=="billing_phone"){
			 		$phone=$r->meta_value;
			 	}
			 	if($r->meta_key=="id_vendedor"){
			 		$id_vendedor=$r->meta_value;
			 	}
			 }
			 $data[]=array(
			 		'ID' =>  $user->ID,
			 		'user_nicename' => $user->user_nicename,
			 		'user_email' => $user->user_email,
			 		'first_name' => $name,
			 		'last_name'  => $last_name,
			 		'billing_phone'  => $phone,
			 		'id_vendedor' => $id_vendedor
			 	);
		 }

		 echo json_encode( $data );
	}

    die();
}

add_action( 'wp_ajax_guardar_info_vendedor', 'guardar_info_vendedor' );

function guardar_info_vendedor() 
{
	if(!empty($_POST['ID'])){

		    if ( isset( $_POST['first_name'] ) ) {
		        // WordPress default first name field.
		        update_user_meta( $_POST['ID'], 'first_name', sanitize_text_field( $_POST['first_name'] ) );

		        // WooCommerce billing first name.
		        update_user_meta( $_POST['ID'], 'billing_first_name', sanitize_text_field( $_POST['first_name'] ) );
		    }

		    if ( isset( $_POST['last_name'] ) ) {
		        // WordPress default last name field.
		        update_user_meta( $_POST['ID'], 'last_name', sanitize_text_field( $_POST['last_name'] ) );

		        // WooCommerce billing last name.
		        update_user_meta( $_POST['ID'], 'billing_last_name', sanitize_text_field( $_POST['last_name'] ) );
		    }

		    if ( isset( $_POST['billing_phone'] ) ) {
		        // WooCommerce billing phone
		        update_user_meta( $_POST['ID'], 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
		    }

		    if ( isset( $_POST['user_email'] ) ) {
		        // WooCommerce billing phone
		        wp_update_user( $_POST['ID'], 'user_email', sanitize_text_field( $_POST['user_email'] ) );

		        update_user_meta( $_POST['ID'], 'billing_email', sanitize_text_field( $_POST['user_email'] ) );
		    }
		   
		    if ( isset( $_POST['user_nicename'] ) ) { 
		        // WooCommerce billing state
		        wp_update_user( $_POST['ID'], 'user_nicename', sanitize_text_field( $_POST['user_nicename'] ) );

		        wp_update_user( $_POST['ID'], 'user_login', sanitize_text_field( $_POST['user_nicename'] ) );

		        update_user_meta( $_POST['ID'], 'nickname', sanitize_text_field( $_POST['user_nicename'] ) );
		    }
				 echo json_encode( "Listo" );
	}

    die();
}


add_action( 'wp_ajax_guardar_info_cliente', 'guardar_info_cliente' );

function guardar_info_cliente() 
{
	if(!empty($_POST['user_id'])){

		    if ( isset( $_POST['id_vendedor'] ) ) {
		        // WordPress default first name field.
		        update_user_meta( $_POST['user_id'], 'id_vendedor', sanitize_text_field( $_POST['id_vendedor'] ) );

		    }

			$direccion = home_url().'/mi-cuenta/view-order/';
			echo json_encode( $direccion );
	}

    die();
}
//-------------------------------------------------------------------------------------
function listar_vendedor() {
	global $wpdb;
	$data = array();
    $query = "SELECT *
					FROM wp_users INNER JOIN wp_usermeta 
					ON wp_users.ID = wp_usermeta.user_id 
					WHERE wp_usermeta.meta_key = 'wp_capabilities' 
					AND wp_usermeta.meta_value LIKE '%vendor%'";
	 $users = $wpdb->get_results($query);

	 foreach ($users as $user) {
	 	 $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$user->ID;
		 $user_data = $wpdb->get_results($query);	 

		 foreach ($user_data as $r) {
		 	if($r->meta_key=="first_name"){
		 		$name=$r->meta_value;
		 	}
		 	if($r->meta_key=="last_name"){
		 		$last_name=$r->meta_value;
		 	}
		 	if($r->meta_key=="billing_phone"){
		 		$phone=$r->meta_value;
		 	}
		 	
		 }
		 $data[]=array(
		 		'ID' =>  $user->ID,
		 		'user_nicename' => $user->user_nicename,
		 		'user_email' => $user->user_email,
		 		'first_name' => $name,
		 		'last_name'  => $last_name,
		 		'billing_phone'  => $phone,
		 	);
	 }
	 return $data;
}

//--------------------------------------------------------------------------------------

function listar_comprador() {
	global $wpdb;
	$data = array();
    $query = "SELECT *
					FROM wp_users INNER JOIN wp_usermeta 
					ON wp_users.ID = wp_usermeta.user_id 
					WHERE wp_usermeta.meta_key = 'wp_capabilities' 
					AND (wp_usermeta.meta_value LIKE '%subscriber%' OR wp_usermeta.meta_value LIKE '%customer%')";
	 $users = $wpdb->get_results($query);

	 $name = "";
	 $last_name = "";
	 $phone = "";
	 $id_vendedo = "";
	 $name_vendedo = "";
	 $last_name_vendedor ="";
	 
	 foreach ($users as $user) {
	 	 $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$user->ID;
		 $user_data = $wpdb->get_results($query);	 

		 foreach ($user_data as $r) {
		 	if($r->meta_key=="first_name"||$r->meta_key=="billing_first_name"){
		 		$name=$r->meta_value;

		 	}
		 	if($r->meta_key=="last_name"||$r->meta_key=="billing_last_name"){
		 		$last_name=$r->meta_value;
		 	}
		 	if($r->meta_key=="billing_phone"){
		 		$phone=$r->meta_value;
		 	}
		 	if($r->meta_key=="id_vendedor"){
		 		$id_vendedor=$r->meta_value;
		 	}
		 }

		$query_vendedor = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$id_vendedor;

 		$vendedor_data = $wpdb->get_results($query_vendedor);

 		foreach ($vendedor_data as $v) {
		 	if($v->meta_key=="first_name"){
		 		$name_vendedor=$v->meta_value;
		 	}
		 	if($v->meta_key=="last_name"){
		 		$last_name_vendedor=$v->meta_value;
		 	}
		 }

		 $data[]=array(
		 		'ID' =>  $user->ID,
		 		'user_nicename' => $user->user_nicename,
		 		'user_email' => $user->user_email,
		 		'first_name' => $name,
		 		'last_name'  => $last_name,
		 		'billing_phone'  => $phone,
		 		'id_vendedor' => $id_vendedor,
		 		'vendedor' => $name_vendedor.' '.$last_name_vendedor
		 	);
	 }
	 return $data;
}

function listar_historial() {
	global $wpdb;
	$data = array();
    $query = "SELECT *
				FROM
				    wp_historico_orden
				    INNER JOIN wp_users 
				        ON (wp_historico_orden.user_id = wp_users.ID);";


	 $histo = $wpdb->get_results($query);

	 $name = "";
	 $last_name = "";
	 $phone = "";
	 $id_vendedo = "";
	 $name_vendedo = "";
	 $last_name_vendedor ="";
	 
	 foreach ($histo as $h) {
	 	 $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$h->user_id;
		 $user_data = $wpdb->get_results($query);	 

		 foreach ($user_data as $r) {
		 	if($r->meta_key=="first_name"||$r->meta_key=="billing_first_name"){
		 		$name=$r->meta_value;

		 	}
		 	if($r->meta_key=="last_name"||$r->meta_key=="billing_last_name"){
		 		$last_name=$r->meta_value;
		 	}
		 	if($r->meta_key=="billing_phone"){
		 		$phone=$r->meta_value;
		 	}
		 	if($r->meta_key=="id_vendedor"){
		 		$id_vendedor=$r->meta_value;
		 	}
		 }

		$query_vendedor = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$id_vendedor;

 		$vendedor_data = $wpdb->get_results($query_vendedor);

 		foreach ($vendedor_data as $v) {
		 	if($v->meta_key=="first_name"){
		 		$name_vendedor=$v->meta_value;
		 	}
		 	if($v->meta_key=="last_name"){
		 		$last_name_vendedor=$v->meta_value;
		 	}
		 }

		 $data[]=array(
		 		'order_id' => $h->order_id,
		 		'date' => $h->date,
		 		'total' => $h->total,
		 		'payment_method' => $h->payment_method,
		 		'user_nicename' => $h->user_nicename,
		 		'user_email' => $h->user_email,
		 		'first_name' => $name,
		 		'last_name'  => $last_name,
		 		'billing_phone'  => $phone,
		 		'id_vendedor' => $id_vendedor,
		 		'vendedor' => $name_vendedor.' '.$last_name_vendedor
		 	);
	 }
	 return $data;
}

//--------------------------------------------------------------------------------------

/*-----------------------------------------------------------------------------------*/

/* FIN PETICION AJAX

/*-----------------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------------*/

/* Funciones de auxiliares

/*-----------------------------------------------------------------------------------*/

function getNameVendedor($user_id){
	
    $key = 'last_name';
    
    $user_last = get_user_meta( $user_id,$key); 

    $key = 'first_name';
    $user_firs = get_user_meta( $user_id,$key); 
    
    $name_full = $user_firs[0].' '.$user_last[0];
    
    return $name_full;
   
}

function getIdVendedor($user_id){

    $key = 'id_vendedor';
    
    $id_vendedor = get_user_meta($user_id, $key); 
    
    return $id_vendedor;
   
}

/*-----------------------------------------------------------------------------------*/

/* FIN Funciones de auxiliares

/*-----------------------------------------------------------------------------------*/





//Añade nofollow a todos los enlaces externos



add_filter('the_content', 'auto_nofollow');

function auto_nofollow($content) {

    //return stripslashes(wp_rel_nofollow($content));

    return preg_replace_callback('/<a>]+/', 'auto_nofollow_callback', $content);

}

function auto_nofollow_callback($matches) {

    $link = $matches[0];

    $site_link = get_bloginfo('url');

    if (strpos($link, 'rel') === false) {

        $link = preg_replace("%(href=S(?!$site_link))%i", 'rel="nofollow" $1', $link);

    } elseif (preg_match("%href=S(?!$site_link)%i", $link)) {

        $link = preg_replace('/rel=S(?!nofollow)S*/i', 'rel="nofollow"', $link);

    }

    return $link;



}



add_theme_support( 'genesis-connect-woocommerce' );

/*-----------------------------------------------------------------------------------*/

/*                          DESARROLLO PRECIOS                                       */

/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/

/* GET CATEGORIAS */

/*-----------------------------------------------------------------------------------*/
function categorias(){
	 $taxonomy     = 'product_cat';
	  $orderby      = 'name';  
	  $show_count   = 0;      // 1 for yes, 0 for no
	  $pad_counts   = 0;      // 1 for yes, 0 for no
	  $hierarchical = 1;      // 1 for yes, 0 for no  
	  $title        = '';  
	  $empty        = 0;

	  $args = array(
	         'taxonomy'     => $taxonomy,
	         'orderby'      => $orderby,
	         'show_count'   => $show_count,
	         'pad_counts'   => $pad_counts,
	         'hierarchical' => $hierarchical,
	         'title_li'     => $title,
	         'hide_empty'   => $empty
	  );
	 $all_categories = get_categories( $args );


	 /*foreach ($all_categories as $cat) {
	    if($cat->category_parent == 0) {
	        $category_id = $cat->term_id;       
	        echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .$category_id.'</a>';

	        $args2 = array(
	                'taxonomy'     => $taxonomy,
	                'child_of'     => 0,
	                'parent'       => $category_id,
	                'orderby'      => $orderby,
	                'show_count'   => $show_count,
	                'pad_counts'   => $pad_counts,
	                'hierarchical' => $hierarchical,
	                'title_li'     => $title,
	                'hide_empty'   => $empty
	        );
	        $sub_cats = get_categories( $args2 );
	        if($sub_cats) {
	            foreach($sub_cats as $sub_category) {
	                echo  $sub_category->name ;
	            }   
	        }
	    }       
	}*/
	return $all_categories;
}

/*-----------------------------------------------------------------------------------*/

/* GET ETIQUETAS */

/*-----------------------------------------------------------------------------------*/
function etiquetas(){
	 
	 $all_etiquetas = get_terms( 'product_tag' );;


	return $all_etiquetas;
}
/*-----------------------------------------------------------------------------------*/

/* PETICION AJAX PRECIOS

/*-----------------------------------------------------------------------------------*/
add_action( 'wp_ajax_cargar_data_table', 'cargar_data_table' );

function cargar_data_table() 
{	
	$args = array('post_type'=> 'product','posts_per_page' => -1);
	$products = get_posts( $args ); 
	$data=array();
	
	foreach ($products  as $product) {
		$_product = wc_get_product( $product->ID);
		$d['moneda'] = get_woocommerce_currency_symbol();

		$d['id']=$product->ID;
	 	$d['producto']=get_post_meta($product->ID,'titulo_corto',true);
	 	$d['categoria']=$_product->get_tags();
	 	$d['precio_regular']=get_post_meta($product->ID,'_regular_price',true); 
	 	$d['precio_venta'] = get_post_meta($product->ID,'_sale_price',true);

	 	if($d['precio_venta'] == "" ){
	 		$d['precio_venta'] = 0;
	 	}

	 	if($d['precio_regular'] == "" ){
	 		$d['precio_regular'] = 0;
	 	}

	 	$d['accion']='<button class="btn btn-success btn-block btn-xs" data-toggle="modal" data-target="#modal-id" 
						onclick="cargarProducto('. $product->ID.','."'".$d['producto']."'".','.$d['precio_regular'].','.$d['precio_venta'].')">Cambiar Precio</button>';
	 	$data[] = $d;
	} 
	
	echo '{"data":'.json_encode( $data ).'}';
	

    die();
}


add_action( 'wp_ajax_cambio_de_precio_1', 'cambio_de_precio_1' );

function cambio_de_precio_1() 
{
	if(!empty($_POST))
	{
		
		$args     = array( 'post_type' => 'product','posts_per_page' => -1);
		
		$negativo = 0;

		$products = get_posts( $args ); 

		if($_POST['tipo']==1)
		{
			$porcentaje = $_POST['valor']/100;
			foreach ($products as $product) {
			  	//$_product = wc_get_product( $product->ID);
			  	$precio= get_post_meta($product->ID,'_price',true);
			  	$precio_sale= get_post_meta($product->ID,'_sale_price',true);
			  	$precio_regular= get_post_meta($product->ID,'_regular_price',true);

			  	if($_POST['accion']=='sumar')
				{
					$precio        = round($precio+($precio*$porcentaje), 0, PHP_ROUND_HALF_UP);
			  		$precio_sale   = round($precio_sale+($precio_sale*$porcentaje), 0, PHP_ROUND_HALF_UP);
			  		$precio_regular= round($precio_regular+($precio_regular*$porcentaje), 0, PHP_ROUND_HALF_UP);
				}
				else
				{
					$precio        = round($precio-($precio*$porcentaje), 0, PHP_ROUND_HALF_UP);
			  		$precio_sale   = round($precio_sale-($precio_sale*$porcentaje), 0, PHP_ROUND_HALF_UP);
			  		$precio_regular= round($precio_regular-($precio_regular*$porcentaje), 0, PHP_ROUND_HALF_UP);
				}
			  	

			  	
			  	if($precio>=0){
			  		update_post_meta($product->ID, '_price', $precio );
			  	}else{
			  		$negativo++;
			  	}

			  	if($precio_sale>=0){
			  		update_post_meta($product->ID, '_sale_price', $precio_sale );
			  	}else{
			  		$negativo++;
			  	}
			  	
			  	if($precio_regular>=0){
			  		update_post_meta($product->ID, '_regular_price', $precio_regular );
			  	}else{
			  		$negativo++;
			  	}
			 }
		}
		else
		{
			foreach ($products as $product) {
			  	
			  	$precio= get_post_meta($product->ID,'_price',true);
			  	$precio_sale= get_post_meta($product->ID,'_sale_price',true);
			  	$precio_regular= get_post_meta($product->ID,'_regular_price',true);


			  	if($_POST['accion']=='sumar')
				{
				  	$precio        = round($precio+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				  	$precio_sale   = round($precio_sale+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				  	$precio_regular= round($precio_regular+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				}
				elseif($_POST['accion']=='restar')
				{
				  	$precio        = round($precio-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				  	$precio_sale   = round($precio_sale-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				  	$precio_regular= round($precio_regular-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				}
				else
				{
					$precio        = round(($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				  	$precio_sale   = round(($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				  	$precio_regular= round(($_POST['valor']), 0, PHP_ROUND_HALF_UP);
				}

				if($precio>=0){
			  		update_post_meta($product->ID, '_price', $precio );
			  	}else{
			  		$negativo++;
			  	}
			  	if($precio_sale>=0){
			  		update_post_meta($product->ID, '_sale_price', $precio_sale );
			  	}else{
			  		$negativo++;
			  	}
			  	if($precio_regular>=0){
			  		update_post_meta($product->ID, '_regular_price', $precio_regular );
			  	}else{
			  		$negativo++;
			  	}

			  	
			  	
			 }

			 
		}

		if($negativo!=0){
			echo '{"success":false}';
		}else{
			echo '{"success":true}';
		}
		
	}

    die();
}

add_action( 'wp_ajax_cambio_de_precio_2', 'cambio_de_precio_2' );

function cambio_de_precio_2() 
{
	if(!empty($_POST))
	{
		$contador = 0;
		$negativo = 0;
		$catorias_no_encontradas = array();
		
			$args = array( 
  				'post_type' 	 => 'product',
  				'posts_per_page' => -1,
  				'tax_query'      => array(
				        array(
				            'taxonomy' => 'product_tag',
				            'field'    => 'term_id', //This is optional, as it defaults to 'term_id'
				            'terms'     => $_POST['categoria']
				        )
			));

			$products = get_posts( $args ); 
			

			if(empty($products)){
				$contador++;
				$cat =  $term = get_term_by( 'id', $_POST['categoria'], 'product_tag', 'ARRAY_A');
	    		$catorias_no_encontradas[] = "<li class='list-group-item'>".$cat['name']."</li>";

			}

			if($_POST['tipo']==1)
			{
				
				$porcentaje = $_POST['valor']/100;
				foreach ($products as $product) {
					
				  	$precio= get_post_meta($product->ID,'_price',true);
				  	$precio_sale= get_post_meta($product->ID,'_sale_price',true);
				  	$precio_regular= get_post_meta($product->ID,'_regular_price',true);

				  	if($_POST['accion']=='sumar')
					{
						$precio        = round($precio+($precio*$porcentaje), 0, PHP_ROUND_HALF_UP);
				  		$precio_sale   = round($precio_sale+($precio_sale*$porcentaje), 0, PHP_ROUND_HALF_UP);
				  		$precio_regular= round($precio_regular+($precio_regular*$porcentaje), 0, PHP_ROUND_HALF_UP);
					}
					else
					{
						$precio        = round($precio-($precio*$porcentaje), 0, PHP_ROUND_HALF_UP);
				  		$precio_sale   = round($precio_sale-($precio_sale*$porcentaje), 0, PHP_ROUND_HALF_UP);
				  		$precio_regular= round($precio_regular-($precio_regular*$porcentaje), 0, PHP_ROUND_HALF_UP);
					}
				  	

				  	
				  	if($precio>=0){
				  		update_post_meta($product->ID, '_price', $precio );
				  	}else{
				  		$negativo++;
				  	}

				  	if($precio_sale>=0){
				  		update_post_meta($product->ID, '_sale_price', $precio_sale );
				  	}else{
				  		$negativo++;
				  	}
				  	
				  	if($precio_regular>=0){
				  		update_post_meta($product->ID, '_regular_price', $precio_regular );
				  	}else{
				  		$negativo++;
				  	}

				  	
				 }
			}
			else
			{
				
				foreach ($products as $product) {
				  	
				  	$precio= get_post_meta($product->ID,'_price',true);
				  	$precio_sale= get_post_meta($product->ID,'_sale_price',true);
				  	$precio_regular= get_post_meta($product->ID,'_regular_price',true);


				  	if($_POST['accion']=='sumar')
					{
					  	$precio        = round($precio+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					  	$precio_sale   = round($precio_sale+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					  	$precio_regular= round($precio_regular+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					}
					elseif($_POST['accion']=='restar')
					{
					  	$precio        = round($precio-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					  	$precio_sale   = round($precio_sale-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					  	$precio_regular= round($precio_regular-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					}
					else
					{
						$precio        = round(($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					  	$precio_sale   = round(($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					  	$precio_regular= round(($_POST['valor']), 0, PHP_ROUND_HALF_UP);
					}


				  	if($precio>=0){
				  		update_post_meta($product->ID, '_price', $precio );
				  	}else{
				  		$negativo++;
				  	}


				  	if($precio_sale>=0){
				  		update_post_meta($product->ID, '_sale_price', $precio_sale );
				  	}else{
				  		$negativo++;
				  	}

				  	if($precio_regular>=0){
				  		update_post_meta($product->ID, '_regular_price', $precio_regular );
				  	}else{
				  		$negativo++;
				  	}
				  	

				  	
				 }

				 
			
			
		}
		$text_cat="";
		$text_neg="";
		if($contador!=0){
			$text_cat =$text_cat."<ul class='list-group'>";
			foreach ($catorias_no_encontradas as $c) {
				$text_cat = $text_cat.$c;
			}
			$text_cat=$text_cat."</ul>";
		}

		if($negativo!=0){
			$text_neg =$text_neg."No se hará la modificación de precios a los productos cuya actualización en el precio den un monto negativo.";
			
		}

		
		if($text_cat!=""||$text_neg!=""){
			echo '{"success":false,"categorias":'.json_encode($text_cat).',"negativo":'.json_encode($text_neg).'}';
		}else{
			echo '{"success":true}';
		}
		
	}


    die();
}

add_action( 'wp_ajax_cambio_de_precio_3', 'cambio_de_precio_3' );

function cambio_de_precio_3() 
{
	if(!empty($_POST))
	{
		
		

		if($_POST['tipo']==1)
		{
			$porcentaje = $_POST['valor']/100;
			
		  	//$_product = wc_get_product( $product->ID);
		  	$precio= get_post_meta($_POST['id_producto'],'_price',true);
		  	$precio_sale= get_post_meta($_POST['id_producto'],'_sale_price',true);
		  	$precio_regular= get_post_meta($_POST['id_producto'],'_regular_price',true);

		  	if($_POST['accion']=='sumar')
			{
				$precio        = round($precio+($precio*$porcentaje), 0, PHP_ROUND_HALF_UP);
		  		$precio_sale   = round($precio_sale+($precio_sale*$porcentaje), 0, PHP_ROUND_HALF_UP);
		  		$precio_regular= round($precio_regular+($precio_regular*$porcentaje), 0, PHP_ROUND_HALF_UP);
			}
			else
			{
				$precio        = round($precio-($precio*$porcentaje), 0, PHP_ROUND_HALF_UP);
		  		$precio_sale   = round($precio_sale-($precio_sale*$porcentaje), 0, PHP_ROUND_HALF_UP);
		  		$precio_regular= round($precio_regular-($precio_regular*$porcentaje), 0, PHP_ROUND_HALF_UP);
			}
		  	

		  	
		  	if($precio>=0){
		  		update_post_meta($_POST['id_producto'], '_price', $precio );
		  	}else{
		  		$negativo++;
		  	}
		  	if($precio_sale>=0){
		  		update_post_meta($_POST['id_producto'], '_sale_price', $precio_sale );
		  	}else{
		  		$negativo++;
		  	}
		  	if($precio_regular>=0){
		  		update_post_meta($_POST['id_producto'], '_regular_price', $precio_regular );
		  	}else{
		  		$negativo++;
		  	}
			 
		}
		else
		{
			
		  	//$_product = wc_get_product( $product->ID);
		  	$precio= get_post_meta($_POST['id_producto'],'_price',true);
		  	$precio_sale= get_post_meta($_POST['id_producto'],'_sale_price',true);
		  	$precio_regular= get_post_meta($_POST['id_producto'],'_regular_price',true);

		  	if($_POST['accion']=='sumar')
			{
			  	$precio        = round($precio+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
			  	$precio_sale   = round($precio_sale+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
			  	$precio_regular= round($precio_regular+($_POST['valor']), 0, PHP_ROUND_HALF_UP);
			}
			else
			{
			  	$precio        = round($precio-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
			  	$precio_sale   = round($precio_sale-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
			  	$precio_regular= round($precio_regular-($_POST['valor']), 0, PHP_ROUND_HALF_UP);
			}

			if($precio>=0){
		  		update_post_meta($_POST['id_producto'], '_price', $precio );
		  	}else{
		  		$negativo++;
		  	}
		  	if($precio_sale>=0){
		  		update_post_meta($_POST['id_producto'], '_sale_price', $precio_sale );
		  	}else{
		  		$negativo++;
		  	}
		  	if($precio_regular>=0){
		  		update_post_meta($_POST['id_producto'], '_regular_price', $precio_regular );
		  	}else{
		  		$negativo++;
		  	}
			 

			 
		}
		if($negativo!=0){
			echo '{"success":false}';
		}else{
			echo '{"success":true}';
		}
	}

    die();
}

add_action( 'wp_ajax_cambio_de_precio_unico', 'cambio_de_precio_unico' );

function cambio_de_precio_unico() 
{
	
	if(!empty($_POST))
	{
		
		
		if(!empty($_POST['precio_regular'])){

			update_post_meta($_POST['id_producto'], '_regular_price', $_POST['precio_regular'] );
		}

		if(!empty($_POST['precio_venta'])){

			update_post_meta($_POST['id_producto'], '_price',  $_POST['precio_venta'] );
			update_post_meta($_POST['id_producto'], '_sale_price',  $_POST['precio_venta'] );
		}
		
		echo json_encode( 'Listo' );


	}


    die();
}

/*-----------------------------------------------------------------------------------*/

	/* PETICION AJAX MODULO VENDEDOR

	/*-----------------------------------------------------------------------------------*/


	add_action( 'wp_ajax_listar_clientes', 'listar_clientes' );

	function listar_clientes() 
	{	
		global $wpdb;
		$data = array();
	    $query = "SELECT *
						FROM wp_users INNER JOIN wp_usermeta 
						ON wp_users.ID = wp_usermeta.user_id 
						WHERE wp_usermeta.meta_key = 'wp_capabilities' 
						AND (wp_usermeta.meta_value LIKE '%subscriber%' OR wp_usermeta.meta_value LIKE '%customer%')";
		 $users = $wpdb->get_results($query);

		 $name = "";
		 $last_name = "";
		 $phone = "";
		 $id_vendedo = "";
		 $name_vendedo = "";
		 $last_name_vendedor ="";
		 
		 foreach ($users as $user) {
		 	 $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$user->ID;
			 $user_data = $wpdb->get_results($query);	 

			 foreach ($user_data as $r) {
			 	if($r->meta_key=="first_name"||$r->meta_key=="billing_first_name"){
			 		$name=$r->meta_value;

			 	}
			 	if($r->meta_key=="last_name"||$r->meta_key=="billing_last_name"){
			 		$last_name=$r->meta_value;
			 	}
			 	if($r->meta_key=="billing_phone"){
			 		$phone=$r->meta_value;
			 	}
			 	if($r->meta_key=="id_vendedor"){
			 		$id_vendedor=$r->meta_value;
			 	}
			 }

			$query_vendedor = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$id_vendedor;

	 		$vendedor_data = $wpdb->get_results($query_vendedor);

	 		foreach ($vendedor_data as $v) {
			 	if($v->meta_key=="first_name"){
			 		$name_vendedor=$v->meta_value;
			 	}
			 	if($v->meta_key=="last_name"){
			 		$last_name_vendedor=$v->meta_value;
			 	}
			 }

			 $data[]=array(
			 		'ID' =>  $user->ID,
			 		'user_nicename' => $user->user_nicename,
			 		'user_email' => $user->user_email,
			 		'nombre_completo' => $name.' '.$last_name,
			 		'billing_phone'  => $phone,
			 		'id_vendedor' => $id_vendedor,
			 		'vendedor' => $name_vendedor.' '.$last_name_vendedor
			 	);
		 }
	 

		echo '{"data":'.json_encode( $data ).'}';


		die();
	}



	add_action( 'wp_ajax_listar_ordenes', 'listar_ordenes' );
	function listar_ordenes( ) {

		$args = array('post_type'=> 'shop_order','posts_per_page' => -1);
		$orders = get_posts( $args );
		$data = array();
		$user = wp_get_current_user();


		global $current_user, $wpdb;
		$role = $wpdb->prefix . 'capabilities';
		$current_user->role = array_keys($current_user->$role);
		$role = $current_user->role[0];


		foreach ($orders as $o) {
			
			$order = new WC_Order($o->ID);
			//		print_r($o);
			$data[] = array(
						"order_id" => $order->get_order_number( ), 
						"cliente"  => get_user_meta($order->get_user_id( ), 'nickname', true), 
						"status" => wc_get_order_status_name( $order->get_status() ),
						"date" => date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ),
						"total" => $order->get_formatted_order_total(),
						"payment_method"=>$order->payment_method_title,
						"vendedor" =>  get_user_meta(get_user_meta($order->get_user_id( ), 'id_vendedor', true), 'first_name', true)." ".get_user_meta(get_user_meta($order->get_user_id( ), 'id_vendedor', true), 'last_name', true),
						"id_vendedor" => get_user_meta($order->get_user_id( ), 'id_vendedor', true),
						"id_user_actual" => $user->ID,

						"accion_ver" => "<button class='btn btn-info btn-xs' data-toggle='modal' href='#ver-orden' onclick='cargarOrden(".$order->get_order_number( ).",".'"'.wc_get_order_status_name( $order->get_status()).'"'.")' data-toggle='tooltip' title='Detalles de la orden'><i class='fa fa-search-plus' aria-hidden='true' ></i></button>",

						"accion_factura" => "<button class='btn btn-success btn-xs'  data-toggle='modal' href='#cargar-factura' onclick='cargarPedido(".$order->get_order_number( ).",".'"'.wc_get_order_status_name( $order->get_status()).'"'.")' data-toggle='tooltip' title='Cargar datos de facturación / Enviar a despacho'><i class='fa fa-pencil' aria-hidden='true'></i></button>",

						"accion_mensaje" => "<button class='btn btn-default btn-xs'  data-toggle='modal' href='#cargar-observacion' onclick='cargarObservacion(".$order->get_order_number( ).")' data-toggle='tooltip' title='Enviar observación al Cliente'><i class='fa fa-user' aria-hidden='true'></i></button>",

						"accion_notificar" => "<button class='btn btn-warning btn-xs' onclick='notificarCliente(".$order->get_order_number( ).")' data-toggle='modal' href='#confirmar' toggle='tooltip' title='Notificar al cliente'><i class='fa fa-paper-plane' aria-hidden='true'></i></button>",

						"role" => $role
					);
			$items = $order->get_formatted_order_total();
			
		}
		echo '{"data":'.json_encode( $data ).'}';
		die();	
	}

	function listar_ordenes_reportes($inicio,$fin) {
		global $current_user, $wpdb;
		$query = "SELECT * FROM $wpdb->posts WHERE CAST(post_date AS DATE) >= '".$inicio."' AND CAST(post_date AS DATE)<= '".$fin."' AND post_type='shop_order' ORDER BY post_date DESC";
		
		$orders = $wpdb->get_results($query);
		$data = array();
		$user = wp_get_current_user();

		
		$role = $wpdb->prefix . 'capabilities';
		$current_user->role = array_keys($current_user->$role);
		$role = $current_user->role[0];


		foreach ($orders as $o) {
			
			$order = new WC_Order($o->ID);
			//		print_r($o);
			$data[] = array(
						"order_id" => $order->get_order_number( ), 
						"cliente"  => get_user_meta($order->get_user_id( ), 'nickname', true), 
						"status" => wc_get_order_status_name( $order->get_status() ),
						"date" => date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ),
						"total" => $order->get_formatted_order_total(),
						"payment_method"=>$order->payment_method_title,
						"vendedor" =>  get_user_meta(get_user_meta($order->get_user_id( ), 'id_vendedor', true), 'first_name', true)." ".get_user_meta(get_user_meta($order->get_user_id( ), 'id_vendedor', true), 'last_name', true),
						'num_factura' => get_post_meta($o->ID,'num_factura',true),
						'fecha_factura' => get_post_meta($o->ID,'fecha_factura',true),
						'empresa_envio' => get_post_meta($o->ID,'empresa_envio',true),
						'monto_seguro' => get_post_meta($o->ID,'monto_seguro',true),
						'obj_orden' => get_post_meta($o->ID,'obj_orden',true),
						'email_cliente' => $order->billing_email,
						'telefono_cliente' =>$order->billing_phone,
						'direccion_envio'=> $order->get_formatted_billing_address(),
						'numero_de_guia'=>get_post_meta($o->ID,'numero_de_guia',true),
						'fecha_despacho' =>get_post_meta($o->ID,'fecha_despacho',true)
					);
			$items = $order->get_formatted_order_total();
	
		}
		return $data;	
	}

	add_action('wp_ajax_detalles_orden','detalles_orden');

	function detalles_orden($id = ""){
		$id_order = "";
		if($id!=""){
			$id_order = $id;
		}else{
			$id_order = $_GET['order_id'];
		}
		$order = new WC_Order($id_order);

		$html = "<!DOCTYPE html>
		<html>
		<head>
			<title>PDF</title>			
			<style>

			</style>
		</head>
		<body>
		<div class='table-responsive'>
					<table class='table order_details' align='lef' border='1px'>
						<thead>
							<tr>
								<th class='product-name'>Producto</th>
								<th class='product-total'>Total</th>
							</tr>
						</thead>
						<tbody>";
							
								
		foreach( $order->get_items() as $item_id => $item ) {
			
			


			$html.="<tr'>
				    <td class='product-name' align='lef'>";
					
						

						$html.= $item['name'];
						$html.= $item['qty'];

						
					
			$html.="</td>
				<td class='product-total'>";
			$html.=$order->get_formatted_line_subtotal( $item );
			$html.=	"</td>
			</tr>";

			if ( $order->has_status( array( 'completed', 'processing' ) ) && ( $purchase_note = get_post_meta( $product->id, '_purchase_note', true ) ) ) {
			
			$html.="<tr class='product-purchase-note'>
				<td colspan='3'>";
			$html.=wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) );
			$html.="</td>
			</tr>";
			}
		}
							
		$html.=do_action( 'woocommerce_order_items_table', $order ); 

		$html.="</tbody>
						<tfoot>";
		
		foreach ( $order->get_order_item_totals() as $key => $total ) {
			
			$html.="<tr>
				<th scope='row' align='lef'>".$total['label']."</th>
				<td>".$total['value']."</td>
			</tr>";
			
		}

		$html.="<tr>
				<th scope='row' align='lef'>Numero de Factura</th>
				<td>".get_post_meta($id_order,'num_factura',true)."</td>
			</tr>";

		$html.="<tr>
				<th scope='row' align='lef'>Fecha de facturación</th>
				<td>".get_post_meta($id_order,'fecha_factura',true)."</td>
			</tr>";
		
		$html.="</tfoot>
			</table>
		</div>";

		$html.= "<header><h2 style='color: black'>Detalle del cliente</h2></header>";
		

		$html.="<table class='table table-bordered ' border='1px'>";
		if ( $order->customer_note ) {
			$html.="<tr>
				<th align='lef'>Nota: </th>
				<td>".wptexturize( $order->customer_note )."</td>
			</tr>";
		}

		if ( $order->billing_email ){
			$html.="<tr>
				<th align='lef'>Email: </th>
				<td>".esc_html( $order->billing_email )."</td>
			</tr>";
		}

		if ( $order->billing_phone ){
			$html.="<tr>
				<th>Telefono:</th>
				<td>".esc_html( $order->billing_phone )."</td>
			</tr>";		
		}

		
		$html.="</table>";

		if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() ) {

			$html.="<div class='col2-set addresses'>
				<div class='col-1'>";

			}

				$html.=" <header class='title'>
					<h3 style='color: black;'>Dirección de Envio</h3>
				</header>
				<address>";
				$html.=( $address = $order->get_formatted_billing_address() ) ? $address : __( 'N/A', 'woocommerce' );
				$html.="</address>";

				$empresa = get_post_meta($id_order,'empresa_envio',true);
				$html.="<address>
					Empresa de encomienda: <strong>".($empresa==""?"No Asignado":$empresa)."</strong>
				</address>";


				$numero_de_guia = get_post_meta($id_order,'numero_de_guia',true);
				$html.="<address>
					Número de guía: <strong>".($numero_de_guia==""?"No Asignado":$numero_de_guia)."</strong>
				</address>";


				$monto_seguro = get_post_meta($id_order,'monto_seguro',true);
				$html.="<address>
					Monto a asegurar: <strong>".($monto_seguro==""?"No Asignado":$monto_seguro)."</strong>
				</address>";


				$fecha_despacho = get_post_meta($id_order,'fecha_despacho',true);
				$html.="<address>
					Fecha de despacho: <strong>".($fecha_despacho==""?"No Asignado":$fecha_despacho)."</strong>
				</address>";
				if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() ) {

			$html.="</div>
			<div class='col-2'>
				<header class='title'>
					<h3>Dirección de Envio</h3>
				</header>
				<address>
					".( $address = $order->get_formatted_shipping_address() ) ? $address : __( 'N/A', 'woocommerce' )."
				</address>
			</div>
		</div>

		</body>
		</html>";

		}

		if($id!=""){
			return $html;
		}else{
			echo json_encode( $html );
		}
		
		die();
	}

	add_action('wp_ajax_get_factura_pedido','get_factura_pedido');

	function get_factura_pedido(){


		$id_post = $_GET['order_id'];
		$order = new WC_Order($id_post);

		$data = array(
			'num_factura' => get_post_meta($id_post,'num_factura',true),
			'fecha_factura' => get_post_meta($id_post,'fecha_factura',true),
			'empresa_envio' => get_post_meta($id_post,'empresa_envio',true),
			'monto_seguro' => get_post_meta($id_post,'monto_seguro',true),
			'obj_orden' => get_post_meta($id_post,'obj_orden',true),
			'numero_de_guia' => get_post_meta($id_post,'numero_de_guia',true),
			'fecha_despacho' => get_post_meta($id_post,'fecha_despacho',true),
			'email_cliente'  => $order->billing_email,
			'nombre_cliente' => get_user_meta($order->get_user_id(), 'billing_first_name', true).' '.get_user_meta($order->get_user_id(), 'billing_last_name', true),
		);

		$order = new WC_Order();


		echo json_encode( $data );
		die();
	}

	add_action('wp_ajax_set_factura_pedido','set_factura_pedido');

	function set_factura_pedido(){

		
		$id_post = $_POST['post_id'];

		$valido = "si";

		$args = array(
					'post_type'  =>  'shop_order',
					'meta_key'   => 'num_factura',
					'meta_value' => $_POST['num_factura']
					
		);

		$o= get_posts( $args );

		if(!empty($o) && count($o)==1 && $o[0]->ID == $_POST['post_id'])
		{
			update_post_meta($id_post,'num_factura',$_POST['num_factura']);
		}
		elseif(empty($o))
		{
			update_post_meta($id_post,'num_factura',$_POST['num_factura']);
		}
		else
		{
			$valido = "no";
		}


		$order = new WC_Order($id_post);
		
		if($order->get_status()=="on-hold")
		{
			$order->update_status('processing');
		}
		
		if($valido == "no")
		{
			echo '{"success":false,"msj":"El número de factura que desea ingresar ya está asignado a otra orden, por favor verifiquelo","type":"warning","icon":"fa fa-warning","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}
		else
		{

			update_post_meta($id_post,'fecha_factura',$_POST['fecha_factura']);
			update_post_meta($id_post,'empresa_envio',$_POST['empresa_envio']);
			update_post_meta($id_post,'monto_seguro',$_POST['monto_seguro']);
			update_post_meta($id_post,'obj_orden',$_POST['obj_orden']);
			echo '{"success":true,"msj":"Se ha registrado la información de factura y envió con éxito","type":"success","icon":"fa fa-check","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}
		
		die();
	}

	add_action('wp_ajax_enviar_despacho','enviar_despacho');

	function enviar_despacho(){

		
		$id_post = $_POST['post_id'];
		$order = new WC_Order($id_post);


		$num_factura = get_post_meta($id_post,'num_factura',true);
		$fecha_factura = get_post_meta($id_post,'fecha_factura',true);
		$empresa_envio = get_post_meta($id_post,'empresa_envio',true);
		

		if($num_factura==""||$fecha_factura==""||$empresa_envio==""||$order->get_status()=="wc-on-hold")
		{
			echo '{"success":false,"msj":"No puede enviar esta orden al despacho hasta que haya sido procesado mediante del registro de los datos que son requeridos ","type":"danger","icon":"fa fa-warning","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}
		else
		{
			$order->update_status('wc-en-despacho');
			echo '{"success":true,"msj":"Se ha cambiado el estado de la orden a “En Despacho” ","type":"info","icon":"fa fa-check","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}
		

		


		die();
	}

	add_action('wp_ajax_notificar_cliente','notificar_cliente');

	function notificar_cliente(){

		
		$message = detalles_orden($_GET['order_id']);

		$order = new WC_Order($_GET['order_id']);

		$to = $order->billing_email;

		$subject = "Envio de Pedido";

		//La dirección de envio del email es la de nuestro blog por lo que agregando este header podremos responder al remitente original
		$headers = 'Reply-to: '.get_option('blogname').' <'.get_option('admin_email').'>' . "\r\n";
 
		//Filtro para indicar que email debe ser enviado en modo HTML
		add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

		$mail = wp_mail( $to, $subject, $message,$headers);

		if($mail==1)
		{
			$order->update_status('wc-cliente-notificad');
			$msj = "Se le ha notificado al Cliente que la Orden #".$_GET['order_id']." ha sido despachada";
			echo '{"success":true,"msj":'.json_encode($msj).',"type":"info","icon":"fa fa-check","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}
		else
		{

			$msj = "Ocurrio un error al trata de notificar al cliente de la Orden #".$_GET['order_id'].", por favor vuelva intentar";
			echo '{"success":false,"msj":'.json_encode($msj).',"type":"danger","icon":"fa fa-warning","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}


		die();
	}

	add_action('wp_ajax_enviar_mensaje_cliente','enviar_mensaje_cliente');

	function enviar_mensaje_cliente(){


		$order = new WC_Order($_POST['order_id']);

		$to = $order->billing_email;

		$message = $_POST['mensaje'];

		$subject = $_POST['asunto'];

		//La dirección de envio del email es la de nuestro blog por lo que agregando este header podremos responder al remitente original
		$headers = 'Reply-to: '.get_option('blogname').' <'.get_option('admin_email').'>' . "\r\n";
 
		//Filtro para indicar que email debe ser enviado en modo HTML
		add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

		$mail = wp_mail( $to, $subject, $message,$headers);

		if($mail==1)
		{
			$order->update_status('wc-cliente-notificad');
			$msj = "Se le ha eviado el mensaje al Cliente de la Orden #".$_POST['order_id']." exitosamente";
			echo '{"success":true,"msj":'.json_encode($msj).',"type":"info","icon":"fa fa-check","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}
		else
		{
			$msj = "Ocurrio un error al enviar el mensaje, por favor vuelva intentar";
			echo '{"success":false,"msj":'.json_encode($msj).',"type":"danger","icon":"fa fa-warning","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}


		die();
	}

	

	/*-----------------------------------------------------------------------------------*/

	/* PETICION AJAX MODULO DESPACHO

	/*-----------------------------------------------------------------------------------*/

	add_action( 'wp_ajax_listar_despacho', 'listar_despacho' );
	function listar_despacho( ) {

		$args = array('post_type'=> 'shop_order','posts_per_page' => -1,'post_status'=>['wc-en-despacho','wc-despachado']);
		$orders = get_posts( $args );
		$data = array();
		$user = wp_get_current_user();


		global $current_user, $wpdb;
		$role = $wpdb->prefix . 'capabilities';
		$current_user->role = array_keys($current_user->$role);
		$role = $current_user->role[0];


		foreach ($orders as $o) {
			
			$order = new WC_Order($o->ID);
			//		print_r($o);
			$data[] = array(
						"order_id" => $order->get_order_number( ), 
						"cliente"  => get_user_meta($order->get_user_id( ), 'nickname', true), 
						"status" => wc_get_order_status_name( $order->get_status() ),
						"date" => date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ),
						"total" => $order->get_formatted_order_total(),
						"payment_method"=>$order->payment_method_title,
						"vendedor" =>  get_user_meta(get_user_meta($order->get_user_id( ), 'id_vendedor', true), 'first_name', true)." ".get_user_meta(get_user_meta($order->get_user_id( ), 'id_vendedor', true), 'last_name', true),
						'num_factura' => get_post_meta($o->ID,'num_factura',true),
						'fecha_factura' => get_post_meta($o->ID,'fecha_factura',true),
						'empresa_envio' => get_post_meta($o->ID,'empresa_envio',true),
						'monto_seguro' => get_post_meta($o->ID,'monto_seguro',true),
						'obj_orden' => get_post_meta($o->ID,'obj_orden',true),
						'email_cliente' => $order->billing_email,
						'telefono_cliente' =>$order->billing_phone,
						'direccion_envio'=> $order->get_formatted_billing_address(),
						'numero_de_guia'=>get_post_meta($o->ID,'numero_de_guia',true),
						'fecha_despacho' =>get_post_meta($o->ID,'fecha_despacho',true),

						"accion_ver" => "<button class='btn btn-info btn-xs' style='width: 50%' data-toggle='modal' href='#ver-orden' onclick='cargarOrden(".$order->get_order_number( ).",".'"'.wc_get_order_status_name( $order->get_status()).'"'.")' data-toggle='tooltip' title='Detalles de la orden'><i class='fa fa-search-plus' aria-hidden='true' ></i></button>",

						"accion" => "<button class='btn btn-success btn-xs' style='width: 50%' data-toggle='modal' href='#modal-guia' onclick='cargarPedido(".$order->get_order_number( ).",".'"'.wc_get_order_status_name( $order->get_status()).'"'.")' data-toggle='tooltip' title='Cargar información de despacho'><i class='fa fa-archive' aria-hidden='true'></i></button>"
					);
			$items = $order->get_formatted_order_total();
			
		}
		echo '{"data":'.json_encode( $data ).'}';
		die();	
	}

	add_action('wp_ajax_get_guia','get_guia');

	function get_guia(){

		
		$id_post = $_POST['post_id'];

		$valido = "si";

		$args = array(
					'post_type'  =>  'shop_order',
					'meta_key'   => 'numero_de_guia',
					'meta_value' => $_POST['numero_de_guia']
					
		);

		$o= get_posts( $args );

		if(!empty($o) && count($o)==1 && $o[0]->ID == $_POST['post_id'])
		{
			update_post_meta($id_post,'numero_de_guia',$_POST['numero_de_guia']);
		}
		elseif(empty($o))
		{
			update_post_meta($id_post,'numero_de_guia',$_POST['numero_de_guia']);
		}
		else
		{
			$valido = "no";
		}


		$order = new WC_Order($id_post);
		
		if($valido == "no")
		{
			echo '{"success":false,"msj":"El número de guia que desea ingresar ya está asignado a otra orden, por favor verifiquelo","type":"warning","icon":"fa fa-warning","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}
		else
		{

			update_post_meta($id_post,'fecha_despacho',$_POST['fecha_despacho']);
			update_post_meta($id_post,'obj_orden',$_POST['obj_orden']);

			if($order->get_status()=="en-despacho")
			{
				$order->update_status('despachado');
			}
			echo '{"success":true,"msj":"Se ha registrado la información de guia con éxito","type":"success","icon":"fa fa-check","estado":"'.wc_get_order_status_name( $order->get_status()).'"}';
		}
		
		die();
	}

	/*-----------------------------------------------------------------------------------*/

	/* PETICION AJAX CAMBIO DE VENDEDOR

	/*-----------------------------------------------------------------------------------*/
    add_action('wp_ajax_enviar_correo_cambio','enviar_correo_cambio');

    function enviar_correo_cambio(){

    	//print_r($_POST);
		
    	$message = '<table border="0">
				<thead style="border-bottom: 1px solid">
					<tr>
						<th colspan="2"><strong><h3>Solicitud de Cambio de Vendedor</h3></strong></th>
					</tr>
					<tr>
						<th colspan="2"><hr></th>
					</tr>
				</thead>
				<tbody style="border-bottom: 1px solid">
					<tr>
						<td  colspan="2"><strong>Datos de Cliente</strong></td>
					</tr>
					<tr>
						<td><strong>ID:</strong></td>
						<td><strong>'.$_POST['id'].'</strong></td>
					</tr>
					<tr>
						<td><strong>NOMBRE:</strong></td>
						<td>'.$_POST['nombre'].'</td>
					</tr>
					<tr>
						<td><strong>EMAIL:</strong></td>
						<td>'.$_POST['email'].'</td>
					</tr>
					<tr>
						<td><strong>TELEFONO:</strong></td>
						<td>'.$_POST['telefono'].'</td>
					</tr>
					<tr>
						<th colspan="2"><hr></th>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td  colspan="2"><strong>Razon de Cambio de Vendedor</strong></td>
					</tr>
					<tr>
						<td  colspan="2">';
		if($_POST['explicacion']==""){
			$message.='Me equivoque al elegir el vendedor';
		}else{
			$message.=$_POST['explicacion'];
		}
		$message.='</td>
					</tr>
					<tr>
						<td><strong>Vendedor Requerido:</strong></td>
						<td>'.$_POST['vendedor'].'</td>
					</tr>
				</tfoot>
			</table>';

		

		$to = array('leomisaelflores@gmail.com','lmisaf22@gmail.com','yk1la2@gmail.com',get_option('admin_email'));

		$subject = "Solicitud de cambio de vendedor";
		
		//La dirección de envio del email es la de nuestro blog por lo que agregando este header podremos responder al remitente original
		$headers = 'Reply-to: '.get_option('blogname').' <'.get_option('admin_email').'>' . "";
 		
		
		add_filter( 'wp_mail_from', 'my_mail_from' );
		function my_mail_from( $email )
		{
			return get_option('admin_email');
		}


		add_filter( 'wp_mail_from_name', 'my_mail_from_name' );
		function my_mail_from_name( $name )
		{
			return get_option('blogname');;
		}

		add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));

		$mail = wp_mail( $to, $subject, $message,$headers);

		print_r($mail);

		if($mail==1)
		{
			
			$msj = "Estamos procesando su solicitud, en breve daremos respuesta a su requerimiento";
			echo '{"success":true,"msj":'.json_encode($msj).',"type":"info","icon":"fa fa-check"}';
		}
		else
		{
			$msj = "Ocurrio un error, por favor vuelva intentar";
			echo '{"success":false,"msj":'.json_encode($msj).',"type":"danger","icon":"fa fa-warning"}';
		}


		die();
	}

	//* Añade moneda y símbolo personalizado en WooCommerce
add_filter( 'woocommerce_currencies', 'add_my_currency' );
function add_my_currency( $currencies ) {
     $currencies['ABC'] = __( 'Bolívares', 'woocommerce' );
     return $currencies;
}
add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);
function add_my_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'ABC': $currency_symbol = 'Bs.'; break;
     }
     return $currency_symbol;
}



?>