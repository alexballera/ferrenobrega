<section class="foot">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-3 footer__logo">
				<div class="hidden-xs hidden-sm">
					<h2><img src="<?php bloginfo('template_url');?>/images/logo-ferrenobrega-350x233.png" class="img-responsive"></h2>
					<p>Somos una empresa joven, con actitud optimista y alto ímpetu de calidad servicio, avocados en la satisfacción plena de nuestros clientes.</p>
					<span class="cards hidden">
						<i class="fa fa-cc-paypal fa-3x"></i>
						<i class="fa fa-cc-amex fa-3x"></i>
						<i class="fa fa-cc-visa fa-3x"></i>
						<i class="fa fa-cc-mastercard fa-3x"></i>
					</span>
				</div>

				<div class="col-xs-12 col-sm-12 hidden-md hidden-lg text-center">
					<h2><img src="<?php bloginfo('template_url');?>/images/logo-ferrenobrega-350x233.png" class="img-responsive center-block"></h2>
					<p>Somos una empresa joven, con actitud optimista y alto ímpetu de calidad servicio, avocados en la satisfacción plena de nuestros clientes.</p>
					<span class="cards hidden">
						<i class="fa fa-cc-paypal fa-3x"></i>
						<i class="fa fa-cc-amex fa-3x"></i>
						<i class="fa fa-cc-visa fa-3x"></i>
						<i class="fa fa-cc-mastercard fa-3x"></i>
					</span>
					<hr>
				</div>
			</div>

			<div class="hidden-xs hidden-sm col-md-3 footer__categorias">
				<h2 style="background-image: url(<?php bloginfo('template_url');?>/images/fondo-footer.png);background-repeat: no-repeat;
                                               			   background-position: center;
                                               			   background-size: 100%;
                                               			       padding-top: 10px;
                                               			       text-align: center;" >Categorías</h2>
				<div class="flexbox-3">

					<?php $cleanmenu = wp_nav_menu( array(
						'theme_location'  => 'secondary-menu',  // footer-menu-uno
						'menu'            => false,
						'container'       => false,
						// 'link_before'     => '<i class="fa fa-angle-right"></i> ',
						'items_wrap'      => '%3$s',
						'depth'           => 0,
						'echo'						=> false,
						) );
					echo str_replace( 'li', 'span', $cleanmenu );
					?>
				</div>
			</div>

			<div class="hidden-xs hidden-sm col-md-3 footer__mensaje">
				<h2 style="background-image: url(<?php bloginfo('template_url');?>/images/fondo-footer.png);background-repeat: no-repeat;
                                               			   background-position: center;
                                               			   background-size: 100%;
                                               			       padding-top: 10px;
                                               			       text-align: center;"  >Mensaje Directo</h2>
                                 <!-- Ferrenobrega -->
                              <?php if (function_exists("add_formcraft_form")) { add_formcraft_form("[fc id='3'][/fc]"); } ?>
                               <!-- Localhost -->
                               <?php if (function_exists("add_formcraft_form")) { add_formcraft_form("[fc id='1'][/fc]"); } ?>
			</div>


			<div class="col-xs-12 col-sm-12 col-md-3 footer__contactanos">
				<h2 class="hidden-xs hidden-sm" style="background-image: url(<?php bloginfo('template_url');?>/images/fondo-footer.png);background-repeat: no-repeat;
                                               			   background-position: center;
                                               			   background-size: 100%;
                                               			       padding-top: 10px;
                                               			       text-align: center;"    >Contáctenos</h2>
				<table class="hidden-xs hidden-sm">
					<!-- <tr>
						<td><span class="i-bg"><i class="fa fa-map-marker"></i></span></td><td style="font-size: 16px;" > Calle Páez C.C Maracay entre Vargas y boulevard C.C Maracay PB local 1 y 4 (frente al Banco de Venezuela

de la calle Páez)</td>
					</tr> -->
					<tr>
						<td><span class="i-bg"><i class="fa fa-phone"></i></span></td><td style="font-size: 16px;"> 0212-2152713 <br> 0414-3993475<br></td>
					</tr>
					<tr>
						<td><span class="i-bg"><i class="fa fa-envelope"></i></span></td><td style="font-size: 16px;"><a href="mailto:ferreterianobrega@gmail.com">ferreterianobrega@gmail.com</a></td>
					</tr>
				</table>
				<!-- <div class="row hidden-md hidden-lg text-center">
					<h2>Contáctenos</h2>
					<div class="col-xs-12"> Calle Páez C.C Maracay entre Vargas y boulevard C.C Maracay PB local 1 y 4 (frente a la antigua sede de los bomberos)</div>
					<div class="col-xs-12"> 0212-2152713 <br> 0414-3993475</div>
					<div class="col-xs-12"><a href="mailto:ferreterianobrega@gmail.com">ferreterianobrega@gmail.com</a></div>
				</div> -->
			</div>

		</div>
	</div>
</section>





<footer>
	<div class="container">
		<div class="row">
			<div class="flexbox">

				<div class="col-xs-12 col-sm-12 col-md-12">
					<p><?php bloginfo('blogname');?> J-40281461-5 © 2016 | Desarrollado por <a href="http://www.ideadigital.com.ve" target="_blank">Idea Digital</a></p>
				</div>
				<div class="col-xs-12 hidden-md hidden-lg"><hr></div>
<!-- 				<div class="col-xs-12 col-sm-12 col-md-5 text-right">

					<?php $cleanmenu = wp_nav_menu( array(
						'theme_location'  => 'footer-menu-dos',
						'menu'            => false,
						'container'       => false,
						'depth'           => 0,
						'echo'						=> false,
						) );
					echo str_replace( 'li', 'span', $cleanmenu );
					?>

				</div> -->

			</div>
		</div>
	</div>
</footer>

<?php 
    $data = listar_vendedor(); 
    global $wpdb;
    $user = wp_get_current_user();
    
?>

<div class="modal fade" id="modal-cambio-vendedor">
	<div class="modal-dialog">
		<div class="modal-content box">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">SOLICITUD DE CAMBIO DE VENDEDOR</h4>
			</div>
			<div class="modal-body">
				<form id="from-cambio-vendedor">
                      
                    <input type="hidden" id="id_user_cambio" value="<?php echo $user->ID; ?>"> 
                    
                    <div class="form-group" id="nombre_user_cambio_form">
                        <label for="">Nombre Completo:</label><span class="text-danger">*</span>
                        <input type="text" id="nombre_user_cambio" class="form-control input-sm" value="<?php echo get_user_meta($user->ID,'first_name',true).' '.get_user_meta($user->ID,'last_name',true); ?>" placeholder="Nombre Completo">
                        <span class="text-danger" id="nombre_user_cambio_msj"></span>
                    </div>

                    <div class="form-group" id="email_user_cambio_form">
                        <label for="">Email:</label><span class="text-danger">*</span>
                        <input type="email" id="email_user_cambio" class="form-control input-sm" placeholder="Email" value="<?php echo $user->user_email; ?>">
                        <span class="text-danger" id="email_user_cambio_msj"></span>
                    </div>
                    
                    <div class="form-group" id="telefono_user_cambio_form">
                        <label for="">Telefono:</label><span class="text-danger">*</span>
                        <input type="text" id="telefono_user_cambio" class="form-control input-sm numeric" placeholder="Telefono" value="<?php echo get_user_meta($user->ID,'billing_phone',true); ?>">
                        <span class="text-danger" id="telefono_user_cambio_msj"></span>
                    </div>
                    
                    <div class="form-group">
                        <label for="">Razón de cambio de vendedor actual:</label><span class="text-danger">*</span>
                        <div class="radio">
                            <label>
                                <input type="radio" name="razon" id="razon-1" value="1" checked="checked">
                                Me equivoque al elegir el vendedor
                            </label>
                            <label style="margin-left: 30px;">
                                <input type="radio" name="razon" id="razon-2" value="2">
                                Otra razón (Explicar)
                            </label>
                        </div>
                    </div>
                    
                    <div class="form-group" id="vendedor_user_cambio_form">
                        <label for="">Vendedor: </label>
                        <select name='' id="vendedor_user_cambio" class="form-control input-sm" required>
                            <option value="" selected disabled>-Seleccione al vendedor deseado-</option>
                            <?php foreach ($data as $d) { ?>
                                <option value="<?php echo $d['first_name'].' '.$d['last_name']; ?>"><?php echo $d['first_name'].' '.$d['last_name']; ?></option>
                            <?php } ?>
                        </select>
                         <span class="text-danger" id="vendedor_user_cambio_msj"></span>
                      </div>
                    
                    <div class="form-group" id="explicacion_user_cambio_form">
                        <textarea name="" id="explicacion_user_cambio" class="form-control input-sm" rows="3" required="required" placeholder="Explicar razón" hidden></textarea>
                        <span class="text-danger" id="explicacion_user_cambio_msj"></span>

                    </div>
                    
                    
                </form>
			</div>
			<div class="load-ajax"></div>
			
			<div class="modal-footer">
			    <button type="button" id="enviar_msj_canbio" class="btn btn-success pull-left">Enviar</button>
				<button type="button"  class="btn btn-default" data-dismiss="modal">Cerrar</button>
				
			</div>
		</div>
	</div>
</div>


<?php WP_footer();?>



<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/stickUp/stickUp.js"></script>
<script src="<?php bloginfo('template_url');?>/plugins/numeric/jquery.numeric.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url');?>/plugins/notify/bootstrap-notify.js" type="text/javascript"></script>
<!-- <script type="text/javascript" src="<?php bloginfo('template_url');?>/plugins/ajax/cambio-vendedor.js"></script> -->
<script type="text/javascript">
jQuery(function(jQuery) {
	jQuery(document).ready( function() {
		jQuery('.navbar > .container').stickUp();
	});
});
</script>

</body>
</html>