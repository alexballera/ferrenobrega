                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  <p
<?php 
/**
Template Name: pagina inicio
*/
?>

<?php include('header.php');?>
<?php include('head.php');?>


<section class="rotate">
	<div class="flexslider principal">
		<ul class="slides">
			<li><img src="<?php bloginfo('template_url');?>/images/banner-mercadopago.jpg"></li>
			<li><img src="<?php bloginfo('template_url');?>/images/banner-zoom.jpg"></li>
			<li><img src="<?php bloginfo('template_url');?>/images/hoffman.jpg"></li>
		</ul>
	</div>
</section>


<div class="clearfloat"><br></div>

<section class="productos hidden-xs">
	

	<div class="container">
	    <section style=" height: 10px; display: table;width: 83.1%;height: 5px; position: absolute;z-index: 999;margin-left: 5px;"></section>
		<h3 class="front-title"><span>LOS MÁS RECIENTES</span></h3>
	</div>
	<div class="container woocommerce">
		<ul class="products">
			<?php	$args = array(
				'post_type' => 'product',
				'posts_per_page' => 4,
				);?>
				<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php include('loop-slider.php');?>
				
			<?php endwhile;	}	else { echo __( 'No products found' ); }	wp_reset_query();	?>
		</ul>
	</div>
</section>

<div class="clearfloat"><br></div>

<section class="productos hidden-xs">
	<div class="container">
	
		<h3 class="front-title"><span>OFERTAS DEL MES</span></h3>
	</div>
	<div class="container woocommerce">
		<div class="productos-slider carrusel flexslider">
			<ul class="slides products productos">
				<?php
				$args = array(
					'post_type'      => 'product',
					'posts_per_page' => 12,
					'meta_query'     => array(
						'relation' => 'OR',
						array(
							'key'           => '_sale_price',
							'value'         => 0,
							'compare'       => '>',
							'type'          => 'numeric'
							),
						array(
							'key'           => '_min_variation_sale_price',
							'value'         => 0,
							'compare'       => '>',
							'type'          => 'numeric'
							)
						)
					);
					?>
					<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php include('loop-slider.php');?>
				<?php endwhile; } else {	echo __( '<h3>No hay productos en oferta</h3>' );} wp_reset_postdata();?>
			</ul>
		</div>
	</div>
</section>

<section class="bottom">

	<div class="clearfloat"><br><br></div>
	<div class="container marketing">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<a href="<?php bloginfo('home');?>/categoria-producto/routers"><img src="<?php bloginfo('template_url');?>/images/cisa.jpg" class="img-responsive center-block"></a>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg"><br></div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<a href="<?php bloginfo('home');?>/categoria-producto/tablets"><img src="<?php bloginfo('template_url');?>/images/charger.jpg" class="img-responsive center-block"></a>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg"><br></div>
			</div>
		</div>
	</div>
	<div class="clearfloat"><br><br></div>
</section>



<?php if(have_posts()) : while(have_posts()) : the_post();?>	
<section class="content-wrap">		
<div class="container page">			
		
<?php the_content();?>	
	</div>	
	</section>
	<?php endwhile;?>
	<!-- Else -->
	<?php else:?>
	<?php endif;?>
	
	
	<section class="productos">
	<div class="container">
		<div class="container"><h3 class="front-title"><span>MARCAS</span></h3>
		</div>
			<div class="rotate-products">
				<div class="container">
					<ul id="slides">
						<li><img src="<?php bloginfo('template_url');?>/images/weldtech.jpg" alt=""></li>
						<li><img src="<?php bloginfo('template_url');?>/images/Swevels.png" alt=""></li>
						<li><img src="<?php bloginfo('template_url');?>/images/SQ.jpg" alt=""></li>
						<li><img src="<?php bloginfo('template_url');?>/images/RidGid.jpg" alt=""></li>
						<li><img src="<?php bloginfo('template_url');?>/images/Fundicion-Pacifico.jpg" alt=""></li>
						<li><img src="<?php bloginfo('template_url');?>/images/Decocar.gif" alt=""></li>
					</ul>
				</div>
		</div>
	</div>
</section>
	
	
	
	
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.flexisel.js"></script>
<script type="text/javascript">
jQuery(window).load(function() {
	jQuery("#slides").flexisel({
		visibleItems: 6,
		animationSpeed: 200,
		autoPlay: false,
		autoPlaySpeed: 3000,
		pauseOnHover: true,
		clone:false,
		enableResponsiveBreakpoints: true,
		responsiveBreakpoints: {
			portrait: {
				changePoint:480,
				visibleItems: 1
			},
			landscape: {
				changePoint:640,
				visibleItems: 2
			},
			tablet: {
				changePoint:768,
				visibleItems: 3
			}
		}
	});
});
</script>
	
	
	<?php include('footer.php');?>