<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->

<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->

<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->

<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>

	<meta charset="utf-8">

	<title><?php bloginfo('name');?> | <?php bloginfo('description');?>  <?php wp_title( '|', true, 'left' ); ?></title>

	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">



	<?php wp_head(); ?>



	<!-- Favicons -->

	<link rel="icon" href="<?php bloginfo('template_url');?>/images/favicon.ico">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo('template_url');?>/images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo('template_url');?>/images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url');?>/images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url');?>/images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url');?>/images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url');?>/images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo('template_url');?>/images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url');?>/images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url');?>/images/apple-icon-180x180.png">
	<link rel="manifest" href="<?php bloginfo('template_url');?>/images/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php bloginfo('template_url');?>/images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">



	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url');?>">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<!-- Bootstrap -->

	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css">

	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/css/dataTables.bootstrap.min.css">



	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="<?php bloginfo('template_url');?>/js/dataTables.bootstrap.min.js"></script>



	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/flexslider/jquery.flexslider-min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/js/flexslider/flexslider.css">
<script type="text/javascript">
      var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
   </script>

<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/funcionesAjax.js"></script>





	<script type="text/javascript">

	jQuery(window).load(function() {

		jQuery('.principal').flexslider({

			// directionNav: true,

                        animation: "slide",

			controlNav: false,

                        directionNav: true,

			// pauseOnAction: false,

			// pauseOnHover: false,

			prevText: "",

			nextText: "",

		});

	});



	jQuery(window).load(function() {

		jQuery('.productos-slider').flexslider({

			animation: "slide",

			slideshow: false,

			directionNav: true,

			controlNav: false,

			pauseOnAction: false,

			pauseOnHover: false,

			prevText: "",

			nextText: "",

			itemWidth: 101,

			itemMargin: 15,

			minItems: 4,

			maxItems: 4

		});

	});



	jQuery(window).load(function() {

		jQuery('.marcas').flexslider({

			animation: "slide",

			animationLoop: true,

			slideshow: false,

			directionNav: true,

			controlNav: false,

			pauseOnAction: false,

			pauseOnHover: false,

			prevText: "",

			nextText: "",

			itemWidth: 100,

			itemMargin: 15,

			minItems: 1,

			maxItems: 8

		});

	});



	jQuery(function () {

		jQuery('[data-toggle="tooltip"]').tooltip()

	})

	
	var lenguaje = {
			    "decimal":        "",
			    "emptyTable":     "No hay datos disponibles en la tabla",
			    "info":           "Mostrando  _START_ a _END_ de _TOTAL_ entradas ",
			    "infoEmpty":      "Mostrando  0 a 0 de 0 entradas ",
			    "infoFiltered":   "(filtrada a partir de  _MAX_  entradas )",
			    "infoPostFix":    "",
			    "thousands":      ",",
			    "lengthMenu":     "Mostra _MENU_ entradas ",
			    "loadingRecords": "Cargando...",
			    "processing":     "Tratamiento...",
			    "search":         "Buscar:",
			    "zeroRecords":    "No se encontraron registros coincidentes",
			    "paginate": {
			        "first":      "Primero",
			        "last":       "Último",
			        "next":       "Siguiente",
			        "previous":   "Anterior"
			    },
			    "aria": {
			        "sortAscending":  ": activar para ordenar la columna ascendente",
			        "sortDescending": ": activar para ordenar la columna descendente"
			    }
		};

	</script>



</head>



<body <?php if (is_single()): echo 'class="single"'; elseif (is_home()): echo 'class="home"'; endif ?>>

