<section class="top" style="height: 35px;">
	<div class="container" style="width: 90%;">
		<div class="row">
			<div class="col-xs-9  col-sm-3 col-md-3">
				<i class="fa fa-envelope-o orange">
				</i>
				<span>ferreterianobrega@gmail.com</span>
			</div>
			<div class="col-xs-9  col-sm-3 col-md-3">
				<i class="fa fa-phone orange">
				</i>
				<span>0212-2152713 / 0414-3993475 </span>
			</div>
			<div class="col-xs-9  col-sm-3 col-md-3" style="right: 100px;width: 400px;padding-left: 60px;">
				<span class="flexbox-4 customer-name"style="height: 20px; font-size: 15px;">
					<?php if ( is_user_logged_in() ) {?>
					<?php global $current_user; get_currentuserinfo(); if ($current_user->user_firstname == true): ?>
					¡Bienvenido <?php echo ''. $current_user->user_firstname .'!'; else: ?>
					¡Bienvenido!<?php endif?>
					<nav class="navbar">
						<div class="container-fluid" style="height: 40px;">
							<ul class="nav navbar-nav">
								<li class="dropdown"  >
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  style="padding-bottom:7px;     font-size: 15px;  "        >Mi Cuenta <span class="caret" ></span></a>
									<ul class="dropdown-menu" style="left: 11px;" >
										<?php if (current_user_can('administrator')): ?>
										<li><a href="<?php bloginfo('home');?>/gestion-vendedores/" style="padding-left: 0px;    font-size: 15px;color: #ffffff;"  >Vendedores (Cambiar - Gestionar)</a></li>
										<?php endif ?>
										<?php if (	current_user_can('administrator') || current_user_can('vendor')): ?>
										<li><a href="<?php bloginfo('home');?>/modulo-de-ventas-vendedor/" style="padding-left: 0px;    font-size: 15px;color: #ffffff;" >Ventas (Gestionar Pedidos)</a></li>
										<?php endif ?>
										<?php if (	current_user_can('administrator') || current_user_can('despachador')): ?>
										<li><a href="<?php bloginfo('home');?>/modulo-de-despacho/" style="padding-left: 0px;    font-size: 15px;color: #ffffff;" >Despacho</a></li>
										<?php endif ?>
										<?php if (current_user_can('administrator')): ?>
										<li><a href="<?php bloginfo('home');?>/gestion-de-precios/" style="padding-left: 0px;    font-size: 15px;color: #ffffff;"  >Precios</a></li>
										<?php endif ?>
										<?php if (current_user_can('administrator')): ?>
										<li><a href="<?php bloginfo('home');?>/wp-admin/" style="padding-left: 0px;    font-size: 15px;color: #ffffff;" >Panel Administrador</a></li>
										<?php endif ?>
										<li><a href="<?php bloginfo('home');?>/mi-cuenta/view-order" style="padding-left: 0px;    font-size: 15px;color: #ffffff;"  >Mis Pedidos</a></li>
										<li><a href="<?php bloginfo('home');?>/mi-cuenta/edit-account/" style="padding-left: 0px;    font-size: 15px;color: #ffffff;"  >Editar Datos</a></li>
										<li><a href="<?php echo wp_logout_url();?>" style="padding-left: 0px;    font-size: 15px;color: #ffffff;" >Cerrar Sesión</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</nav>
					<?php } else {?>
					<a href="<?php bloginfo('home');?>/mi-cuenta" class="log-in" title="">
						<i class="fa fa-sign-in"></i>
						<i class="icon-signin" style="padding-right:5px;"></i>Iniciar Sesión / Registrarse
					</a>
					<?php };?>
				</span>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-2 pull-right text-right">
				<a class="" href="https://www.facebook.com/Ferrenobrega-1873956566164126/" target="_blank"><i class="fa fa-facebook-official"></i></a>
				<a class="" href="http://twitter.com/ferrenobregave" target="_blank"><i class="fa fa-twitter"></i></a>
				<a class="" href="https://www.instagram.com/ferrenobregave/" target="_blank"><i class="fa fa-instagram"></i></a>
			</div>
		</div>
	</div>
</section>
<header class="hey"    style="padding-top: 28px;">
	<div class="flexbox container">
		<div class="col-xs-12 col-sm-12 col-md-5">
			<a href="<?php bloginfo('home');?>"><img src="<?php bloginfo('template_url');?>/images/logo-ferrenobrega-350x233.png" width="250" class="logus img-responsive center-block"></a>
		</div>
		<div class="nopad col-xs-12 col-sm-12 col-md-12"style="padding-left: 30px;">
			
			
			<div class="nopad flexbox-2 col-xs-12">
				<form action="<?php bloginfo('home');?>" class="flexbox" role="search" method="get">
					<input type="text" value="" name="s" id="s" class="formbox" placeholder="¿Qué quieres buscar?" autocomplete="off">
					<button type="submit"><i class="fa fa-search"></i></button>
					<input type="hidden" name="post_type" value="product" />
				</form>
			</div>
			
		</div>
		<div class="nopad col-xs-12 col-sm-12 col-md-3">
			<div class="cart-top flexbox-2 hidden-xs hidden-sm col-md-8 pull-right">
				
				<a class="flexbox cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" data-toggle="tooltip" data-placement="top" title="Ver Carrito">
					<span class="cart-icon">
						<span class="items" style=" margin-left: 20px;top: 20px;"><?php echo sprintf (_n( '%d', '%d', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?></span>
						
					</span>
					<span class="total"><?php echo WC()->cart->get_cart_total(); ?></span>
				</a>
			</div>
		</div>
	</div>
</header>
<section class="menu">
	<nav class="navprin navbar navbar-default navbar-static-top hidden-xs">
		<div class="topmenu container-fluid nopad">
			<div class="container">
				<div class="collapse navbar-collapse" id="navbar-2">
					
					<div class="menu-header-1-container">
						<ul id="menu-header-1" class="flexbox menu-prin1 nav navbar-nav1">
							<li id="menu-item-383" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-349 current_page_item menu-item-383" style="border-right: 1px solid #d3d3d3 !important;"><a href="http://ferrenobrega.com.ve/"><span>INICIO</span></a></li>
							<li id="menu-item-592" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-592" style="border-right: 1px solid #d3d3d3 !important;"><a href="http://ferrenobrega.com.ve/datos-bancarios/"><span>DATOS BANCARIOS</span></a></li>
							
							<li id="menu-item-584" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-584"   style="border-right: 1px solid #d3d3d3 !important;"><a href="http://ferrenobrega.com.ve/registro-de-pago/"><span>REGISTRO DE PAGO</span></a></li>
							
							<li id="menu-item-585" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-585" style="border-right: 1px solid #d3d3d3 !important;"><a href="http://ferrenobrega.com.ve/contactanos/"><span>CONTACTANOS</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<section style=" height: 10px; display: table;width: 100%;height: 5px; background:#ff9400;position: absolute;z-index: 999; ">
		</section>
		<div class="container" style="margin-left: 0px; margin-right: 0px;padding-left: 0px;padding-right: 0px; width: 100%;">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
				<span class="sr-only">Menu</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="navbar-1">
				<section  style=" background-color: #313131;">
					<?php wp_nav_menu( array(
						'theme_location'  => 'secondary-menu',
						'container'       => 'ul',
						'menu_class'      => 'flexbox menu-prin nav navbar-nav',
						'fallback_cb'     => 'wp_page_menu',
						'link_before'     => '<span>',
						'link_after'      => '</span>',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 0
				) ); ?>
			</section>
			
			<section  style=" background-color: #171717;">
				<div class="container">
					<div class="collapse navbar-collapse" id="navbar-3" style="float: left;">
						<?php wp_nav_menu( array(
							'theme_location'  => 'third_menu',
							'container'       => 'ul',
							'menu_class'      => 'flexbox menu-prin nav navbar-nav',
							'fallback_cb'     => 'wp_page_menu',
							'link_before'     => '<span>',
							'link_after'      => '</span>',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0
					) ); ?>
				</div>
			</div>
		</section>
	</div>
</div>
</nav>
</section>
<section class="menu hidden-sm hidden-md hidden-lg">
<nav class="navprin navbar navbar-default navbar-static-top">
<div class="topmenu container-fluid nopad">
	<div class="container">
		<div class="collapse navbar-collapse" id="navbar-2">
			<?php wp_nav_menu( array( 'theme_location' => 'primary-menu',	'menu_class' => 'flexbox menu-prin1 nav navbar-nav1','link_before' => '<span>','link_after' => '</span>') ); ?>
		</div>
	</div>
</div>
<section style=" height: 10px; display: table;width: 100%;height: 5px; background:#ff9400;position: absolute;z-index: 999; ">
</section>
<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-4" style="margin-top:25px;">
		<span class="sr-only">Menu</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
	</div>
	<div class="collapse navbar-collapse" id="navbar-4">
		<?php wp_nav_menu( array(
			'theme_location'  => 'secondary-menu',
			'container'       => 'ul',
			'menu_class'      => 'flexbox menu-prin nav navbar-nav',
			'fallback_cb'     => 'wp_page_menu',
			'link_before'     => '<span>',
			'link_after'      => '</span>',
		'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 0
	) ); ?>
	<?php wp_nav_menu( array(
			'theme_location'  => 'tres',
			'container'       => 'ul',
			'menu_class'      => 'flexbox menu-prin nav navbar-nav',
			'fallback_cb'     => 'wp_page_menu',
			'link_before'     => '<span>',
			'link_after'      => '</span>',
		'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth'           => 0
) ); ?>

</div>
</div>
</nav>
</section>
<section style=" height: 10px; display: table;width: 100%;height: 5px; background:#ff9400;position: absolute;z-index: 999; ">
</section>