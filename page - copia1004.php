<?php 
/*
Template Name: inicio
*/
?>

<?php include('header.php');?>
<?php include('head.php');?>

<link href='https://fonts.googleapis.com/css?family=Amaranth:400italic' rel='stylesheet' type='text/css'>

<section class="rotate">
	<div class="flexslider principal">
		<ul class="slides">
			<li><a href="<?php bloginfo('home');?>/tienda"><img src="<?php bloginfo('template_url');?>/images/banner1_compugate.jpg"></a></li>
			<li><img src="<?php bloginfo('template_url');?>/images/banner2.jpg"></a></li>
		</ul>
	</div>
</section>


<div class="clearfloat"><br></div>

<section class="productos ">
	

	<div class="container">
	
		<h3 class="front-title"><span style="font-family: 'Amaranth', sans-serif; text-transform: capitalize;    font-weight: bold;">Productos Recientes</span></h3>
	</div>
	<div class="container woocommerce"  style="padding-top: 15px;"    >
		<ul class="products">
			<?php	$args = array(
				'post_type' => 'product',
				'posts_per_page' => 8,
                                 'meta_query'     => array(
						'relation' => 'OR',
						array(
							'key'           => '_sale_price',
							'value'         => 0,
							'compare'       => '=',
							'type'          => 'numeric'
							),
						
						)
			
				
				);?>
				<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php include('loop-slider.php');?>
				
			<?php endwhile;	}	else { echo __( 'No products found' ); }	wp_reset_query();	?>
		</ul>
	</div>
</section>

<div class="clearfloat"><br></div>

<section class="productos hidden-xs" style=" padding-top: 2%;">
	<div class="container">

		<h3 class="front-title"><span style="font-family: 'Amaranth', sans-serif;    text-transform:none;    font-weight: bold;"         >Ofertas del Mes</span></h3>
	</div>
	<div class="container woocommerce" style="padding-top: 15px;"   >
	<!-- 	<div class="productos-slider carrusel flexslider"> -->
			<ul class="products">
				<?php
				$args = array(
					'post_type'      => 'product',
					'posts_per_page' => 8,
					'meta_query'     => array(
						'relation' => 'OR',
						array(
							'key'           => '_sale_price',
							'value'         => 0,
							'compare'       => '>',
							'type'          => 'numeric'
							),
						array(
							'key'           => '_min_variation_sale_price',
							'value'         => 0,
							'compare'       => '>',
							'type'          => 'numeric'
							)
						)
					);
					?>
					<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php include('loop-slider.php');?>
				<?php endwhile; } else {	echo __( '<h3>No hay productos en oferta</h3>' );} wp_reset_postdata();?>
			</ul>
		<!-- </div> -->
	</div>
</section>






<section class="bottom">

	<div class="container" style=" padding-top: 2%;"   >
   
       <div class="container"><h3 class="front-title"><span  style="font-family: 'Amaranth', sans-serif;text-transform: none;    font-weight: bold;"              >Conoce a nuestro equipo</span></h3></div>
	<div class="clearfloat"><br></div>
		<div class="container marketing" style="padding-bottom: 25px;"  >
			  <?php 
                                   $user = wp_get_current_user();
                                   
                                   $vendedores = listar_vendedor();
             
                                     foreach ($vendedores as $vendedor) {
                                        $img = get_avatar($vendedor['ID'],185);
                                       
                                       ?>
                                      <div class="col-sm-6 col-md-4 col-lg-3 text-center">
                                         
                                           <div class="text-center">
                                           		<div class="contenedor-avatar">
                                           			<?php   echo $img; ?><br><br>
                                           		</div>
                                               
                                               <div style="background-image: url(<?php bloginfo('template_url');?>/images/fondo-nombre.png);
                                               			   color: white; padding: 10px;
                                               			   background-repeat: no-repeat;
                                               			   background-position: center;
                                               			   background-size: 100%;">
                                               		<?php   echo $vendedor['first_name']." ".$vendedor['last_name']; ?>
                                               </div>
                                               
                                             </div>
                                         
                                      </div>
                                           
                                      <?php
                                     }
             
                                  ?>
	</div>
</section>
	 









<?php if(have_posts()) : while(have_posts()) : the_post();?>	
<section class="content-wrap">		
<div class="container page">			
		
<?php the_content();?>	
	</div>	
	</section>
	<?php endwhile;?>
	<!-- Else -->
	<?php else:?>
	<?php endif;?>
	





	<section class="productos">
	
	
	<div class="clearfix"><br></div>
	<div class="rotate-products">
		<div class="container">
			<ul id="slides">
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/apple-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/epson-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/hp-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/logo-microsoft.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/logo-samsung.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/sandisk-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/lenovo-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/lg-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/dell-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/tplink-logo.png" alt=""></li>
			</ul>
		</div>
	</div>
</section>
	
	
	
	
<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.flexisel.js"></script>
<script type="text/javascript">
jQuery(window).load(function() {
	jQuery("#slides").flexisel({
		visibleItems: 6,
		animationSpeed: 200,
		autoPlay: false,
		autoPlaySpeed: 3000,
		pauseOnHover: true,
		clone:false,
		enableResponsiveBreakpoints: true,
		responsiveBreakpoints: {
			portrait: {
				changePoint:480,
				visibleItems: 1
			},
			landscape: {
				changePoint:640,
				visibleItems: 2
			},
			tablet: {
				changePoint:768,
				visibleItems: 3
			}
		}
	});
});
</script>


<div class="nopad hidden-xs hidden-sm col-md-12">
<iframe src="https://www.google.com/maps/d/embed?mid=z9XHXCgDQs-Y.kEiXvR8boiAY" width="100%" height="480"frameborder="0" style="border:0; display: table;" allowfullscreen></iframe> 
</div>
	
	
	<?php include('footer.php');?>