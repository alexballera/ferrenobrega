<?php 
/*
Template Name: Modulo para despacho
*/
?>
<?php include('header.php');?>
<?php include('head.php');?>

<?php global $current_user;
 ?>
<?php if ($current_user->roles[0]=="administrator"||$current_user->roles[0]=="despachador"){ ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/plugins/notify/animate.min.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/plugins/daterangepicker/daterangepicker.css">
<?php if(have_posts()) : while(have_posts()) : the_post();?>
	<section class="content-wrap" style=" padding-bottom: 1.5%;" >
		<div class="container page">

			<h2><?php the_title();?></h2>
			<?php the_breadcrumb();?>
			<?php the_content();?>

			<div class="col-xs-12">	
						<div class="box box-success">
							<div class="box-header">
								<h3 class="box-title">Ordenes</h3>
							</div>
							<div class="box-body">
				                  
									<table class="table table-bordered table-hover" id="table-despacho">
										<thead>
											<tr>
												<th>Orden</th>
												<th>Cliente</th>
												<th>Estado</th>
												<th>Factura</th>
												<th>Fecha de Facturación</th>
												<th>Acción</th>
											</tr>
										</thead>
									</table>

							</div>
						</div>

				</div>


		</div>



		<!-- VENTANA MODAL PARA LA TABLA DE ORDENES (INICIO) -->


				<div class="modal fade" id="ver-orden">
					<div class="modal-dialog">
						<div class="modal-content box">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title">Orden #<span id="num_pedido"></span> <span id="estado_pedido" class="label label-danger">	</span></h4>
							</div>
							<div class="modal-body">
								<div class="table-responsive" id="detalles_pedido">
									
								</div>
							</div>
							<div class="load-ajax"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-danger pull-left" onclick="generarPDF('<?php bloginfo('template_url');?>/plugins/DOMPDF/reporte_orden_pdf.php')"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>  Exportar PDF</button>
								<button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
							</div>
						</div>
					</div>
				</div>


		<!-- VENTANA MODAL PARA LA TABLA DE ORDENES (FIN) -->


				<div class="modal fade" id="modal-guia">
					<div class="modal-dialog">
						<div class="modal-content box">
							<form id="form-factura">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<h4 class="modal-title">Orden #<span id="num_orden"></span> <span id="estado_orden" class="label label-danger">	</span></h4>
								</div>
								<div class="modal-body">
									<div class="row">	
										<div class="form-group col-sm-4">
											<label for="">Pedido: </label><span class="text-danger"> *</span>
											<div class="input-group">
											  	<input type="text" class="form-control input-sm" id="order_id" disabled="true">
											  	<span class="input-group-addon"><i class="fa fa-archive" aria-hidden="true"></i></span>
											</div>
											
										</div>

										<div class="form-group col-sm-4" id="num_factura_form">
											<label for="">Numerdo de Factura: </label><span class="text-danger"> *</span>
											<div class="input-group">
											  	<input type="text" class="form-control input-sm input" id="num_factura"  disabled="true">
											  	<span class="input-group-addon"><i class="fa fa-file" aria-hidden="true"></i></span>
											</div>
											
											<span class="text-danger" id="num_factura_msj"></span>
										</div>
										
										<div class="form-group col-sm-4" id="fecha_factura_form">
											<label for="">Fecha: </label><span class="text-danger"> *</span>
											<div class="input-group">
											  	<input type="text" class="form-control input-sm input" id="fecha_factura" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask disabled="true">
											  	<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
											</div>
											
											<span class="text-danger" id="fecha_factura_msj"></span>
										</div>	

										<div class="form-group col-sm-6" id="empresa_envio_form">
											<label for="">Empresa de envio: </label><span class="text-danger"> *</span>
											<div class="input-group">
											  	<input type="text" class="form-control input-sm input" id="empresa_envio" disabled="true">
											  	<span class="input-group-addon"><i class="fa fa-truck" aria-hidden="true"></i></span>
											</div>
											
											<span class="text-danger" id="empresa_envio_msj"></span>
										</div>	

										<div class="form-group col-sm-6" id="monto_seguro_form">
											<label for="">Monto de aseguramiento: </label><span class="text-danger"> *</span>
											<div class="input-group">
											  	<input type="text" class="form-control input-sm numeric input" id="monto_seguro" disabled="true">
											  	<span class="input-group-addon"><i class="fa fa-money"></i></span>
											</div>
											
											<span class="text-danger" id="monto_seguro_msj"></span>
										</div>

										<div class="form-group col-sm-12">
											<textarea class="form-control input-sm input" rows="3" id="obj_orden" placeholder="Observación" style="min-height: 200px; max-height: 200px; max-width: 100%; min-width: 100%;" disabled="true"></textarea>

										</div>

										<div class="form-group col-sm-6" id="numero_de_guia_form">
											<label for="">Numerdo de guía: </label><span class="text-danger"> *</span>
											<div class="input-group">
											  	<input type="text" class="form-control input-sm input numeric" id="numero_de_guia">
											  	<span class="input-group-addon"><i class="fa fa-file" aria-hidden="true"></i></span>
											</div>
											
											<span class="text-danger" id="numero_de_guia_msj"></span>
										</div>
										
										<div class="form-group col-sm-6" id="fecha_despacho_form">
											<label for="">Fecha de despacho: </label><span class="text-danger"> *</span>
											<div class="input-group">
											  	<input type="text" class="form-control input-sm input validacion" id="fecha_despacho" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
											  	<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
											</div>
											
											<span class="text-danger" id="fecha_despacho_msj"></span>
										</div>	

									</div>
									
									
										
								</div>

								

								<div class="modal-footer">
									<button type="button" class="btn btn-success pull-left input" id="guardar" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									
								</div>
							</form>
							<div class="load-ajax"></div>
						</div>
					</div>
				</div>


	</section>
<?php endwhile;?>
<!-- Else -->
<?php else:?>
<?php endif;?>
<?php }else{?>
		<script type="text/javascript">
		
			

		</script>
<?php 	} ?>

<?php include('footer.php');?>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/plugins/input-mask/jquery.inputmask.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url');?>/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?php bloginfo('template_url');?>/plugins/numeric/jquery.numeric.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url');?>/plugins/notify/bootstrap-notify.js" type="text/javascript"></script>
<!-- date-range-picker -->
<script src="<?php bloginfo('template_url');?>/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php bloginfo('template_url');?>/plugins/daterangepicker/daterangepicker.js"></script>


<script type="text/javascript" src="<?php bloginfo('template_url');?>/plugins/ajax/despacho.js"></script>