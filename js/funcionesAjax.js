
    // Ajax Request - Save Data
  function Mostrar(btn){
       console.log(btn.value);
       console.log(btn);
        var datal = {
            'action': 'get_post_information',
            'id_user': btn.value
        };

        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: datal, 
            dataType: 'json',
            success: function(data) { 
           
               jQuery('#ID').val(data[0].ID);
                jQuery('#user_nicename').val(data[0].user_nicename);
                jQuery('#first_name').val(data[0].first_name);
                jQuery('#last_name').val(data[0].last_name);
                jQuery('#user_email').val(data[0].user_email);
                jQuery('#billing_phone').val(data[0].billing_phone);
            }   
        });
  };



function Guardar() {
    var datal = {
            'action': 'guardar_info_vendedor',
            'ID':   jQuery('#ID').val(),
            'user_nicename':   jQuery('#user_nicename').val(),
            'first_name':    jQuery('#first_name').val(),
            'last_name':    jQuery('#last_name').val(),
            'user_email':   jQuery('#user_email').val(),
            'billing_phone':   jQuery('#billing_phone').val()
        };

        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: datal, 
            dataType: 'json',
            success: function(data) { 
                location.reload();
            }   
        });
  };

function MostrarVendedor(btn){
       
        var datal = {
            'action': 'get_post_information',
            'id_user': btn.value
        };

        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: datal, 
            dataType: 'json',
            success: function(data) { 
                jQuery('#user_id').val(btn.value);
                jQuery('#id_vendedor').val(data[0].id_vendedor);
            }   
        });
  };

function GuardarDatosClientes() {

      var datal = {
            'action': 'guardar_info_cliente',
            'user_id':   jQuery('#user_id').val(),
            'id_vendedor':   jQuery('#id_vendedor').val()
        };

        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: datal, 
            dataType: 'json',
            success: function(data) { 
                location.reload();
            }   
        });     
};

function CambiarVendedor(userd_id,vendor_id) {

      var datal = {
            'action': 'guardar_info_cliente',
            'user_id':   userd_id,
            'id_vendedor':   vendor_id
        };

        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: datal, 
            dataType: 'json',
            success: function(data) { 
                 window.location.href = data;
            }   
        });     
};