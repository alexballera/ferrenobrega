<?php /* Template Name: Contacto*/ include('header.php');?>
<?php include('head.php');?>


<?php if(have_posts()) : while(have_posts()) : the_post();?>
	<section class="content-wrap">
		<div class="container page-contact">
			<h2><?php the_title();?></h2>
			<?php the_breadcrumb();?>
			<div class="row"style=" padding-bottom: 30px;">
				<div class="col-xs-12 col-sm-12 col-md-6">
					<h3>Mensaje Directo</h3>
					<?php if (function_exists('iphorm')) echo iphorm(6); ?>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6">
					<?php the_content();?>
					<h3>Síguenos</h3>
					<a class="" href="http://www.facebook.com/compugatecenter" target="_blank">
						<i class="fa fa-facebook-official fa-3x"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</a>
					<a class="" href="http://twitter.com/compugatecenter" target="_blank">
						<i class="fa fa-twitter fa-3x"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</a>
					
					<a class="" href="http://instagram.com/compugatecenter" target="_blank">
						<i class="fa fa-instagram fa-3x"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</a>
				
				</div>
			</div>
		</div>
</section>

<div class="nopad hidden-xs hidden-sm col-md-12">
<iframe src="https://www.google.com/maps/d/embed?mid=z9XHXCgDQs-Y.kEiXvR8boiAY" width="100%" height="480"frameborder="0" style="border:0; display: table;" allowfullscreen></iframe> 
</div>
	


<?php endwhile;?>
<!-- Else -->
<?php else:?>
<?php endif;?>
<?php include('footer.php');?>