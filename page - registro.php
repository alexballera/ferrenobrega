<?php 
/*
Template Name: pagina registro
*/
?>
<?php include('header.php');?>
<?php include('head.php');?>

<?php if(have_posts()) : while(have_posts()) : the_post();?>
	<section class="content-wrap">
		<div class="container page">
			<?php 	 $user = wp_get_current_user(); ?>
			<h2><?php the_title();?></h2>
			<?php the_breadcrumb();?>


			<?php 	 
			$id_vendedor = getIdVendedor($user->ID);

			if($id_vendedor[0]==5){
		  		?>
		  			<?php if (function_exists('iphorm')) echo iphorm(20); ?>
		  		<?php
		  	}
		  	if($id_vendedor[0]==8){
		  		?>
		  			<?php if (function_exists('iphorm')) echo iphorm(21); ?>
		  		<?php
		  	}
		  	if($id_vendedor[0]==9){
		  		?>
		  			<?php if (function_exists('iphorm')) echo iphorm(19); ?>
		  		<?php
		  	}
		  	if($id_vendedor[0]==10){
		  		?>
		  			<?php if (function_exists('iphorm')) echo iphorm(16); ?>
		  		<?php
		  	}
		  	if($id_vendedor[0]==109){
		  		?>
		  			<?php  echo ('

		 <p style="text-align: left; font-size: 20px;">Disculpen las molestias estimados usuarios, nos encontramos actualizando esta sección...</p>
        <p style="text-align: left; font-size: 20px;">Mientras por favor informar sus pagos a cada vendedor mediante una llamada en horarios de oficina.</p>
  
                    <div class="col-xs-12 text-center" style="top: 20px;/* align-content: center; */position:  relative;left: 15%;" > 
                  <span class="i-bg" style="position: relative;right: 29%;" >
                  <i class="fa fa-envelope" style="color: white;"></i>
                  </span><strong><p  style="position: absolute;top: 5%;left: 25%;"  >Email</p></strong><br>
                  <strong style="position: relative;left: -15%;top: 10px;">javierguillen@compugatecenter.com.ve</strong>
                  <hr/ style="border-top: 0px solid #eee;">  
				<span class="i-bg" style="position: relative;right: 29%;">
                <i class="fa fa-phone" style="color: white;"></i>
                </span>
                <strong><p style="position: absolute;top: 38%;left: 25%;" >Telefonos</p></strong><br>
                <hr/ style="border-top: 0px solid #eee;">

												<table  style="position: RELATIVE;LEFT: 25%;">
												<tbody>
												<tr>
												<td width="157">
												<table class=" aligncenter" width="100%">
												<tbody>
												<tr>
												<td>(0243)- 2470987

												(0243)-2477544

												(0243)-2474547

												(0243)-2461455
													
												(0414) 335.19.12
												
												
												</td>
												
												</tr>
												<td>
												
												<strong>EXT. (105)</strong>	
												</td>

												</tbody>
												</table>
												</td>
												</tr>
												</tbody>
												</table>


												
												   </div> 
												               </p>


'); ?>
		  		<?php
		  	}
		  	?>
			<?php the_content();?>

		</div>
	</section>
<?php endwhile;?>
<!-- Else -->
<?php else:?>
<?php endif;?>
<?php include('footer.php');?>

<?php 
	
	 if(empty($user->ID)){
		  
		  		?>
			  	<script type="text/javascript">
					jQuery(function ($) {
							jQuery('#myModal').modal('show')
					 });
				</script>		
			  	<?php
		  
	 }else{
	 	$id_vendedor = getIdVendedor($user->ID);
	 	
	 	$name = getNameVendedor($id_vendedor[0]);
	 	
	 	?>
	  	<script type="text/javascript">
			jQuery("input[name=iphorm_8_112]").val("<?php echo $name;?>");
		</script>		
	  	<?php

	  	
	 }
?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">¡Aviso!</h3>
      </div>
      <div class="modal-body text-center">
       	<p> Antes de llenar el formulario de pago es indispensable registrarse en nuestra web (no te tomará más de 1 minuto y tus datos estarán a tu alcance para una próxima compra).</p>

      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-default" data-dismiss="modal" style="background-color: #379712 !important; border-radius: 0 !important; color: #fff !important; font-size: 16px; font-weight: 400; padding: 15px 40px !important; text-transform: uppercase; transition: all 0.3s ease-out 0s; margin: 2.5px;">¡Ok! Vamos</a>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
	jQuery(".iphorm_8_112-element-wrap").hide(); 

	jQuery(".iphorm_100_112-element-wrap").hide();

	jQuery(".iphorm_11_112-element-wrap").hide();
	jQuery(".iphorm_12_112-element-wrap").hide();
	jQuery(".iphorm_13_112-element-wrap").hide();
	jQuery(".iphorm_14_112-element-wrap").hide();
	jQuery("button[name=iphorm_submit]").click(function() {
            // Recargo la página
            setInterval(function(){
		        location.reload();;
		    }, 20000);
        });

	jQuery('#myModal').on('hidden.bs.modal', function (e) {
	   window.location.href = "<?php bloginfo('home');?>/mi-cuenta";
	});
</script>	