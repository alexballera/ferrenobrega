<?php
/**
 * Customer processing order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<?php
//------------------------------------
   //           DATOS DEL VENDOR INICIO
   //------------------------------------
        global $wpdb;
     $user = wp_get_current_user();

     
     $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$user->ID;
     $resultado = $wpdb->get_results($query);
     $vendedor = "no";

     
     foreach ($resultado as $r) {
      if($r->meta_key=="id_vendedor"){
       $vendedor=$r->meta_value;
      }
     }

     $query = "SELECT * FROM wp_users WHERE ID = ".$vendedor;

     $email_vendedor = $wpdb->get_results($query);

     $email_vendedor = $email_vendedor[0]->user_email;

     $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$vendedor;

     $resultado = $wpdb->get_results($query);
     $img = get_avatar($vendedor);

   
     foreach ($resultado as $r) {
      if($r->meta_key=="first_name"){
       $name_vendedor=$r->meta_value;
      }
      if($r->meta_key=="last_name"){
       $last_name_vendedor=$r->meta_value;
      }
      if ($r->meta_key=="billing_phone") {
       $phone_vendedor=$r->meta_value;
      }
     }

   //------------------------------------
   //           DATOS DEL VENDOR FIN
   //------------------------------------



?>






<?php do_action('woocommerce_email_header', $email_heading); ?>

<p><?php _e( "Your order has been received and is now being processed. Your order details are shown below for your reference:", 'woocommerce' ); ?></p>

<?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text ); ?>

<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( $order->is_download_permitted(), true, $order->has_status( 'processing' ) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

	<p style="margin: 16px 0 16px 0;"> Si ya realizaste tu pago en cualquiera de nuestras cuentas, informalo a continuación.</p>
	<a id="boton_inner_verde" href="<?php bloginfo('home');?>/registro-de-pago/" target="_blank">INFORMAR PAGO</a>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text ); ?>

<p>
                <div class="text-center">
                  <?php  echo $img; ?><br><br>
                  Saludos <strong><?php  echo $name." ".$last_name; ?></strong> soy <strong><?php  echo $name_vendedor." ".$last_name_vendedor; ?></strong> estoy aquí para ayudarte si tienes dudas puedes llamarme 
                </div><br>
                <div class="col-xs-6 text-center"> 
                  <span class="i-bg">
              <i class="fa fa-phone" style="color: white;"></i>
             </span>
             <?php  echo $phone_vendedor; ?>
                </div>
                
                <div class="col-xs-6 text-center"> 
                  <span class="i-bg">
              <i class="fa fa-envelope" style="color: white;"></i>
             </span>
             <?php echo $email_vendedor; ?>
                </div> 
               </p>





<?php do_action( 'woocommerce_email_footer' ); ?>


