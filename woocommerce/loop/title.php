<?php
/**
 * Product loop title
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="flexbox product-details">
	<h3>

		<?php if( get_field( "titulo_corto" ) ): the_field('titulo_corto'); else: the_title(); endif;?>

	</h3>
</div>