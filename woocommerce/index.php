<?php 
/*
Template Name: paginainicio
*/
?>

<?php include('header.php');?>
<?php include('head.php');?>


<section class="rotate">
	<div class="flexslider principal">
		<ul class="slides">
			<li><a href="<?php bloginfo('home');?>/tienda"><img src="<?php bloginfo('template_url');?>/images/banner1_compugate.jpg"></a></li>
			<li><img src="<?php bloginfo('template_url');?>/images/banner2.jpg"></a></li>
		</ul>
	</div>
</section>


<div class="clearfloat"><br></div>

<section class="productos hidden-xs">
	

	<div class="container">
	    <section style=" height: 10px; display: table;width: 83.1%;height: 5px; background: rgb(32, 171, 255);position: absolute;z-index: 999;margin-left: 5px;"></section>
		<h3 class="front-title"><span>Lo Más Recientes</span></h3>
	</div>
	<div class="container woocommerce">
		<ul class="products">
			<?php	$args = array(
				'post_type' => 'product',
				'posts_per_page' => 8,
				);?>
				<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post();?>
				<?php include('loop-slider.php');?>
				
			<?php endwhile;	}	else { echo __( 'No products found' ); }	wp_reset_query();	?>
		</ul>
	</div>
</section>

<div class="clearfloat"><br></div>

<section class="productos hidden-xs">
	<div class="container">
	<section style=" height: 10px; display: table;width: 83.1%;height: 5px; background: rgb(32, 171, 255);position: absolute;z-index: 999;margin-left: 5px;"></section>
		<h3 class="front-title"><span>Ofertas del Mes</span></h3>
	</div>
	<div class="container woocommerce">
		<div class="productos-slider carrusel flexslider">
			<ul class="slides products">
				<?php
				$args = array(
					'post_type'      => 'product',
					'posts_per_page' => 8,
					'meta_query'     => array(
						'relation' => 'OR',
						array(
							'key'           => '_sale_price',
							'value'         => 0,
							'compare'       => '>',
							'type'          => 'numeric'
							),
						array(
							'key'           => '_min_variation_sale_price',
							'value'         => 0,
							'compare'       => '>',
							'type'          => 'numeric'
							)
						)
					);
					?>
					<?php $loop = new WP_Query( $args ); if ( $loop->have_posts() ) { while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php include('loop-slider.php');?>
				<?php endwhile; } else {	echo __( '<h3>No hay productos en oferta</h3>' );} wp_reset_postdata();?>
			</ul>
		</div>
	</div>
</section>

<section class="bottom">
<section style=" height: 10px; display: table;width: 83.1%;height: 5px; background: rgb(32, 171, 255);position: absolute;z-index: 999;margin-left: 109px;"></section>
	<div class="container"><h3 class="front-title"><span>Y Aun Hay Más</span></h3></div>
	<div class="clearfloat"><br></div>
	<div class="container marketing">
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<a href="<?php bloginfo('home');?>/categoria-producto/routers"><img src="<?php bloginfo('template_url');?>/images/banner-peque1.jpg" class="img-responsive center-block"></a>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg"><br></div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<a href="<?php bloginfo('home');?>/categoria-producto/tablets"><img src="<?php bloginfo('template_url');?>/images/banner-peque2.jpg" class="img-responsive center-block"></a>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg"><br></div>
			</div>
			<div class="col-xs-12 col-sm-4">
				<a href="<?php bloginfo('home');?>/categoria-producto/cableado"><img src="<?php bloginfo('template_url');?>/images/banner-peque3.jpg" class="img-responsive center-block"></a>
				<div class="col-xs-12 hidden-sm hidden-md hidden-lg"><br></div>
			</div>
		</div>
	</div>
</section>







<?php if(have_posts()) : while(have_posts()) : the_post();?>	
<section class="content-wrap">		
<div class="container page">			
		
<?php the_content();?>	
	</div>	
	</section>
	<?php endwhile;?>
	<!-- Else -->
	<?php else:?>
	<?php endif;?>
	
	
	<section class="productos">
	<div class="container">
		<h3 class="text-center">TRABAJAMOS CON LAS MEJORES MARCAS</h3>
	</div>
	<div class="clearfix"><br></div>
	<div class="rotate-products">
		<div class="container">
			<ul id="slides">
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/apple-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/epson-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/hp-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/logo-microsoft.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/logo-samsung.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/sandisk-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/lenovo-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/lg-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/dell-logo.png" alt=""></li>
				<li><img src="<?php bloginfo('template_url');?>/images/marcas/tplink-logo.png" alt=""></li>
			</ul>
		</div>
	</div>
</section>
	
	
	
	
	<script type="text/javascript" src="<?php bloginfo('template_url');?>/js/jquery.flexisel.js"></script>
<script type="text/javascript">
jQuery(window).load(function() {
	jQuery("#slides").flexisel({
		visibleItems: 6,
		animationSpeed: 200,
		autoPlay: false,
		autoPlaySpeed: 3000,
		pauseOnHover: true,
		clone:false,
		enableResponsiveBreakpoints: true,
		responsiveBreakpoints: {
			portrait: {
				changePoint:480,
				visibleItems: 1
			},
			landscape: {
				changePoint:640,
				visibleItems: 2
			},
			tablet: {
				changePoint:768,
				visibleItems: 3
			}
		}
	});
});
</script>



<div class="nopad hidden-xs hidden-sm col-md-12">
	<iframe src="https://www.google.com/maps/d/u/0/embed?mid=z8aK2V01AkPw.kYNH0T6STKW8" width="100%" height="480" frameborder="0" style="border:0; display: table;" allowfullscreen></iframe>
</div>
	
	
	
	<?php include('footer.php');?>