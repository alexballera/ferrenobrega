<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>

<div class="flexbox-3 col-xs-12 col-sm-12 col-md-6">

<div class="images" style="min-height: 547px;">

	<?php if ( $product->is_on_sale() ) : ?>

	<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . __( 'Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>

<?php endif; ?>



<?php
if ( has_post_thumbnail() ) {

	$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
	$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
	$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );
	$home					= get_template_directory_uri();
	$imagen_full = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
	$ruta_imagen = $imagen_full[0];

	$attachment_count = count( $product->get_gallery_attachment_ids() );

	if ( $attachment_count > 0 ) {
		$gallery = '[product-gallery]';
	} else {
		$gallery = '';
	}

	echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="'.$title.'" data-rel="prettyPhoto[product-gallery]"><img src="'.$ruta_imagen.'" alt="'.$imagen_title.'"></a>', $image_link, $image_caption, $imagen_full ), $post->ID );

} else {

	echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

}
?>

</div>

<?php do_action( 'woocommerce_product_thumbnails' ); ?>


</div>
