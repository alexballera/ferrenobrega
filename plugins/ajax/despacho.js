var tabla_ordenes = jQuery('#table-despacho').DataTable({
		ajax: ajaxurl+'?action=listar_despacho',
		columns: [
			{ data: 'order_id', name: 'order_id'},
			{ data: 'cliente', name: 'cliente'},
			{ 
				data: 'status', 
			 	name: 'status',
			 	render: function(data, type, full){
			 		var text;
			 		if(data=="Despachado")
			 		{
			 			text = "<span class='label label-success'>"+data+"</span>"
			 		}
			 		else
			 		{
			 			
			 			text = "<span class='label label-danger'>"+data+"</span>"
			 		
			 		}
			 		return text;
			 	}
			 },
			{ data: 'num_factura', name: 'num_factura'},
			{ data: 'fecha_factura', name: 'fecha_factura'},
			{ 
				data: 'order_id',
				render: function(data, type, full){
					
					var text = full.accion_ver+full.accion;
					
					return text;
				}

			},
			
		],
		"order": [[ 0, "desc" ]],
		scrollY:  "500px",
        scrollCollapse: true,
        language:lenguaje
	});

jQuery('.numeric').numeric('.');

	jQuery('.numeric').keyup(function () {
	    if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
	       this.value = this.value.replace(/[^0-9\.]/g, '');
	    }
	});

 //Datemask dd/mm/yyyy
 jQuery("#fecha_despacho").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});


 jQuery('#modal-guia').on('hidden.bs.modal', function (e) {
        jQuery('#form-factura')[0].reset();
        limpiarCampos();
       
    });

 jQuery('#guardar').click(function(){
		if(validarCampos()=="si"){
			load_inicio();
			var $btn = jQuery(this).button('loading');
	    	var data = {
	    		'post_id':jQuery('#order_id').val(),
	    		'numero_de_guia':jQuery('#numero_de_guia').val(),
	    		'obj_orden':jQuery('#obj_orden').val(),
	    		'fecha_despacho': jQuery('#fecha_despacho').val()
	    	};

	    	var route = ajaxurl+'?action=get_guia';

	    	jQuery.ajax({
	    		url: route,
	    		data: data,
	    		method: "POST",
	    		dataType: 'json',
	    		success:function(res){
	    			load_cerrar();
	    			jQuery('#estado_orden').html(res.estado);
	    			notificar(res.msj,res.type,res.icon)
	    			tabla_ordenes.ajax.reload();
	    			$btn.button('reset');
	    		},
	    		error: function(res){

	    		}
	    	})
	    }
});

function cargarOrden(id,status){
		

		load_inicio();

		jQuery('#num_pedido').html(id);
		jQuery('#estado_pedido').html(status);
		var route = ajaxurl+'?action=detalles_orden&order_id='+id;

		jQuery.get( route, function( data ) {
			 
			jQuery('#detalles_pedido').fadeIn(10000).html(data);

			load_cerrar();

		},"json" );
	}

function cargarPedido(id,status){

		load_inicio();


    	jQuery('#num_orden').html(id);
		jQuery('#estado_orden').html(status);


		jQuery('#order_id').val(id); 
		var route = ajaxurl+'?action=get_factura_pedido&order_id='+id;

		jQuery.get( route, function( data ) {
			 
			

			load_cerrar();

    		jQuery('#num_factura').val(data.num_factura);
    		jQuery('#fecha_factura').val(data.fecha_factura);
    		jQuery('#empresa_envio').val(data.empresa_envio);
    		jQuery('#obj_orden').val(data.obj_orden);
    		jQuery('#monto_seguro').val(data.monto_seguro);
    		jQuery('#numero_de_guia').val(data.numero_de_guia);
    		jQuery('#fecha_despacho').val(data.fecha_despacho);

		},"json" );
	}


function generarPDF(url){
		var id = jQuery("#num_pedido").html();
		window.location = url+'?order_id='+id;
}

function load_inicio(){
	jQuery('.load-ajax').addClass('overlay');
	jQuery('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
}

function load_cerrar(){
	jQuery('.load-ajax').removeClass('overlay');
	jQuery('.load-ajax').html('');
}


function validarCampos(){
	limpiarCampos();
	var validar = "si";
	if(jQuery('#numero_de_guia').val()==""){
		jQuery('#numero_de_guia_form').addClass('has-error');
		jQuery('#numero_de_guia_msj').html('Este campo es requerido ');
		validar = "no";
	}
	if(jQuery('#fecha_despacho').val()==""){
		jQuery('#fecha_despacho_form').addClass('has-error');
		jQuery('#fecha_despacho_msj').html('Este campo es requerido ');
		validar = "no";
	}
	return validar;
}

function limpiarCampos(){
	
	
		jQuery('#numero_de_guia_form').removeClass('has-error');
		jQuery('#numero_de_guia_msj').html('');
		
		jQuery('#fecha_despacho_form').removeClass('has-error');
		jQuery('#fecha_despacho_msj').html('');
		
	
}

function notificar(mensaje,type = 'success',icon = "fa fa-check"){
	jQuery.notify({
    	icon: icon,
    	message: mensaje
    	
    },{
        type: type,
        timer: 2000,
        placement: {
            from: 'top',
            align: 'center'
        },
        z_index: 9999999,
	});
}

