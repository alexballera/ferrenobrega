	

	var tabla_cliente = jQuery('#tabla-clientes').DataTable({
		ajax: ajaxurl+'?action=listar_clientes',
		columns: [
			{ data: 'ID', name: 'ID'},
			{ data: 'nombre_completo', name: 'nombre_completo'},
			{ data: 'user_nicename', name: 'user_nicename'},
			{ data: 'user_email', name: 'user_email'},
			{ data: 'billing_phone', name: 'billing_phone'},
			{ data: 'vendedor', name: 'vendedor'},
		],
		scrollY:  "500px",
        scrollCollapse: true,
        language:lenguaje
	});

	var tabla_ordenes = jQuery('#tabla-ordenes').DataTable({
		ajax: ajaxurl+'?action=listar_ordenes',
		columns: [
			{ data: 'order_id', name: 'order_id'},
			{ data: 'cliente', name: 'cliente'},
			{ data: 'date', name: 'date'},
			{ 
				data: 'status', 
			 	name: 'status',
			 	render: function(data, type, full){
			 		var text;
			 		if(data=="Despachado")
			 		{
			 			text = "<span class='label label-success'>"+data+"</span>"
			 		}
			 		else if(data=="Cliente Notificado")
			 		{
			 			text = "<span class='label label-primary'>"+data+"</span>"
			 		}
			 		else
			 		{
			 			
			 			text = "<span class='label label-danger'>"+data+"</span>"
			 		
			 		}
			 		return text;
			 	}
			 },
			{ data: 'payment_method', name: 'payment_method'},
			{ data: 'total', name: 'total'},
			{ data: 'vendedor', name: 'vendedor'},
			{ 
				data: 'order_id',
				render: function(data, type, full){
					
					var text = full.accion_ver;
					if(full.id_user_actual == full.id_vendedor || full.role=="administrator"){
						text = text+full.accion_factura	
						text = text+full.accion_mensaje	
					}
					if(full.status == "Despachado"){
						text = text+full.accion_notificar
					}
					return text;
				}

			},
		],
		"order": [[ 0, "desc" ]],
		scrollY:  "500px",
        scrollCollapse: true,
        language:lenguaje
	});



	jQuery('.numeric').numeric('.');

	jQuery('.numeric').keyup(function () {
	    if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
	       this.value = this.value.replace(/[^0-9\.]/g, '');
	    }
	});

      //Datemask dd/mm/yyyy
    jQuery("#fecha_factura").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});

    jQuery('#cargar-observacion').on('hidden.bs.modal', function (e) {
        jQuery('#form-observacion')[0].reset();
        limpiarCamposObservacion();
       
    });

    jQuery('#cargar-factura').on('hidden.bs.modal', function (e) {
        jQuery('#form-factura')[0].reset();
        limpiarCamposFActura();
       
    });

    var startDate = new Date();
	var endDate = new Date();


    
	    jQuery('#fecha-excel').daterangepicker({
	    	startDate: startDate,
          	endDate: endDate,
	        locale: {
	            format: 'DD/MM/YYYY',
	            cancelLabel: 'Cancelar',
	            applyLabel: 'Seleccionar'
	        }
	    }, 
	    function(start, end, label) {
	        startDate = start;
	        endDate = end;
	    });
	

	function cargarOrden(id,status){
		

		jQuery('.load-ajax').addClass('overlay');

    	jQuery('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');

		jQuery('#num_pedido').html(id);
		jQuery('#estado_pedido').html(status);
		var route = ajaxurl+'?action=detalles_orden&order_id='+id;

		jQuery.get( route, function( data ) {
			 
			jQuery('#detalles_pedido').fadeIn(10000).html(data);

			jQuery('.load-ajax').removeClass('overlay');

    		jQuery('.load-ajax').html('');

		},"json" );
	}

	function cargarPedido(id,status){

		if(status == "Despachado"||status == "Cliente Notificado"){
			jQuery(".input").prop('disabled', true);
		}else{
			jQuery(".input").prop('disabled', false);
		}

		jQuery('.load-ajax').addClass('overlay');

    	jQuery('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');


    	jQuery('#num_orden').html(id);
		jQuery('#estado_orden').html(status);


		jQuery('#order_id').val(id); 
		var route = ajaxurl+'?action=get_factura_pedido&order_id='+id;

		jQuery.get( route, function( data ) {
			 
			

			jQuery('.load-ajax').removeClass('overlay');

    		jQuery('.load-ajax').html('');

    		jQuery('#num_factura').val(data.num_factura);
    		jQuery('#fecha_factura').val(data.fecha_factura);
    		jQuery('#empresa_envio').val(data.empresa_envio);
    		jQuery('#monto_seguro').val(data.monto_seguro);
    		jQuery('#obj_orden').val(data.obj_orden);

		},"json" );
	}

	function cargarObservacion(id){
		load_inicio();


    	jQuery('.num_pedido').html(id);


		
		var route = ajaxurl+'?action=get_factura_pedido&order_id='+id;

		jQuery.get( route, function( data ) {

			load_cerrar();

    		jQuery('#cliente_obj').val(data.nombre_cliente);
    		jQuery('#email_obj').val(data.email_cliente);
    		

		},"json" );
	} 

	jQuery('#guardar').click(function(){
		if(validarCamposFActura()=="si"){
			load_inicio();
			var $btn = jQuery(this).button('loading');
	    	var data = {
	    		'post_id':jQuery('#order_id').val(),
	    		'num_factura':jQuery('#num_factura').val(),
	    		'fecha_factura': jQuery('#fecha_factura').val(),
	    		'empresa_envio': jQuery('#empresa_envio').val(),
	    		'monto_seguro': jQuery('#monto_seguro').val(),
	    		'obj_orden': jQuery('#obj_orden').val()
	    	};

	    	var route = ajaxurl+'?action=set_factura_pedido';

	    	jQuery.ajax({
	    		url: route,
	    		data: data,
	    		method: "POST",
	    		dataType: 'json',
	    		success:function(res){
	    			load_cerrar();
	    			jQuery('#estado_orden').html(res.estado);
	    			notificar(res.msj,res.type,res.icon)
	    			tabla_ordenes.ajax.reload();
	    			$btn.button('reset');
	    		},
	    		error: function(res){

	    		}
	    	})
	    }
	});
	
	jQuery('#enviar').click(function(){
		load_inicio();
			var $btn = jQuery(this).button('loading');
	    	var data = {
	    		'post_id':jQuery('#order_id').val()
	    	};

	    	var route = ajaxurl+'?action=enviar_despacho';

	    	jQuery.ajax({
	    		url: route,
	    		data: data,
	    		method: "POST",
	    		dataType: 'json',
	    		success:function(res){
	    			load_cerrar();
	    			jQuery('#estado_orden').html(res.estado);
	    			notificar(res.msj,res.type,res.icon)
	    			tabla_ordenes.ajax.reload();
	    			$btn.button('reset');
	    		},
	    		error: function(res){

	    		}
	    	})
	});

	function load_inicio(){
		jQuery('.load-ajax').addClass('overlay');
    	jQuery('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
	}

	function load_cerrar(){
		jQuery('.load-ajax').removeClass('overlay');
    	jQuery('.load-ajax').html('');
	}


	function validarCamposFActura(){
		limpiarCamposFActura();
		var validar = "si";
		if(jQuery('#num_factura').val()==""){
			jQuery('#num_factura_form').addClass('has-error');
    		jQuery('#num_factura_msj').html('Este campo es requerido ');
    		validar = "no";
		}
		if(jQuery('#fecha_factura').val()==""){
			jQuery('#fecha_factura_form').addClass('has-error');
    		jQuery('#fecha_factura_msj').html('Este campo es requerido ');
    		validar = "no";
		}
		if(jQuery('#empresa_envio').val()==""){
			jQuery('#empresa_envio_form').addClass('has-error');
    		jQuery('#empresa_envio_msj').html('Este campo es requerido ');
    		validar = "no";
		}
		if(jQuery('#monto_seguro').val()==""){
			jQuery('#monto_seguro_form').addClass('has-error');
    		jQuery('#monto_seguro_msj').html('Este campo es requerido ');
    		validar = "no";
		}
		return validar;
	}

	function limpiarCamposFActura(){
		
		
			jQuery('#num_factura_form').removeClass('has-error');
    		jQuery('#num_factura_msj').html('');
    		
		
		
			jQuery('#fecha_factura_form').removeClass('has-error');
    		jQuery('#fecha_factura_msj').html('');
    		
		
		
			jQuery('#empresa_envio_form').removeClass('has-error');
    		jQuery('#empresa_envio_msj').html('');
    		
		
		
			jQuery('#monto_seguro_form').removeClass('has-error');
    		jQuery('#monto_seguro_msj').html('');
    		
		
	}

	function validarCamposObservacion(){
		limpiarCamposObservacion();
		var validar = "si";
		if(jQuery('#asunto_obj').val()==""){
			jQuery('#asunto_obj_input').addClass('has-error');
    		jQuery('#asunto_obj_msj').html('Este campo es requerido ');
    		validar = "no";
		}
		if(jQuery('#contenido_obj_mensaje').val()==""){
			jQuery('#contenido_obj_mensaje_input').addClass('has-error');
    		jQuery('#contenido_obj_mensaje_msj').html('Este campo es requerido ');
    		validar = "no";
		}
		return validar;
	}

	function limpiarCamposObservacion(){
		
		
			jQuery('#asunto_obj_input').removeClass('has-error');
    		jQuery('#asunto_obj_msj').html('');
		
		
			jQuery('#contenido_obj_mensaje_input').removeClass('has-error');
    		jQuery('#contenido_obj_mensaje_msj').html('');
    		
		
	}

	function notificar(mensaje,type = 'success',icon = "fa fa-check"){
		jQuery.notify({
	    	icon: icon,
	    	message: mensaje
	    	
	    },{
	        type: type,
	        timer: 2000,
	        placement: {
	            from: 'top',
	            align: 'center'
	        },
	        z_index: 9999999,
		});
	}

	function generarExcel(url){

	

		var i = formatDate(startDate);

		var f = formatDate(endDate);

		console.log(i);

		console.log(f);


		window.location = url+'?inicio='+i+'&fin='+f;
	}

	function generarPDF(url){
		var id = jQuery("#num_pedido").html();
		window.location = url+'?order_id='+id;
	}

	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}

	function notificarCliente(id){
		
		

    	jQuery('#orden_notificar').html(id);

		
	}

	jQuery('#si').click(function(){
		var $btn = jQuery(this).button('loading');
		load_inicio();
		var route = ajaxurl+'?action=notificar_cliente&order_id='+jQuery('#orden_notificar').html();

		jQuery.get( route, function( res ) {
			
			load_cerrar();
			notificar(res.msj,res.type,res.icon);
			$btn.button('reset');
			tabla_ordenes.ajax.reload();
			jQuery('#confirmar').modal('hide');

		},"json" );

	});

	jQuery('#enviar_obj').click(function(){
		var validar = validarCamposObservacion();

		if(validar=="si"){


			var $btn = jQuery(this).button('loading');
			load_inicio();
			var route = ajaxurl+'?action=enviar_mensaje_cliente&order_id='+jQuery('#orden_notificar').html();

			var data = {
				'order_id'  : jQuery('.num_pedido').html(),
				'asunto' : jQuery('#asunto_obj').val(),
				'mensaje': jQuery('#contenido_obj_mensaje').val()
			};

			jQuery.ajax({
	    		url: route,
	    		data: data,
	    		method: "POST",
	    		dataType: 'json',
	    		success:function(res){
	    			load_cerrar();
	    			jQuery('#estado_orden').html(res.estado);
	    			notificar(res.msj,res.type,res.icon)
	    			tabla_ordenes.ajax.reload();
	    			$btn.button('reset');
	    		},
	    		error: function(res){

	    		}
	    	});

    	}

	});

