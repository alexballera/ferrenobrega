
	jQuery('#sub_categoria').select2({
		placeholder: 'Select an option'
	});
	
	jQuery('.numeric').numeric('.');

	jQuery('.numeric').keyup(function () {
	    if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
	       this.value = this.value.replace(/[^0-9\.]/g, '');
	    }
	});


	var tabla = jQuery('#tabla-producto').DataTable({
		ajax: ajaxurl+'?action=cargar_data_table',
		columns: [
			{ data: 'producto', name: 'producto',},
			{ 
				data: 'categoria',
				name: 'categoria',
				render: function ( data, type, full, meta ) {
					var text;
					if(data==false){
						text = "Sin Etiqueta"
					}else{
						text = data;
					}
                    
                    return text;
                }
			},
			{ 
				data: 'precio_regular',
			 	name: 'precio_regular',
			 	render: function ( data, type, full, meta ) {
                    var text = data+' '+full.moneda;
                    return text;
                }
			 },
			{ 
				data: 'precio_venta', 
				name: 'precio_venta',
				render: function ( data, type, full, meta ) {
                    var text = data+' '+full.moneda;
                    return text;
                }
			},
			{
                data: 'accion',name: 'accion'
            }
		],
		scrollY:  "500px",
        scrollCollapse: true,
        language:{
			    "decimal":        "",
			    "emptyTable":     "No hay datos disponibles en la tabla",
			    "info":           "Mostrando  _START_ a _END_ de _TOTAL_ entradas ",
			    "infoEmpty":      "Mostrando  0 a 0 de 0 entradas ",
			    "infoFiltered":   "(filtrada a partir de  _MAX_  entradas )",
			    "infoPostFix":    "",
			    "thousands":      ",",
			    "lengthMenu":     "Mostra _MENU_ entradas ",
			    "loadingRecords": "Cargando...",
			    "processing":     "Tratamiento...",
			    "search":         "Buscar:",
			    "zeroRecords":    "No se encontraron registros coincidentes",
			    "paginate": {
			        "first":      "Primero",
			        "last":       "Último",
			        "next":       "Siguiente",
			        "previous":   "Anterior"
			    },
			    "aria": {
			        "sortAscending":  ": activar para ordenar la columna ascendente",
			        "sortDescending": ": activar para ordenar la columna descendente"
			    }
		}
	});

	if(jQuery('input:radio[name=tipo_de_calculo_general]:checked').val()==1){
		jQuery('#porcentaje-general').show();
		jQuery('#monto-general').hide();
		jQuery( "#calculo_radio_ajuste_1" ).prop( "disabled", true );
		jQuery( "#calculo_radio_1" ).prop( "checked", true );
	}else{
		jQuery('#porcentaje-general').hide();
		jQuery('#monto-general').show();
		jQuery( "#calculo_radio_ajuste_1" ).prop( "disabled", false );
	}

	


	jQuery('input:radio[name=tipo_de_calculo_general]').change(function(){
		if(jQuery(this).val()==1){
			jQuery('#porcentaje-general').show();
			jQuery('#monto-general').hide();
			jQuery( "#calculo_radio_ajuste_1" ).prop( "disabled", true );
			jQuery( "#calculo_radio_1" ).prop( "checked", true );
		}else{
			jQuery('#porcentaje-general').hide();
			jQuery('#monto-general').show();
			jQuery( "#calculo_radio_ajuste_1" ).prop( "disabled", false );
		}
	});


	if(jQuery('input:radio[name=tipo_de_calculo_categoria]:checked').val()==1){
		jQuery('#porcentaje-categoria').show();
		jQuery('#monto-categoria').hide();
		jQuery( "#calculo_radio_ajuste_2" ).prop( "disabled", true );
		jQuery( "#calculo_radio_3" ).prop( "checked", true );
	}else{
		jQuery('#porcentaje-categoria').hide();
		jQuery('#monto-categoria').show();
		jQuery( "#calculo_radio_ajuste_2" ).prop( "disabled", false );
	}

	jQuery('input:radio[name=tipo_de_calculo_categoria]').change(function(){
		if(jQuery(this).val()==1){
			jQuery('#porcentaje-categoria').show();
			jQuery('#monto-categoria').hide();
			jQuery( "#calculo_radio_ajuste_2" ).prop( "disabled", true );
			jQuery( "#calculo_radio_3" ).prop( "checked", true );
		}else{
			jQuery('#porcentaje-categoria').hide();
			jQuery('#monto-categoria').show();
			jQuery( "#calculo_radio_ajuste_2" ).prop( "disabled", false );
		}
	});

	if(jQuery('input:radio[name=tipo_de_calculo_unico]:checked').val()==1){
		jQuery('#porcentaje-unico').show();
		jQuery('#monto-unico').hide();
	}else{
		jQuery('#porcentaje-unico').hide();
		jQuery('#monto-unico').show();
	}

	jQuery('input:radio[name=tipo_de_calculo_unico]').change(function(){
		if(jQuery(this).val()==1){
			jQuery('#porcentaje-unico').show();
			jQuery('#monto-unico').hide();
		}else{
			jQuery('#porcentaje-unico').hide();
			jQuery('#monto-unico').show();
		}
	});

	jQuery('#actualizar-general').click(function(){

		
    	var tipo = jQuery('input:radio[name=tipo_de_calculo_general]:checked').val();

    	var valor;

    	var accion = jQuery('input:radio[name=accion_calculo_general]:checked').val();

    	if(tipo==1)
    	{
    
			valor = jQuery('#input-porcentaje-general').val()
			if(valor==''){
				jQuery('#porcentaje-general').addClass("has-error");
	            jQuery('#error_general_1').html('Este campo es requerido');
	            return false
			}
		}
		else
		{
			valor = jQuery('#input-monto-general').val()
			if(valor==''){
				jQuery('#monto-general').addClass("has-error");
	            jQuery('#error_general_2').html('Este campo es requerido');
	            return false
			}
		}

		var $btn = jQuery(this).button('loading');

    	jQuery('.load-ajax1').addClass('overlay');

    	jQuery('.load-ajax1').html('<i class="fa fa-refresh fa-spin"></i>');


    	var data = {
            'action': 'cambio_de_precio_1',
            'tipo'  : tipo,
            'valor' : valor,
            'accion': accion
        };

        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: data, 
            dataType: 'json',
            success: function(data) { 
            	$btn.button('reset');
				jQuery('.load-ajax1').removeClass('overlay');
                jQuery('.load-ajax1').html('');
                jQuery('#input-porcentaje-general').val("");
				jQuery('#input-monto-general').val("");

				jQuery('#porcentaje-general').removeClass("has-error");
	            jQuery('#error_general_1').html('');
				jQuery('#monto-general').removeClass("has-error");
	            jQuery('#error_general_2').html('');
                tabla.ajax.reload();

                var mensaje ;
                if(data.success==true){
					mensaje = "¡Se han actualizado los precios generales de los productos con éxito!";
					type = 'success';
					icon = "fa fa-check"
				}else{
					mensaje = "No se hará la modificación de precios a los productos cuya actualización en el precio den un monto negativo";
					type = 'danger';
					icon = 'fa fa-warning';
				}

                

                notificar(mensaje,type,icon);
                
            },
            error: function(){
            	$btn.button('reset');
				jQuery('.load-ajax1').removeClass('overlay');
                jQuery('.load-ajax1').html('');
            }   
        });

	});

	jQuery('#actualizar-categoria').click(function(){

		
    	var tipo = jQuery('input:radio[name=tipo_de_calculo_categoria]:checked').val();

    	var valor;

    	var accion = jQuery('input:radio[name=accion_calculo_categoria]:checked').val();

    	
    	var array = jQuery('#sub_categoria').val();
    	
    	if(array==null){
			jQuery('#select_categoria').addClass("has-error");
	        jQuery('#error_categoria_3').html('Este campo es requerido');
	        return false
		}else{
			
	        jQuery('#error_categoria_3').html('');
		}

    	if(tipo==1)
    	{
			valor = jQuery('#input-porcentaje-categoria').val();
			if(valor==''){
				jQuery('#porcentaje-categoria').addClass("has-error");
	            jQuery('#error_categoria_1').html('Este campo es requerido');
	            return false
			}
		}
		else
		{
			valor = jQuery('#input-monto-categoria').val();
			if(valor==''){
				jQuery('#monto-categoria').addClass("has-error");
	            jQuery('#error_categoria_2').html('Este campo es requerido');
	            return false
			}
		}

		

		var $btn = jQuery(this).button('loading');

    	jQuery('.load-ajax2').addClass('overlay');

    	jQuery('.load-ajax2').html('<i class="fa fa-refresh fa-spin"></i>');


    	var data = {
            'action'   : 'cambio_de_precio_2',
            'tipo'     : tipo,
            'valor'    : valor,
            'accion'   : accion,
            'categoria': jQuery('#sub_categoria').val()
        };

        
        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: data, 
            dataType: 'json',
            success: function(data) { 
            	$btn.button('reset');
				jQuery('.load-ajax2').removeClass('overlay');
                jQuery('.load-ajax2').html('');
                jQuery('#input-porcentaje-categoria').val('');
				jQuery('#input-monto-categoria').val('');
				//jQuery('#sub_categoria').val('');
				//jQuery("#sub_categoria").trigger("change.select2");
				//console.log(data.success );
				var mensaje;
				var type;
				var icon;
				
					mensaje = "¡Se han actualizado los precios por etiqueta con éxito!";
					type = 'success';
					icon = "fa fa-check"
				

				jQuery('#porcentaje-categoria').removeClass("has-error");
	            jQuery('#error_categoria_1').html('');
				jQuery('#monto-categoria').removeClass("has-error");
	            jQuery('#error_categoria_2').html('');	

                tabla.ajax.reload();
               
                
                notificar(mensaje,type,icon);

                if(data.negativo){
                	if(data.negativo!=0){
						mensaje = "No se hará la modificación de precios a los productos cuya actualización en el precio den un monto negativo";
						type = 'danger';
						icon = 'fa fa-warning';
						notificar(mensaje,type,icon);
					}
				}
				
            },
            error: function(){
            	$btn.button('reset');
				jQuery('.load-ajax2').removeClass('overlay');
                jQuery('.load-ajax2').html('');

            }   
        });

	});

	function cargarProducto(id,name,regular_preci,sale_preci){
		
		jQuery('#id-producto').val(id);
		jQuery('.modal-title').html(name);
		jQuery('#p_r').html(regular_preci);
		jQuery('#p_s').html(sale_preci);
	}

	jQuery('#modificar-precio-unico').click(function(){

		if(jQuery('#input-precio-regular-unico').val()==''&&jQuery('#input-precio-venta-unico').val()==''){
			return false;
		}
		var $btn = jQuery(this).button('loading');



		var data = {
            'action'        : 'cambio_de_precio_unico',
            'id_producto'   : jQuery('#id-producto').val(),
            'precio_regular': jQuery('#input-precio-regular-unico').val(),
            'precio_venta'  : jQuery('#input-precio-venta-unico').val()
        };

        
        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: data, 
            dataType: 'json',
            success: function(data) { 
            	$btn.button('reset');

            	if(jQuery('#input-precio-regular-unico').val()!="") {
                	
                	jQuery('#p_r').html(jQuery('#input-precio-regular-unico').val());
                }

                if(jQuery('#input-precio-venta-unico').val()!="") {
                	jQuery('#p_s').html(jQuery('#input-precio-venta-unico').val());
                }

				jQuery('#input-precio-regular-unico').val('')
				jQuery('#input-precio-venta-unico').val('')
                tabla.ajax.reload();



                var mensaje = "¡Se ha modificar el precio del producto con éxito!";
                
                notificar(mensaje);

                
            },
            error: function(){
            	$btn.button('reset');
            }   
        });
	});

	jQuery('#actualizar-unico').click(function(){

		

    	var tipo = jQuery('input:radio[name=tipo_de_calculo_unico]:checked').val();

    	var valor;

    	var accion = jQuery('input:radio[name=accion_calculo_unico]:checked').val();

    	if(tipo==1)
    	{
    
			valor = jQuery('#input-porcentaje-unico').val()
			if(valor==''){
				jQuery('#porcentaje-unico').addClass("has-error");
	            jQuery('#error_unico_1').html('Este campo es requerido');
	            return false
			}
		}
		else
		{
			
			valor = jQuery('#input-monto-unico').val()
			if(valor==''){
				jQuery('#porcentaje-unico').addClass("has-error");
	            jQuery('#error_unico_2').html('Este campo es requerido');
	            return false
			}
		}

		var $btn = jQuery(this).button('loading');

    	jQuery('.load-ajax3').addClass('overlay');

    	jQuery('.load-ajax3').html('<i class="fa fa-refresh fa-spin"></i>');

    	var data = {
            'action'   : 'cambio_de_precio_3',
            'tipo'     : tipo,
            'valor'    : valor,
            'accion'   : accion,
            'id_producto': jQuery('#id-producto').val(),
        };

        
        jQuery.ajax({ 
            type: 'POST', 
            url: ajaxurl, 
            data: data, 
            dataType: 'json',
            success: function(data) { 
            	$btn.button('reset');
				jQuery('.load-ajax3').removeClass('overlay');
                jQuery('.load-ajax3').html('');

                jQuery('#porcentaje-unico').removeClass("has-error");
	            jQuery('#error_unico_1').html('');
				jQuery('#porcentaje-unico').removeClass("has-error");
	            jQuery('#error_unico_2').html('');

                tabla.ajax.reload();

                var mensaje ;
                if(data.success==true){
					mensaje = "¡Se ha actualiza el precio del producto con éxito!";
					type = 'success';
					icon = "fa fa-check"
				}else{
					mensaje = "No se hará la modificación de precios a los productos cuya actualización en el precio den un monto negativo";
					type = 'danger';
					icon = 'fa fa-warning';
				}
                
                notificar(mensaje,type,icon);
                
            },
            error: function(){
            	$btn.button('reset');
				jQuery('.load-ajax3').removeClass('overlay');
                jQuery('.load-ajax3').html('');
            }   
        });

	});

	function notificar(mensaje,type = 'success',icon = "fa fa-check"){
		jQuery.notify({
	    	icon: icon,
	    	message: mensaje
	    	
	    },{
	        type: type,
	        timer: 2000,
	        placement: {
	            from: 'top',
	            align: 'center'
	        },
	        z_index: 9999999,
		});
	}

