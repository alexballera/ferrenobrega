
jQuery('#enviar_msj_canbio').click(function(){

	var validar = validarCamposMail();
	if(validar=="si"){
		load_inicio();
		var data = {
			'id'         : jQuery('#id_user_cambio').val(),
			'nombre'     : jQuery('#nombre_user_cambio').val(),
			'email'      : jQuery('#email_user_cambio').val(),
			'telefono'   : jQuery('#telefono_user_cambio').val(),
			'vendedor'   : jQuery('#vendedor_user_cambio').val(),
			'razon'      : jQuery("input[name=razon]:checked").val(),
			'explicacion': jQuery('#explicacion_user_cambio').val()
		};
		var route = ajaxurl+'?action=enviar_correo_cambio';

	    	jQuery.ajax({
	    		url: route,
	    		data: data,
	    		method: "POST",
	    		dataType: 'json',
	    		success:function(res){
	    			load_cerrar();
	    			notificar(res.msj,res.type,res.icon)
	    		},
	    		error: function(res){

	    		}
	    	})
	}

});

if(jQuery('input[name=razon]:checked').val()==1){
	jQuery('#explicacion_user_cambio').hide();
}else{
	jQuery('#explicacion_user_cambio').show();
}

jQuery('input[name=razon]').change(function(){
	if(jQuery(this).val()==1){
		jQuery('#explicacion_user_cambio').hide();
	}else{
		jQuery('#explicacion_user_cambio').show();
	}
});

jQuery('.numeric').keyup(function () {
    if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
       this.value = this.value.replace(/[^0-9\.]/g, '');
    }
});

jQuery('#modal-cambio-vendedor').on('hidden.bs.modal', function (e) {
    jQuery('#from-cambio-vendedor')[0].reset();
    jQuery('#explicacion_user_cambio').hide();
    limpiarCamposMail();
   	
});
 

function load_inicio(){
	jQuery('.load-ajax').addClass('overlay');
	jQuery('.load-ajax').html('<i class="fa fa-refresh fa-spin"></i>');
}

function load_cerrar(){
	jQuery('.load-ajax').removeClass('overlay');
	jQuery('.load-ajax').html('');
}


function validarCamposMail(){
	limpiarCamposMail();
	var validar = "si";

	if(jQuery('#nombre_user_cambio').val()==""){
		jQuery('#nombre_user_cambio_form').addClass('has-error');
		jQuery('#nombre_user_cambio_msj').html('Este campo es requerido ');
		validar = "no";
	}
	if(jQuery('#email_user_cambio').val()==""){
		jQuery('#email_user_cambio_form').addClass('has-error');
		jQuery('#email_user_cambio_msj').html('Este campo es requerido ');
		validar = "no";
	}else{
		var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
	    if (regex.test(jQuery('#email_user_cambio').val().trim())) {
	       
	    } else {
	    	jQuery('#email_user_cambio_form').addClass('has-error');
			jQuery('#email_user_cambio_msj').html('Este email no es valido');
	       	validar = "no";
	    }
	}

	
	if(jQuery('#telefono_user_cambio').val()==""){
		jQuery('#telefono_user_cambio_form').addClass('has-error');
		jQuery('#telefono_user_cambio_msj').html('Este campo es requerido ');
		validar = "no";
	}
	if(jQuery('#vendedor_user_cambio').val()==null){
		jQuery('#vendedor_user_cambio_form').addClass('has-error');
		jQuery('#vendedor_user_cambio_msj').html('Este campo es requerido ');
		validar = "no";
	}

	if(jQuery("input[name=razon]:checked").val()==2){
		if(jQuery('#explicacion_user_cambio').val()==""){
			jQuery('#explicacion_user_cambio_form').addClass('has-error');
			jQuery('#explicacion_user_cambio_msj').html('Este campo es requerido ');
			validar = "no";
		}
	}
	return validar;
}

function limpiarCamposMail(){
	
	
		jQuery('#nombre_user_cambio_form').removeClass('has-error');
		jQuery('#nombre_user_cambio_msj').html('');
		
		jQuery('#email_user_cambio_form').removeClass('has-error');
		jQuery('#email_user_cambio_msj').html('');

		jQuery('#telefono_user_cambio_form').removeClass('has-error');
		jQuery('#telefono_user_cambio_msj').html('');

		jQuery('#vendedor_user_cambio_form').removeClass('has-error');
		jQuery('#vendedor_user_cambio_msj').html('');

		jQuery('#explicacion_user_cambio_form').removeClass('has-error');
		jQuery('#explicacion_user_cambio_msj').html('');
		
	
}

function notificar(mensaje,type = 'success',icon = "fa fa-check"){
	jQuery.notify({
    	icon: icon,
    	message: mensaje
    	
    },{
        type: type,
        timer: 2000,
        placement: {
            from: 'top',
            align: 'center'
        },
        z_index: 9999999,
	});
}

