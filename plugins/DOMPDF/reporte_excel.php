<?php

header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=Reporte_ordenes_".$_GET['inicio']."_a_".$_GET['fin'].".xls");

define( 'WP_USE_THEMES', false );
require_once( '../../../../../wp-load.php' );

$ordenes = listar_ordenes_reportes($_GET['inicio'],$_GET['fin']);	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LISTA DE USUARIOS</title>
</head>
<body>
<table width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="17" bgcolor="skyblue"><CENTER><strong>REPORTE DE ORDENES</strong></CENTER></td>
  </tr>
  <tr>
    <td><strong>PEDIDO</strong></td>
    <td><strong>NOMBRE DEL CLIENTE</strong></td>
    <td><strong>EMAIL DEL CLIENTE</strong></td>
    <td><strong>TELEFONO DEL CLIENTE</strong></td>
    <td><strong>FECHA DE PEDIDO</strong></td>
    <td><strong>ESTADO DEL PEDIDO</strong></td>
    <td><strong>METODO DE PAGO</strong></td>
    <td><strong>TOTAL</strong></td>
    <td><strong>NUMERO DE FACTURA</strong></td>
    <td><strong>FECHA DE FACTURACION</strong></td>
    <td><strong>DIRECCION DE ENVIO</strong></td>
    <td><strong>EMPRESA DE ENCOMIENDA</strong></td>
    <td><strong>NUMERO DE GUIA</strong></td>
    <td><strong>MONTO DE SEGURO DEL ENVIO</strong></td>
    <td><strong>FECHA DE DESPACHO</strong></td>
    <td><strong>VENDEDOR</strong></td>
    <td><strong>OBSERVACION</strong></td>
  </tr>
  
<?php
		
foreach ($ordenes as $orden) {					

?>  
 <tr>
	<td><?php echo $orden["order_id"]; ?></td>
	<td><?php echo $orden["cliente"]; ?></td>
	<td><?php echo $orden["email_cliente"]; ?></td>
	<td><?php echo $orden["telefono_cliente"]; ?></td>
	<td><?php echo $orden["date"]; ?></td>
	<td><?php echo $orden["status"]; ?></td>
	<td><?php echo $orden["payment_method"]; ?></td>
	<td><?php echo $orden["total"]; ?></td>  
	<td><?php echo $orden["num_factura"]; ?></td>  
	<td><?php echo $orden["fecha_factura"]; ?></td>  
	<td><?php echo $orden["direccion_envio"]; ?></td>
	<td><?php echo $orden["empresa_envio"]; ?></td>
	<td><?php echo $orden["numero_de_guia"]; ?></td>
	<td><?php echo $orden["monto_seguro"]; ?></td>
	<td><?php echo $orden["fecha_despacho"]; ?></td>
	<td><?php echo $orden["vendedor"]; ?></td> 
	<td><?php echo $orden["obj_orden"]; ?></td>                   
 </tr> 
  <?php
	}
  ?>
</table>
</body>
</html>