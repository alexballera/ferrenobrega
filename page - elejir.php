<?php 
/*
Template Name: pagina elejir
*/
?>
<?php include('header.php');?>
<?php include('head.php');?>
<?php if(have_posts()) : while(have_posts()) : the_post();?>
  <section class="content-wrap">
    <div class="container page">

      <h2><?php the_title();?></h2>
      <?php the_breadcrumb();?>
      <?php the_content();?>
      <script type="text/javascript">
          jQuery(function ($) {
            jQuery('#myModal').modal('show')
          });
       </script>


      <div class="container-fluid">
        <div class="row">
            <div class="modal fade bs-example-modal-lg" id="myModal" data-keyboard="false" data-backdrop="static" >
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">

                <div class="modal-header">
                  
                  <h4 class="modal-title text-center" id="myModalLabel">Selecionar Vendedor</h4>
                </div>

                <div class="modal-body">
                    <div class="container-fluid">
                      <div class="row"> 
                        <div class="col-md-12">
                          <div class="text-center" >  
                                <p>Elija el nombre del vendedor que desea que gestione su envío. Si alguno de ellos respondió una pregunta suya en MercadoLibre, coloque su nombre. A partir de este momento Ud. será atendido siempre por este vendedor.</p>
                                <?php 
                                   $user = wp_get_current_user();
                                   
                                   $vendedores = listar_vendedor();
             
                                     foreach ($vendedores as $vendedor) {
                                        $img = get_avatar($vendedor['ID'],185);
                                       ?>
                                      <div class="col-md-4 text-center">
                                         <a href="#" type="button" onclick="CambiarVendedor('<?php echo $user->ID ?>','<?php echo $vendedor['ID'];?>')">
                                           <div class="text-center">
                                               <div class="contenedor-avatar" style="margin-bottom: 0;">
                                                <?php   echo $img; ?><br><br>
                                              </div>
                                               
                                             
                                                  <div class="elegir-vendedor" style="color: white; padding: 10px;">
                                                  <?php   echo $vendedor['first_name']." ".$vendedor['last_name']; ?>
                                               </div>
                                                </div>
                                         </a>
                                      </div>
                                           
                                      <?php
                                     }
             
                                  ?>
                          </div>
                        </div> 
                      </div>  
                    </div>
                  

                </div>

                <div class="modal-footer">
                  <a  class="pull-right" href="<?php bloginfo('home');?>/bienvenida/" style="background-color: #379712 !important; border-radius: 0 !important; color: #fff !important; font-size: 16px; font-weight: 400; padding: 15px 40px !important; text-transform: uppercase; transition: all 0.3s ease-out 0s; margin: 2.5px;">Regresar</a>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>  
    </div>
  </section>
<?php endwhile;?>
<!-- Else -->
<?php else:?>
<?php endif;?>
<?php include('footer.php');?>