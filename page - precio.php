<?php 
/*
Template Name: modulo de precios
*/
?>
<?php global $current_user; ?>
<?php include('header.php');?>
<?php include('head.php');?>
<?php if ($current_user->roles[0]=="administrator"): ?>
<?php if(have_posts()) : while(have_posts()) : the_post();?>
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/plugins/select2/select2.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url');?>/plugins/notify/animate.min.css">
	<style type="text/css">
		.box {
		  position: relative;
		  border-radius: 3px;
		  background: #ffffff;
		  border-top: 3px solid #d2d6de;
		  margin-bottom: 20px;
		  width: 100%;
		  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
		}
		.box.box-primary {
		  border-top-color: #3c8dbc;
		}
		.box.box-info {
		  border-top-color: #00c0ef;
		}
		.box.box-danger {
		  border-top-color: #dd4b39;
		}
		.box.box-warning {
		  border-top-color: #f39c12;
		}
		.box.box-success {
		  border-top-color: #00a65a;
		}
		.box.box-default {
		  border-top-color: #d2d6de;
		}
		.box.collapsed-box .box-body,
		.box.collapsed-box .box-footer {
		  display: none;
		}
		.box .nav-stacked > li {
		  border-bottom: 1px solid #f4f4f4;
		  margin: 0;
		}
		.box .nav-stacked > li:last-of-type {
		  border-bottom: none;
		}
		.box.height-control .box-body {
		  max-height: 300px;
		  overflow: auto;
		}
		.box .border-right {
		  border-right: 1px solid #f4f4f4;
		}
		.box .border-left {
		  border-left: 1px solid #f4f4f4;
		}
		.box.box-solid {
		  border-top: 0;
		}
		.box.box-solid > .box-header .btn.btn-default {
		  background: transparent;

		}
		.box.box-solid > .box-header .btn:hover,
		.box.box-solid > .box-header a:hover {
		  background: rgba(0, 0, 0, 0.1);
		}
		.box.box-solid.box-default {
		  border: 1px solid #d2d6de;
		}
		.box.box-solid.box-default > .box-header {
		  color: #444444;
		  background: #d2d6de;
		  background-color: #d2d6de;
		}
		.box.box-solid.box-default > .box-header a,
		.box.box-solid.box-default > .box-header .btn {
		  color: #444444;
		}
		.box.box-solid.box-primary {
		  border: 1px solid #3c8dbc;
		}
		.box.box-solid.box-primary > .box-header {
		  color: #ffffff;
		  background: #3c8dbc;
		  background-color: #3c8dbc;
		}
		.box.box-solid.box-primary > .box-header a,
		.box.box-solid.box-primary > .box-header .btn {
		  color: #ffffff;
		}
		.box.box-solid.box-info {
		  border: 1px solid #00c0ef;
		}
		.box.box-solid.box-info > .box-header {
		  color: #ffffff;
		  background: #00c0ef;
		  background-color: #00c0ef;
		}
		.box.box-solid.box-info > .box-header a,
		.box.box-solid.box-info > .box-header .btn {
		  color: #ffffff;
		}
		.box.box-solid.box-danger {
		  border: 1px solid #dd4b39;
		}
		.box.box-solid.box-danger > .box-header {
		  color: #ffffff;
		  background: #dd4b39;
		  background-color: #dd4b39;
		}
		.box.box-solid.box-danger > .box-header a,
		.box.box-solid.box-danger > .box-header .btn {
		  color: #ffffff;
		}
		.box.box-solid.box-warning {
		  border: 1px solid #f39c12;
		}
		.box.box-solid.box-warning > .box-header {
		  color: #ffffff;
		  background: #f39c12;
		  background-color: #f39c12;
		}
		.box.box-solid.box-warning > .box-header a,
		.box.box-solid.box-warning > .box-header .btn {
		  color: #ffffff;
		}
		.box.box-solid.box-success {
		  border: 1px solid #00a65a;
		}
		.box.box-solid.box-success > .box-header {
		  color: #ffffff;
		  background: #00a65a;
		  background-color: #00a65a;
		}
		.box.box-solid.box-success > .box-header a,
		.box.box-solid.box-success > .box-header .btn {
		  color: #ffffff;
		}
		.box.box-solid > .box-header > .box-tools .btn {
		  border: 0;
		  box-shadow: none;
		}
		.box.box-solid[class*='bg'] > .box-header {
		  color: #fff;
		}
		.box .box-group > .box {
		  margin-bottom: 5px;
		}
		.box .knob-label {
		  text-align: center;
		  color: #333;
		  font-weight: 100;
		  font-size: 12px;
		  margin-bottom: 0.3em;
		}
		.box > .overlay,
		.overlay-wrapper > .overlay,
		.box > .loading-img,
		.overlay-wrapper > .loading-img {
		  position: absolute;
		  top: 0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		}
		.box .overlay,
		.overlay-wrapper .overlay {
		  z-index: 50;
		  background: rgba(255, 255, 255, 0.7);
		  border-radius: 3px;
		}
		.box .overlay > .fa,
		.overlay-wrapper .overlay > .fa {
		  position: absolute;
		  top: 50%;
		  left: 50%;
		  margin-left: -15px;
		  margin-top: -15px;
		  color: #000;
		  font-size: 30px;
		}
		.box .overlay.dark,
		.overlay-wrapper .overlay.dark {
		  background: rgba(0, 0, 0, 0.5);
		}
		.box-header:before,
		.box-body:before,
		.box-footer:before,
		.box-header:after,
		.box-body:after,
		.box-footer:after {
		  content: " ";
		  display: table;
		}
		.box-header:after,
		.box-body:after,
		.box-footer:after {
		  clear: both;
		}
		.box-header {
		  color: #444;
		  display: block;
		  padding: 10px;
		  position: relative;
		}
		.box-header.with-border {
		  border-bottom: 1px solid #f4f4f4;
		}
		.collapsed-box .box-header.with-border {
		  border-bottom: none;
		}
		.box-header > .fa,
		.box-header > .glyphicon,
		.box-header > .ion,
		.box-header .box-title {
		  display: inline-block;
		  font-size: 18px;
		  margin: 0;
		  line-height: 1;
		}
		.box-header > .fa,
		.box-header > .glyphicon,
		.box-header > .ion {
		  margin-right: 5px;
		}
		.box-header > .box-tools {
		  position: absolute;
		  right: 10px;
		  top: 5px;
		}
		.box-header > .box-tools [data-toggle="tooltip"] {
		  position: relative;
		}
		.box-header > .box-tools.pull-right .dropdown-menu {
		  right: 0;
		  left: auto;
		}
		.btn-box-tool {
		  padding: 5px;
		  font-size: 12px;
		  background: transparent;
		  color: #97a0b3;
		}
		.open .btn-box-tool,
		.btn-box-tool:hover {
		  color: #606c84;
		}
		.btn-box-tool.btn:active {
		  box-shadow: none;
		}
		.box-body {
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		  border-bottom-right-radius: 3px;
		  border-bottom-left-radius: 3px;
		  padding: 10px;
		}
		.no-header .box-body {
		  border-top-right-radius: 3px;
		  border-top-left-radius: 3px;
		}
		.box-body > .table {
		  margin-bottom: 0;
		}
		.box-body .fc {
		  margin-top: 5px;
		}
		.box-body .full-width-chart {
		  margin: -19px;
		}
		.box-body.no-padding .full-width-chart {
		  margin: -9px;
		}
		.box-body .box-pane {
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		  border-bottom-right-radius: 0;
		  border-bottom-left-radius: 3px;
		}
		.box-body .box-pane-right {
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		  border-bottom-right-radius: 3px;
		  border-bottom-left-radius: 0;
		}
		.box-footer {
		  border-top-left-radius: 0;
		  border-top-right-radius: 0;
		  border-bottom-right-radius: 3px;
		  border-bottom-left-radius: 3px;
		  border-top: 1px solid #f4f4f4;
		  padding: 10px;
		  background-color: #ffffff;
		}
		.chart-legend {
		  margin: 10px 0;
		}
		@media (max-width: 991px) {
		  .chart-legend > li {
		    float: left;
		    margin-right: 10px;
		  }
		}
		.box-comments {
		  background: #f7f7f7;
		}
		.box-comments .box-comment {
		  padding: 8px 0;
		  border-bottom: 1px solid #eee;
		}
		.box-comments .box-comment:before,
		.box-comments .box-comment:after {
		  content: " ";
		  display: table;
		}
		.box-comments .box-comment:after {
		  clear: both;
		}
		.box-comments .box-comment:last-of-type {
		  border-bottom: 0;
		}
		.box-comments .box-comment:first-of-type {
		  padding-top: 0;
		}
		.box-comments .box-comment img {
		  float: left;
		}
		.box-comments .comment-text {
		  margin-left: 40px;
		  color: #555;
		}
		.box-comments .username {
		  color: #444;
		  display: block;
		  font-weight: 600;
		}
		.box-comments .text-muted {
		  font-weight: 400;
		  font-size: 12px;
		}

		.btn-success{
			background-color: #379712 !important; 
			border-radius: 0 !important; 
			color: #fff !important; 
			text-transform: uppercase;
			
		}
		.btn-default{
			border-radius: 0 !important; 
			text-transform: uppercase;
		}
		.well{
			background-color: rgba(46,65,104,0.3);
			border: 1px solid rgba(46,65,104,0.4)
			
			
		}
	</style>
	<section class="content-wrap">
		<div class="container page">

			<h2><?php the_title();?></h2>
			<?php the_breadcrumb();?>
			<?php the_content();?>
			<div class="container-fluid">
				<div class="row">

					<!--Formulario de cambio de precios general-->
					<div class="col-xs-12 col-md-6">
						<div class="box box-info">
							<div class="box-header">
								<h3 class="box-title">Cambio de precio general</h3>
							</div>
							<div class="box-body">
								
								<form action="" method="POST" role="form">
									
									<div class="well well-sm">
										<label>Tipo: </label>
										<div class="radio">

											<label>
												<input type="radio" name="tipo_de_calculo_general" id="radio1" value="1" checked="checked">
												Porcentaje  
											</label>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<label>
												<input type="radio" name="tipo_de_calculo_general" id="radio2" value="2">
												Monto
											</label>
										</div>
									</div>
								
									<div class="form-group" id="porcentaje-general">
										<div class="input-group">
										  <input type="text" class="form-control numeric" placeholder="Porcentaje" id="input-porcentaje-general" aria-describedby="basic-addon2">
										  <span class="input-group-addon" ><strong>%</strong></span>
										</div>
										<div>
											<span class="text-danger" id="error_general_1"></span>
										</div>
										
									</div>
									
									<div class="form-group" id="monto-general">
										<div class="input-group">
										  <input type="text" class="form-control numeric" placeholder="Monto" id="input-monto-general" aria-describedby="basic-addon2">
										  <span class="input-group-addon" id="basic-addon2"><i class="fa fa-money"></i></span>
										</div>
										<div>
											<span class="text-danger" id="error_general_2"></span>
										</div>
									</div>
									
									<div class="well well-sm">
										<div class="radio">
											<label>
												<input type="radio" name="accion_calculo_general" id="calculo_radio_1" value="sumar" checked="checked">
												Aumentar  
											</label>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<label>
												<input type="radio" name="accion_calculo_general" id="calculo_radio_2" value="restar">
												Disminuir
											</label>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<label>
												<input type="radio" name="accion_calculo_general" id="calculo_radio_ajuste_1" value="ajustar">
												Ajustar
											</label>
										</div>
									</div>

									<button type="button" class="btn btn-success btn-block" id="actualizar-general" data-loading-text="Actualizando Precios..." autocomplete="off">Actualizar</button>
								</form>
							</div>
							<div class="load-ajax1"></div>
						</div>	
					</div>

					<!--Formulario de cambio de precios por categoria-->
					<div class="col-xs-12 col-md-6">
						<div class="box box-info">
							<div class="box-header">
								<h3 class="box-title">Cambio de precio por etiquetas</h3>
							</div>
							<div class="box-body">


								<form action="" method="POST" role="form" id="form-categoria">
									
									<div class="well well-sm">
										<label>Tipo: </label>
										<div class="radio">
											<label>
												<input type="radio" name="tipo_de_calculo_categoria" id="radio3" value="1" checked="checked">
												Porcentaje  
											</label>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<label>
												<input type="radio" name="tipo_de_calculo_categoria" id="radio4" value="2">
												Monto
											</label>
										</div>
									</div>

									<?php $etiquetas_orderby_options = etiquetas(); ?>
										

									<div class="form-group " id="select_categoria">

										
										<select name="" id="sub_categoria" class="form-control" required="required" style="width: 100%;">
												<option>-Seleccione-</option>
												<?php foreach ($etiquetas_orderby_options as $etiqueta): ?>
													<option value="<?php echo $etiqueta->term_id; ?>"><?php echo $etiqueta->name; ?></option>
												<?php endforeach ?>
												
										</select>

										
										<div>
											<span class="text-danger" id="error_categoria_3"></span>
										</div>
									</div>
								
									<div class="form-group" id="porcentaje-categoria">
										<div class="input-group">
										  <input type="text" class="form-control numeric" placeholder="Porcentaje" aria-describedby="basic-addon2" id="input-porcentaje-categoria">
										  <span class="input-group-addon"><strong>%</strong></span>
										</div>
										<div>
											<span class="text-danger" id="error_categoria_1"></span>
										</div>
									</div>
									
									<div class="form-group" id="monto-categoria">
										<div class="input-group">
										  <input type="text" class="form-control numeric" placeholder="Monto" aria-describedby="basic-addon2" id="input-monto-categoria">
										  <span class="input-group-addon" ><i class="fa fa-money"></i></span>
										</div>
										<div>
											<span class="text-danger" id="error_categoria_2"></span>
										</div>
									</div>

									<div class="well well-sm">
										<div class="radio">
											<label>
												<input type="radio" name="accion_calculo_categoria" id="calculo_radio_3" value="sumar" checked="checked">
												Aumentar  
											</label>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<label>
												<input type="radio" name="accion_calculo_categoria" id="calculo_radio_4" value="restar">
												Disminuir
											</label>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<label>
												<input type="radio" name="accion_calculo_categoria" id="calculo_radio_ajuste_2" value="ajustar" disable="true">
												Ajustar
											</label>
										</div>
									</div>
								
									<button type="button" class="btn btn-success btn-block" id="actualizar-categoria" data-loading-text="Actualizando Precios..." autocomplete="off">Actualizar</button>
								</form>
							</div>
							<div class="load-ajax2"></div>
						</div>	
					</div>

					<?php  
					  //$args = array('post_type' 	 => 'product','posts_per_page' => -1);
					  //$products = get_posts( $args ); 

					?>

					<div class="col-xs-12">	
						<div class="box box-info">
							<div class="box-header">
								<h3 class="box-title" >Productos</h3>
							</div>
							<div class="box-body">
								<table class="table table-bordered table-hover" id="tabla-producto">
									<thead>
										<tr>
											<th>Producto</th>
											<th>Etiqueta</th>
											<th>Precio regular</th>
											<th>Precio de venta</th>
											<th>Acción</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
						
					</div>	
				</div>
			</div>

			
			<div class="modal fade" id="modal-id">
				<div class="modal-dialog">
					<div class="modal-content box box-info">

						<form action="" method="POST" role="form">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Producto</h4>
						</div>
						<div class="modal-body">
								<input type="hidden" id="id-producto"></input>

								<div class="well well-sm" style="font-weight: bold;">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group" style="margin-top: 7px">
												<p style="font-size: 12px">Precio Regular Actual : <sapn id="p_r"></sapn>  <?php echo get_woocommerce_currency_symbol(); ?></p>
											</div>
											<div class="form-group" style="margin-top: 30px">
												<p style="font-size: 12px">Precio de Venta Actual: <sapn id="p_s"></sapn> <?php echo get_woocommerce_currency_symbol(); ?></p>
											</div>
											
										</div>

										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" class="form-control input-sm numeric" id="input-precio-regular-unico" placeholder="Precio Regular Nuevo"></input>
											</div>
											<div class="form-group">
												<input type="text" class="form-control input-sm numeric" id="input-precio-venta-unico"  placeholder="Precio de Venta Nuevo"></input>
											</div>
										</div>

										<div class="col-xs-12">
											<button type="button" class="btn btn-success btn-block btn-sm" id="modificar-precio-unico" data-loading-text="Modificando..." autocomplete="off" >Modificar</button>
										</div>
									</div>
									
								</div>


									
								<div class="well well-sm">
									<label>Tipo: </label>
									<div class="radio">

										<label>
											<input type="radio" name="tipo_de_calculo_unico" id="radio1" value="1" checked="checked">
											Porcentaje  
										</label>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<label>
											<input type="radio" name="tipo_de_calculo_unico" id="radio2" value="2">
											Monto
										</label>
									</div>
								</div>
							
								<div class="form-group" id="porcentaje-unico">
									<div class="input-group">
									  <input type="text" class="form-control numeric" placeholder="Porcentaje" aria-describedby="basic-addon2" id="input-porcentaje-unico">
									  <span class="input-group-addon" id="basic-addon2"><strong>%</strong></span>
									</div>
									<div>
										<span class="text-danger" id="error_unico_1"></span>
									</div>
								</div>
								
								<div class="form-group" id="monto-unico">
									<div class="input-group">
									  <input type="text" class="form-control numeric" placeholder="Monto|" aria-describedby="basic-addon2" id="input-monto-unico">
									  <span class="input-group-addon" id="basic-addon2"><i class="fa fa-money"></i></span>
									</div>
									<div>
										<span class="text-danger" id="error_unico_2"></span>
									</div>
								</div>
								
								<div class="well well-sm">
									<div class="radio">
										<label>
											<input type="radio" name="accion_calculo_unico" id="calculo_radio_1" value="sumar" checked="checked">
											Aumentar  
										</label>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<label>
											<input type="radio" name="accion_calculo_unico" id="calculo_radio_2" value="restar">
											Disminuir
										</label>
									</div>
								</div>

								
							
						</div>
						<div class="load-ajax3"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							<button type="button" class="btn btn-success pull-left" id="actualizar-unico" data-loading-text="Actualizando..." autocomplete="off" >Actualizar</button>
						</div>
						</form>
					</div>
				</div>
			</div>
		   
		</div>
	</section>
<?php endwhile;?>
<!-- Else -->
<?php else:?>
<?php endif;?>
<?php else: ?>
	<script type="text/javascript">
		
		window.location = "<?php bloginfo('home');?>";

	</script>
<?php endif;?>
<?php include('footer.php');?>
<script src="<?php bloginfo('template_url');?>/plugins/select2/select2.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url');?>/plugins/numeric/jquery.numeric.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url');?>/plugins/notify/bootstrap-notify.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url');?>/plugins/ajax/precios.js" type="text/javascript"></script>

