<?php
/*
Template Name: paginabienvenida
*/
?>
<?php include('header.php');?>
<?php include('head.php');?>
<?php if(have_posts()) : while(have_posts()) : the_post();?>
	<section class="content-wrap">
		<div class="container page">
			<?php 
				global $wpdb;
				$user = wp_get_current_user();

				 
				 $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$user->ID;
				 $resultado = $wpdb->get_results($query);
				 $vendedor = "no";

				 $primera =  get_user_meta( $user->ID, 'primera_vez', true );

				if(empty($primera)){
					$wpdb->insert($wpdb->usermeta, array("user_id" => $user->ID, "meta_key" => "primera_vez", "meta_value" => 1), array("%d", "%s", "%d"));
				}

				 foreach ($resultado as $r) {
				 	if($r->meta_key=="billing_first_name"){
				 		$name=$r->meta_value;
				 	}
				 	if($r->meta_key=="billing_last_name"){
				 		$last_name=$r->meta_value;
				 	}
				 	if($r->meta_key=="id_vendedor"){
				 		$vendedor=$r->meta_value;
				 	}


				 }

				 if($vendedor == "no"){
				 	 $query = "SELECT *
						FROM wp_users INNER JOIN wp_usermeta 
						ON wp_users.ID = wp_usermeta.user_id 
						WHERE wp_usermeta.meta_key = 'wp_capabilities' 
						AND wp_usermeta.meta_value LIKE '%vendor%' 
						ORDER BY RAND() LIMIT 1";

					 $resultado = $wpdb->get_results($query);

					 $id_vendedor = $resultado[0]->ID;

					 $wpdb->insert($wpdb->usermeta, array("user_id" => $user->ID, "meta_key" => "id_vendedor", "meta_value" => $resultado[0]->ID), array("%d", "%s", "%d"));
				 }else{
					 $id_vendedor = $vendedor;
				 }

				 //-----------------------------------------------------------------
				 //
				 //               DATOS DEL VENDEDOR (INICIO)
				 //
				 //-----------------------------------------------------------------

				 $query = "SELECT * FROM wp_users WHERE ID = ".$id_vendedor;

				 $email_vendedor = $wpdb->get_results($query);

				 $email_vendedor = $email_vendedor[0]->user_email;

				 $query = "SELECT * FROM wp_usermeta WHERE wp_usermeta.user_id = ".$id_vendedor;

				 $resultado = $wpdb->get_results($query);
				 $img = get_avatar($id_vendedor);

	  
				 foreach ($resultado as $r) {
				 	if($r->meta_key=="first_name"){
				 		$name_vendedor=$r->meta_value;
				 	}
				 	if($r->meta_key=="last_name"){
				 		$last_name_vendedor=$r->meta_value;
				 	}
				 	if ($r->meta_key=="billing_phone") {
				 		$phone_vendedor=$r->meta_value;
				 	}
				 }
				
				 //-----------------------------------------------------------------
				 //
				 //               DATOS DEL VENDEDOR (FIN)
				 //
				 //-----------------------------------------------------------------
			
				 if(!empty($resultado)){
				 	
				 	?>
				 	<script type="text/javascript">
				 	jQuery(function ($) {
						jQuery('#myModal').modal('show')
					});
				 	
				 	
				 	</script>
				 	
				 	<?php 
				 }

			?>
			<h2><?php the_title();?></h2>
			<?php the_breadcrumb();?>
			<?php the_content();?>

			

			<!-- Modal -->
			<div class="modal fade bs-example-modal-lg" id="myModal" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">

			      <div class="modal-header">
			        
			        <h4 class="modal-title text-center" id="myModalLabel">¡Te damos la Bienvenida!</h4>
			      </div>

			      <div class="modal-body">
			      		<div class="container-fluid">
			      			<div class="row">	
		      					<div class="col-md-12">
		      						<div class="text-center" >	
	      								<p>
	      									Saludos <strong><?php echo $name." ".$last_name; ?></strong> bienvenido a nuestro sistema.
	      								</p>
	      								
	      								
		      					        <div class="col-md-12 text-center">
		      					           <hr>
								           <form action="">
								             <p>
								           	  ¿Quieres elegir tu asesor de ventas (Si respondes "No" el sistema te asignará uno automáticamente)?&nbsp;&nbsp; 
											  Si <input type="radio" name="opcion" id="si" value="si" checked="true">&nbsp;
											  No <input type="radio" name="opcion" id="no" value="no" >
								           	  
		      					             </p>
								           </form>
		      								<hr>	
		      					        </div>
		      					       
	      							</div>
	      							
		      					</div>
		      					
			      			</div>	
			      		</div>
			       

			      </div>

			      <div class="modal-footer">
			       <a  type="button" class="pull-right" id="cerrar" style="background-color: #379712 !important; border-radius: 0 !important; color: #fff !important; font-size: 16px; font-weight: 400; padding: 15px 40px !important; text-transform: uppercase; transition: all 0.3s ease-out 0s; margin: 2.5px;">¡OK! Continuemos</a>
			      </div>

			    </div>
			  </div>
			</div>

		</div>
	</section>
<?php endwhile;?>
<!-- Else -->
<?php else:?>
<?php endif;?>
<?php include('footer.php');?>

<script type="text/javascript">
 	
 	jQuery('#cerrar').click(function(){
 		if(jQuery('input:radio[name=opcion]:checked').val()=='si'){
			window.location.href = "<?php bloginfo('home');?>/elija-un-vendedor/";
		}else{
			window.location.href = "<?php bloginfo('home');?>/mi-cuenta/view-order";
		}
 	});

 	/*jQuery('#myModal').on('hidden.bs.modal', function (e) {
	   window.location.href = "/mi-cuenta/view-order";
	})


	jQuery("#no").on( 'change', function() {
	    if( jQuery(this).is(':checked') ) {
	        // Hacer algo si el checkbox ha sido seleccionado
	        window.location.href = "/elija-un-vendedor/";
	    }
	});*/
 </script>