<?php 
/*
Template Name: pagina historico
*/
?>

<?php include('header.php');?>
<?php include('head.php');?>
<?php if(have_posts()) : while(have_posts()) : the_post();?>
	<?php 

	 $data = listar_vendedor();
	?>
	<section class="content-wrap">
		<div class="container page">

			<h2><?php the_title();?></h2>
			<?php the_breadcrumb();?>
			<?php the_content();?>
			 <h3 class="text-center">Vendedores</h3>	
			 <table class="table table-striped table-bordered display" id="tabla-custodio" width="100%">
                 <thead>
                     <th>ID</th>
                     <th>Usuario</th>
                     <th>Email</th>
                     <th>Nombre</th>
                     <th>TELEFONO</th>
                     <th>Acccion</th>
                 </thead>
                 <tbody>
                 	<?php foreach ($data as $d) { ?>
	                 	<tr>
	                 		 <td class="text-center"><?php echo $d['ID']; ?></td>
		                     <td ><?php echo $d['user_nicename']; ?></td>
		                     <td ><?php echo $d['user_email']; ?></td>
		                     <td ><?php echo $d['first_name'].' '.$d['last_name']; ?></td>
		                     <td ><?php echo $d['billing_phone']; ?></td>
		                     <td class="text-center"><button value="<?php echo $d['ID']; ?>" class="btn btn-success btn-sm" onClick="Mostrar(this)" data-toggle="modal" data-target="#editar_vendedor">Editar</button>
		                     </td>
		                 </tr>
                 	<?php } ?>
                 </tbody>
             </table>

             <?php  
             	$data_comprador = listar_comprador();
             ?>
             <!-- -->
             <h3 class="text-center">Cliente</h3>
             <table class="table table-striped table-bordered display" id="tabla-Cliente" width="100%">
                 <thead>
                     <th>ID</th>
                     <th>Usuario</th>
                     <th>Email</th>
                     <th>Nombre</th>
                     <th>TELEFONO</th>
                     <th>VENDEDOR ASIGNADO</th>
                     <th>Acccion</th>
                 </thead>
                 <tbody>
                 	<?php foreach ($data_comprador as $dc) { ?>
	                 	<tr>
	                 		 <td class="text-center"><?php echo $dc['ID']; ?></td>
		                     <td ><?php echo $dc['user_nicename']; ?></td>
		                     <td ><?php echo $dc['user_email']; ?></td>
		                     <td ><?php echo $dc['first_name'].' '.$dc['last_name']; ?></td>
		                     <td ><?php echo $dc['billing_phone']; ?></td>
		                     <td ><?php echo $dc['vendedor']; ?></td>
		                     <td class="text-center"><button value="<?php echo $dc['ID']; ?>" class="btn btn-success btn-sm" onClick="MostrarVendedor(this)" data-toggle="modal" data-target="#editar_cliente">Asignar/Cambiar Vendedor</button>
		                     </td>
		                 </tr>
                 	<?php } ?>
                 </tbody>
             </table>

             <?php $data_historico = listar_historial() ?>
             <h3 class="text-center">Ordenes</h3>
             <table class="table table-striped table-bordered display" id="tabla-Cliente" width="100%">
                 <thead>
                     <th>Numero de Orden</th>
                     <th>Fecha</th>
                     <th>Total</th>
                     <th>Metodo de Pago</th>
                     <th>Cliente</th>
                     <th>Vendedor</th>
                 </thead>
                 <tbody>
                 	<?php foreach ($data_historico as $dh) { ?>
	                 	<tr>
	                 		 <td class="text-center"><?php echo $dh['order_id']; ?></td>
		                     <td ><?php echo $dh['date']; ?></td>
		                     <td ><?php echo $dh['total']; ?></td>
		                     <td ><?php echo $dh['payment_method']; ?></td>
		                     <td ><?php echo $dh['first_name'].' '.$dh['last_name']; ?></td>
		                     <td ><?php echo $dh['vendedor']; ?></td>
		                     </td>
		                 </tr>
                 	<?php } ?>
                 </tbody>
             </table>
              <span id="ajax_admin_options_nonce" class="hidden"><?php echo wp_create_nonce( 'ajax_admin_options_nonce' ); ?></span>
		</div>
	</section>

	<!-- Modal -->
	<div class="modal fade" id="editar_vendedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">

	   	<form> 
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="">Editar Vendedor</h4>
		      </div>
		      <div class="modal-body">
		       		
		      	  <input type="hidden" class="form-control" id="ID" name="ID"  required> 
				  <div class="form-group ">
				    <label for="user_nicename">Nombre de Usuario: </label>
				    <input type="text" class="form-control " id="user_nicename" name="user_nicename" placeholder="Usuario" required disabled>
				  </div>

				  <div class="form-group">
				    <label for="first_name">Nombre del Vendedor: </label>
				    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Nombre" required>
				  </div>

				  <div class="form-group">
				    <label for="last_name">Apellido del Vendeor: </label>
				    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Apellido">
				  </div>

				  <div class="form-group disable">
				    <label for="user_email">Email:</label>
				    <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" required disabled>
				  </div>

				  <div class="form-group">
				    <label for="billing_phone">Telefono: </label>
				    <input type="billing_phone" class="form-control" id="billing_phone" name="billing_phone" placeholder="Telefono" required>
				  </div>
			
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-danger" style="border-radius: 0 !important; color: #fff !important; font-size: 16px; font-weight: 400; text-transform: uppercase; margin: 2.5px;" data-dismiss="modal">Cerrar</button>
		        <button type="button" id="editar" onclick="Guardar()" class="btn btn-success" style="background-color: #379712 !important; border-radius: 0 !important; color: #fff !important; font-size: 16px; font-weight: 400; text-transform: uppercase; margin: 2.5px;">Guardar</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="editar_cliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">

	   	<form> 
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="">Asignar/Cambiar Vendedor</h4>
		      </div>
		      <div class="modal-body">
		       		
		      	  <input type="hidden" class="form-control" id="user_id" name="user_id"  required> 

				  <div class="form-group ">
				    <label for="user_nicename">Vendedor Asignado: </label>
				    <select name='id_vendedor' id="id_vendedor" class="form-control" required>
				    	<option value="" selected disabled>-Seleccione-</option>
				    	<?php foreach ($data as $d) { ?>
				    		<option value="<?php echo $d['ID']; ?>"><?php echo $d['first_name'].' '.$d['last_name']; ?></option>
				    	<?php } ?>
				    </select>
				   
				  </div>
			
		      </div>
		      <div class="modal-footer">
		       <button type="button" class="btn btn-danger" style="border-radius: 0 !important; color: #fff !important; font-size: 16px; font-weight: 400; text-transform: uppercase; margin: 2.5px;" data-dismiss="modal">Cerrar</button>
		        <button type="button" onclick="GuardarDatosClientes()" id="guradar_vendedor" class="btn btn-success" style="background-color: #379712 !important; border-radius: 0 !important; color: #fff !important; font-size: 16px; font-weight: 400; text-transform: uppercase; margin: 2.5px;">Guardar</button>
		      </div>
	      </form>
	    </div>
	  </div>
	</div>



<?php endwhile;?>
<!-- Else -->
<?php else:?>
<?php endif;?>
<?php include('footer.php');?>

<script type="text/javascript">
	jQuery('table.display').DataTable({
		select: true,
		scrollY:  "500px",
        scrollCollapse: true,
        language:{
			    "decimal":        "",
			    "emptyTable":     "No hay datos disponibles en la tabla",
			    "info":           "Mostrando  _START_ a _END_ de _TOTAL_ entradas ",
			    "infoEmpty":      "Mostrando  0 a 0 de 0 entradas ",
			    "infoFiltered":   "(filtrada a partir de  _MAX_  entradas )",
			    "infoPostFix":    "",
			    "thousands":      ",",
			    "lengthMenu":     "Mostra _MENU_ entradas ",
			    "loadingRecords": "Cargando...",
			    "processing":     "Tratamiento...",
			    "search":         "Buscar:",
			    "zeroRecords":    "No se encontraron registros coincidentes",
			    "paginate": {
			        "first":      "Primero",
			        "last":       "Último",
			        "next":       "Siguiente",
			        "previous":   "Anterior"
			    },
			    "aria": {
			        "sortAscending":  ": activar para ordenar la columna ascendente",
			        "sortDescending": ": activar para ordenar la columna descendente"
			    }
		}
	});

</script>