<?php include('header.php');?>
<?php include('head.php');?>

<?php if(have_posts()) : while(have_posts()) : the_post();?>
<?php the_title();?>
<?php the_content();?>
<?php endwhile;?>
<?php else:?>
<!-- Else -->
<?php endif;?>


<?php include('footer.php');?>